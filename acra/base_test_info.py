'''
=============================================================================

 Name:     base_test_info.py

 Author:   Chris Gilbert

 Date:     March 29, 2022
 
 Copyright Curtiss Wright 2022

 Description

 Core class for production tests.

=============================================================================
'''

from os import path, getcwd, sys, chdir, environ, listdir, mkdir          #system
from shutil import copy
from platform import platform, architecture
from time import time, strftime    # sleep
from re import match
#from datetime import datetime, date
from sys import exit as base_test_info_exit    #stdout, argv removed from list

from pickle import dump, load

import logging

#from acra.utils import acraexec

from acra.test_framework import check_folder, del_file, py_cls, sanitise_folder_name
from acra.common_functions import time_stamp_results_folder, time_stamp_report, contains_number

# libraries required for PDF generation
from acra.reporting_tools import seconds_to_dmy, show_debug, section_heading, report_heading, report_summary, report_result, pass_fail     # section_result, gen_pdf_report
from acra import serial_number_tools
from acra.version import version as acra_utils_version



#import FileUtils
# for 'pickling' test data so that it can be retrieved when the test is restarted


class BaseTestInfo :
    '''
    Basic data structure and functions for use in production test scripts.
    Additional variables can be added in teh test script itself, without needing
    to make any changes to this module at all
    
    1.0 - 1.3 Feral versions of the test framework.
    1.4 introduces BASETESTINFO.First revision in GIT repository
    1.5 integrates housekeeping sector support
    2.0 Refactored after passing through pylint...
    
    self.results_dict : This is a dictionary that uses the module serial number as the key.
    Each entry consists of a list of 3 items
    
        1) Test Name (string)
        2) Was the test run (bool True/False)
        3) The results file name for this test
    
    e.g.
    
        self.results_dict = { 'ABC1234': [['Precal', True, <somepath>], ['DC Accuracy', True,  <somepath>], ['AC Accuracy', True,  <somepath>], ['SINAD', True,  <somepath>]],
                              'BCD2345': [['Precal', True, <somepath>], ['DC Accuracy', True,  <somepath>], ['AC Accuracy', True,  <somepath>], ['SINAD', True,  <somepath>]],
                              'CDE3456', [['Precal', True, <somepath>], ['DC Accuracy', False, <somepath>], ['AC Accuracy', False, <somepath>], ['SINAD', False, <somepath>]]
                            }
    '''

    def __init__(self):
        '''
        Base structure for test class.
        New variables can be added but this is the base variable set:

        '''

        # test information
        self.framework_version = '4.0'

        # populated from INI file
        self.product_name      = ''            # module name e.g. KAD/ABC/123/Z/99/ET/XYZ
        self.pickle_file       = ''            # pickle file for temporary storage
        self.test_folder       = getcwd()      # where the test is called from
        self.test_report_folder = 'REPORT'
        self.results_folder     = 'RESULTS'

        self.pdf_report_test        = True     # generate the report_test in PDF format in addition to txt format
        self.record_temp       = True          # record the temp and RH, True/False

        self.record_equipment  = True          # record the equipment history True/False
        self.path_to_cal_database = ''         

        self.import_equipment_file = True      # import and validate an equipment list
        self.import_jig_file       = True      # import and validate a jig list
        
        self.imported_equipment_file = ''
        self.imported_jig_file       = ''
        
        self.equip_file_creation_time  = ''
        self.jig_file_creation_time  = ''

        # test report details
        self.pdf_report        = False         # default to no PDF report generated 
        self.template_path     = ''            # path to template for PDF report generation
        self.template_name     = ''            # template name (xxx.dotx) for PDF report generation
        self.report_front_page = ''            # report_test front page (yyy.docx) for PDF report generation
        self.report_name       = ''            # created by the test

        self.equipment_list    = []            # empty list for all equipment to be added to - function, manufacturer model
                                               # this comes from the INI file

        self.jig_list          = []            # empty list for all jigs etc to be added to - Description
                                               # this comes from the INI file

                                               
        self.legal_time        = 0.0           # how long the operator has between starting the test and generating the report_test.

        self.type_no           = 0xFFF         # default to generic proxy

        self.fwa               = ''            # assumes one firmware image in product expects FWA/AA/003 strings
        self.fwa_rev           = ''            # firmware rev
        self.fpga1             = ''            # assumes 1 FPGA image in product, expects TIC/AB/123
        self.fpga1_rev         = ''            # FPGA revision
        self.fpga_ix           = 0             # default is one FPGA per product. 
        
        self.fpga2             = ''
        self.fpga2_rev         = ''

        # ISP details        
        self.golden_fpga1      = ''
        self.golden_fpga1_rev  = ''
        self.golden_fpga2      = ''
        self.golden_fpga2_rev  = ''
    
        self.isp_fpga1         = ''
        self.isp_fpga1_rev     = ''
        self.isp_fpga2         = ''
        self.isp_fpga2_rev     = ''        

        self.golden_fwa1       = ''
        self.golden_fwa1_rev   = ''
        self.isp_fwa1          = ''
        self.isp_fwa1_rev      = ''
        
        self.isp_dir           = '.\\'         # FPGA ISP folder.


        self.kc_command        = ''
        self.kup_command       = ''
        self.ds3_command       = ''
        self.ds3_output_file   = ''
        self.tftp              = False
        
        # DAS Studio Version
        self.das_version        = '3.4.19'       # default to 3.4.19: A 2021 release
        
        self.programming_retry_count = 5        # retry count of programming fails.

        # user_name input variables
        self.user_name              = ''
        self.ser_no             = ''
        self.aft_ser_nos        = []
        self.aft_test           = False

        # lists populated with headers
        self.assembly_list     = [ ['Component', 'Name', 'Serial Number'] ]
        self.equipment_details = [ ['Function', 'Make', 'Model', 'S/N', 'Last Calibration Date', 'Next Calibration Date'] ]
        self.jig_details       = [ ['Description', 'ID', 'Last Test Date', 'Next Test Date'] ]

        # dynamic variables
        self.date_time          = ''            # time in "%B %d, %Y\t%H:%M:%S" format
        self.timestamp          = 0.0           # time in seconds since start of the epoch
        self.start_time         = ''            # test start time in "%B %d, %Y\t%H:%M:%S" format
        self.end_time           = ''
        self.test_duration      = 0.0
        self.test_seq_number    = 0             # test Sequence number so that it is available for report_testing, if necessary 
        self.interim_rpt_path   = ''
        self.int_test_report_name = ''          # for sharing with other utilities
        self.running_all        = False 
        self.test_list          = []
        self.excluded_list      = ['Enter Test Info', 'Generate Report', 'Run All', 'Quit']
        self.this_test_name     = ''
        self.test_temperature_point = ''
        self.results_dict       = {}            # empty dictionary for AFT/ADAU results. key is the serial number
        self.files_to_copy_dict = {}            # empty dictionary of files for copying to the server. key is the serial number
        
        self.network_backup     = False         # assume the test results are not backed up to the network
        self.network_backup_folder = ''         # If they are we need a mapped drive and a folder to map to
        
        
        self.results            = []
        self.use_latest_das     = True
        self.das_root           = 'C:\\Test_DAS\\StandaloneCompiler'
        self.das_standalone_dir = ''
        self.das_standalone_cmd = ''
        self.das_studio_msg_log = ''  
        
        self.top_sector         = 0x20
        
        self.ini_file_name        = ''

        self.valid_keys         = []

        self.start_rh           = 0.0           # relative humidity at the start of the test
        self.end_rh             = 0.0           # relative humidity at the end of the test
        self.start_temp         = 0.0           # temperature at the start of the test
        self.end_temp           = 0.0           # temperature at the end of the test

        self.humidity_limits   = []            # humidity limits for test
        self.temp_limits       = []            # temperature limits for test

        # generic constants for use throughout
        self.quit_test   = 'Q'
        self.report_test = 'R'
        self.run_all_tests = 'A'
        self.equip_split_char = ':'
        
        # check if the test is running on Windows 7 and set any system paths accordingly
        # At the moment it sets tftp_path and self.win7check
        self.check_os()
        
        # eeprom size for serial number etc manipulation.
        self.eeprom_size       = 0x80
        
        # instantiate housekeeping tools so that they are accessible from housekeeping even though they are in a different library
        self.housekeeping = serial_number_tools.SERIAL_NO()
        
        self.logging_level = logging.WARNING       #logging.DEBUG logging.INFO logging.WARNING logging.ERROR

        # variables added for a real AFT test.
        self.slot_ix        = 0
        self.current_module = 0


    def gen_housekeeping_sector(self):
        '''
        wrapper for serial_number_tools.CreateTopSector() that uses the arguements
        available in BASETESTINFO
        outputs either a string (classic programming tools) or a list of bytes (tftp)

        this data needs to be written to a text file or a binary file
        
        for binary file use self.housekeeping.write_binary_file()
        
        Any phy setup info still needs to be added to this data in the tftp world
        
        '''
       
        retvals = self.housekeeping.create_top_sector(self.type_no, self.ser_no, self.eeprom_size, self.product_name, self.tftp)
        
        if self.tftp:
            return retvals[1]

        return retvals
        
    def write_binary_file(self, binary_list, file_ext='bin'):
        '''
        wrapper for serial_number_tools.write_binary_file()
        
        Populates the sector address in the filename from data in BASETESTINFO
        
        Output file extension defaults to 'bin', but can be provided as an arguement if necessary.
        
        expects binary_list as generated by struct_empty() and add_phy_data()
        
        '''
        self.housekeeping.write_binary_file(self.eeprom_size-1, binary_list, file_ext)
        
    def add_phy_data(self, top_sector_data, some_filename):
        '''
        
        top_sector_data is the basic top sector - module name, type, date info as generated by gen_housekeeping_sector()
        
        filename is the auxilliary data file name and must include any path information
        
        The auxilliary data in the file must be in the following format:
        7FC100 1104
        7FC101 0501
        
        and it must be contiguious - no gaps.
        
        If there are gaps the file must be broken into a nummber of files and 
        each set of auxilliary data added independently
        '''
        self.housekeeping.read_aux_file(some_filename)
        return( self.housekeeping.insert_aux_data(top_sector_data))
        

    def clear_base_struct(self):
        '''
        Clear the base variables for a test re-run
        '''
        self.user_name           = ''
        self.timestamp           = 0.0
        self.start_time          = ''
        self.end_time            = ''
        self.start_rh            = 0.0           # relative humidity at the start of the test
        self.end_rh              = 0.0           # relative humidity at the end of the test
        self.start_temp          = 0.0           # temperature at the start of the test
        self.end_temp            = 0.0           # temperature at the end of the test
        self.assembly_list       = [ ['Component', 'Name', 'Serial Number'] ]
        if not self.import_equipment_file:
            self.equipment_details  = [ ['Function', 'Make', 'Model', 'S/N', 'Last Calibration Date', 'Next Calibration Date'] ]

    def template_results(self):
        '''
        generate the correct result list for the module
        '''
        self.results = []
        for entry in self.test_list:
            entry_list = [entry[0]]
            entry_list.append(False)
            entry_list.append('\tTest not run\t')

            self.results.append(entry_list)


    def show_raw_results(self):
        '''
        map the raw results list to a readable format....
        '''
        msg = ''
        for result in self.results:
            msg += f'\n{result[0]}'

            if result[1] :
                msg += ',\tTrue'
            else :
                msg += ',\tFalse'

            msg += f',\tString: {result[2]}'

        return msg


    def show_base_struct(self):
        '''
        Displays the contents of the base data structure
        '''
        msg = '\n\nGeneric Test Information\n\n'
        msg += f'framework_version      {self.framework_version}'
        msg += f'pickle_file            {self.pickle_file}\n'
        msg += f'test_folder            {self.test_folder}\n'
        msg += f'test_report_folder          {self.test_report_folder}\n'
        msg += f'results_folder         {self.results_folder}\n'
        msg += f'legal_time             {self.legal_time}\n'
        if self.record_temp:                # record the temp and RH, True/False
            msg += 'record_temp            True\n'
        else:
            msg += 'record_temp            False\n'

        if self.record_equipment:          # record the equipment, True/False
            msg += 'record_equipment       True\n'
        else:
            msg += 'record_equipment       False\n'



        if self.pdf_report:                 # Generate a PDF report True/False
            msg += 'pdf_report_test              True\n'
        else:
            msg += 'pdf_report_test              False\n'



        for equipment in self.equipment_list:
            msg += 'Equipment    '
            msg += f'{equipment}\n'
           

        msg += 'Valid Keys:    '
        for key in self.valid_keys:
            msg += f'{key}, '

        msg += '\n'

        msg += '\n\nPDF report Information\n\n'
        msg += f'template_path          {self.template_path}\n'
        msg += f'template_name          {self.template_name}\n'
        msg += f'report_front_page      {self.report_front_page}\n'
        msg += f'report_name            {self.report_name}\n'

        msg += '\n\nTest Specifics\n\n'
        msg += f'user_name:             {self.user_name}\n'
        msg += f'date_time              {self.date_time}\n'
        msg += f'timestamp              {self.timestamp}\n'
        msg += f'start_time             {self.start_time}\n'
        msg += f'end_time               {self.end_time}\n'
        msg += f'test_duration          {self.test_duration}\n'

        msg += '\n\nProduct Details\n\n'
        msg += f'product_name           {self.product_name}\n'
        msg += f'type_no                {self.type_no}\n'
        msg += f'ser_no:                {self.ser_no}\n'

        msg += '\n\nFPGA and Firmware Information\n\n'
        msg += f'fwa                    {self.fwa}\n'
        msg += f'fwa_rev                {self.fwa_rev}\n'
        msg += f'fpga1                  {self.fpga1}\n'
        msg += f'fpga1_rev              {self.fpga1_rev}\n'

        msg += '\n\nCommands Used\n\n'

        msg += f'kc_command             {self.kc_command}\n'
        msg += f'kup_command            {self.kup_command}\n'
        msg += f'ds3_command            {self.ds3_command}\n'
        msg += f'ds3_output_file        {self.ds3_output_file}\n'

        msg += '\n\nEnvironmentals\n\n'

        msg += f'start_rh               {self.start_rh}\n'
        msg += f'end_rh                 {self.end_rh}\n'
        msg += f'start_temp             {self.start_temp}\n'          
        msg += f'end_temp               {self.end_temp}\n'          

        for assembly in self.assembly_list:
            msg += 'Assembly '
            for el in assembly:
                msg += f'{el} '
            msg += '\n'

        temp_list = self.printable_equip_list()
        for details in temp_list:
            msg += 'Equipment Used '
            for el in details:
                msg += f':{el} '
            msg += '\n'

        ix = 1
        for limit in self.humidity_limits:
            msg += f'Humidity Limit {ix} :    {limit:.2f}\n'
            ix += 1

        ix = 1
        for limit in self.temp_limits:
            msg += f'Temperature Limit {ix} : {limit:.2f}\n'
            ix += 1

        msg += '\n\nTest Constants\n\n'

        msg += f'quit_test                {self.quit_test}\n'
        msg += f'report_test              {self.report_test}\n'
        msg += f'run_all_tests             {self.run_all_tests}\n'
        msg += f'equip_split_char      {self.equip_split_char}\n'

        return msg

    def struct_empty(self):
        '''
        Is the class/structure empty?
        '''
        return not self.user_name

    def get_test_time(self):
        '''
        Get the timestamp in seconds:
        '''
        self.start_time  = strftime("%B %d, %Y\t%H:%M:%S")
        self.timestamp  = time()

    def get_run_time(self):
        '''
        get the test run time
        and return it in epoch time
        '''
        self.date_time  = strftime("%B %d, %Y\t%H:%M:%S")
        return time()


    def check_test_time(self):
        '''
        Check the test time: Returns True or False
        Results expected to be generated within 2 hours of run
        '''
        end_ts  = time()
        self.end_time  = strftime("%B %d, %Y\t%H:%M:%S")
        test_duration = end_ts - self.timestamp
        if test_duration > (self.legal_time*60):
            print(f'Error: Test exceeded required run time {self.legal_time} minutes ({test_duration} minutes)\nTest must be completed within required time limit\n\n\nTerminating...\n\n')
            base_test_info_exit()
        self.test_duration =  test_duration


    def gen_log_file_name(self):
        '''
        Generate the log file based on the serial number

        generates a string of the form "<serial_number>.log"
        '''
        log_file_name = f'{self.ser_no}.log'
        return log_file_name

    def fill_equipment_details(self):
        '''
        Takes the equipment in the list read from the ini file (self.equipment_list)
        and appends the serial number/ID to the equipment list.
        serial/ID numbers

        Does not check the date, this happens in test_framework
        
        Equipment list entries are of the form:
        'Function', 'Make', 'Model', 'S/N''
        '''
        
        instrument_ix = 0
        for required_instrument in self.equipment_details:

            r_instrument_function    = required_instrument[0]    
            r_instrument_make        = required_instrument[1]
            r_instrument_model       = required_instrument[2]            
        
        
            if r_instrument_function.upper() != 'FUNCTION':
                
                read_fa_label = True
                while read_fa_label:
                    msg = f'Enter ID Label/Serial Number for\t{r_instrument_function}:{r_instrument_make}:{r_instrument_model}\t:'
                    eq_ser_no = (input(msg).strip()).upper()
                    if eq_ser_no in ('NA', 'N/A') :
                        eq_ser_no = 'N/A'
                    
                    if contains_number(eq_ser_no) or eq_ser_no == 'N/A':
                        # give the opportuity to mess up the serial number/ID
                        msg = 'Is the ID correct? (y/n) '
                        if input(msg).upper() == 'Y':
                            read_fa_label = False
                    else:    
                        print('>>>Error: invalid ID/serial number entered\n')
                        
                self.equipment_details[instrument_ix].append(eq_ser_no)

            # move on to the next instrument in the list
            instrument_ix += 1

    def fill_jig_details(self):
        '''
        Takes the equipment in the list read from the ini file (self.equipment_list)
        and appends the serial number/ID to the equipment list.
        serial/ID numbers

        Does not check the date, this happens in test_framework
        
        Equipment list entries are of the form:
        'Function', 'Make', 'Model', 'S/N''
        '''
        
        
        for jig_ix, required_jig in enumerate(self.jig_details):

            r_jig_name    = required_jig    
        
            if r_jig_name[0].upper() != 'DESCRIPTION':
                
                read_fa_label = True
                while read_fa_label:
                    msg = f'Enter ID Label/Serial Number for\t{r_jig_name}\t:'
                    jig_ser_no = (input(msg).strip()).upper()
                    if jig_ser_no in ('NA', 'N/A') :
                        jig_ser_no = 'N/A'

                    if contains_number(jig_ser_no) or jig_ser_no == 'N/A':                        
                        # give the opportuity to mess up the serial number/ID
                        msg = 'Is the ID correct? (y/n) '
                        if input(msg).upper() == 'Y':
                            read_fa_label = False
                    else:    
                        print('>>>Error: invalid ID/serial number entered\n')
                        
                self.jig_details[jig_ix] = [r_jig_name[0],jig_ser_no]

            # move on to the next instrument in the list
            jig_ix += 1



    def populate_assembly_info(self):
        '''
        Fills in the assembly information
        assembly_list

        For now it assumes top level product
        lower levels tracked through syteline?
        '''        
        assembly = ['Product', self.product_name, self.ser_no]
        if len(self.assembly_list) == 1:
            self.assembly_list.append(assembly)
        else:
            self.assembly_list[1] = assembly


    def printable_equip_list(self):
        '''
        Return the equipment list in a printable format for word
        '''
        result_list = []
        for details in self.equipment_details:
            equip = []
            for el in details:
                if isinstance(el, float):
                    msg = f'{seconds_to_dmy(el)} '
                    equip.append(msg)
                else:
                    equip.append(el)
            result_list.append(equip)
        return result_list


    def printable_test_summary(self):
        '''
        returns a formatted string with a summary of the tester,
        date, equipment and temp/humidity if required
        for text report_test.
        '''

        msg  = f'Product:         {self.product_name}\n'
        msg += f'Serial Number    {self.ser_no}\n'
        msg += f'Test Technician: {self.user_name}\n'
        msg += f'Test Time:       {self.end_time}\n'
        msg += f'Test Run Time:   {int(float(self.test_duration/60))} minutes\n'

        if self.record_temp:
            msg += f'\n\nStart %%RH:       {self.start_rh}\n'
            msg += f'End %%RH:         {self.end_rh}\n'
            msg += f'%RH Limits:      Min {self.humidity_limits[0]}, Max {self.humidity_limits[1]}\n'

            msg += f'Start Temp (C):  {self.start_temp}\n'
            msg += f'End Temp (C):    {self.end_temp}\n'
            msg += f'Temp Limits (C): Min {self.temp_limits[0]}, Max {self.temp_limits[1]}\n'

        if self.record_equipment:
            #self.equipment_details.append(eq_details)
            msg += '\n\nTest Equipment Used\n\n'
            if self.import_equipment_file:
                msg += f'Imported Equipment File: {self.imported_equipment_file}, created {self.equip_file_creation_time}\n\n'
            for equip in self.equipment_details:
                #print (equip)
                #input('hit enter')
                for el in equip:
                    #print(el)
                    if isinstance(el, float):
                        #input('DEBUG: is float: True')
                        msg += f'\t{seconds_to_dmy(el)}'
                    else:
                        msg += f'{el}\t'
                msg += '\n'

        if self.record_equipment:
            #self.equipment_details.append(eq_details)
            msg += '\n\nTest Jigs Used\n\n'
            if self.import_jig_file:
                msg += f'Imported Jig File: {self.imported_jig_file}, created {self.jig_file_creation_time}\n\n'
            for jig in self.jig_details:
                #print (equip)
                #input('hit enter')
                for el in jig:
                    #print(el)
                    if isinstance(el, float):
                        #input('DEBUG: is float: True')
                        msg += f'\t{seconds_to_dmy(el)}'
                    else:
                        msg += f'{el}\t'
                msg += '\n'


        return msg



    ###########################################
    # Test Menu Stuff
    ###########################################
    def main_menu(self, anymenu):
        '''
        Display the menu selection and return the
        user_name selection. e.g.

        ===============================================================================
        =                       DV FrameWork Version: 1.2
        =
        =
        =                       NET/SWI/004 FUNCTIONAL TEST
        ================================================================================

                         1)     Enter Test Info
                         2)     Check Versions
                         3)     Set MAC Address
                         4)     Check IP Address
                         5)     Check Network Ports
                         6)     Check GPS
                         7)     Check Battery Backup
                         8)     Check IRIG
                         9)     Generate Test report_test
                         A)     Run All
                         Q)     quit_test


        Enter selection:
        '''
        py_cls()
        msg  = ''
        msg += '='*79
        msg += f'\n=\t\t\tDV FrameWork Version: {self.framework_version}'
        msg += '\n='
        msg += '\n='
        msg += f'\n=\t\t\t{self.product_name} FUNCTIONAL TEST'
        msg += '\n='
        msg += '='*79
        msg += '\n'
        menu_len = len(anymenu)
        for i in range(menu_len):
            if i == (menu_len-3):
                msg +=f'\n\t\t{self.report_test})\t{anymenu[i][0]}'
            elif i == (menu_len-2):
                msg +=f'\n\t\t{self.run_all_tests})\t{anymenu[i][0]}'
            elif i == (menu_len-1):
                msg +=f'\n\t\t{self.quit_test})\t{anymenu[i][0]}'
            else:
                msg +=f'\n\t\t{(i+1)})\t{anymenu[i][0]}'

        msg += '\n\n'
        print(msg)
        sel = (input('\t\tEnter selection: ')).upper()
        return sel


    def set_valid_keys(self):
        '''
        creates a list of valid test keys for checking against
        '''

        len_opts = len(self.test_list)

        # last 3 options are fixed, R, A, Q
        for ix in range (len_opts):
            if ix < (len_opts -3):
                opt = f'{(ix+1)}'
                self.valid_keys.append(opt)

        self.valid_keys.append('R')  # for report_test Generation
        self.valid_keys.append('A')  # for Run All tests
        self.valid_keys.append('Q')  # for quit_test
        
        
    ###############################
    #
    # End of menu related stuff
    #
    ###############################

    ##########################################
    #
    # Functions related to 'Enter Test Info'
    #
    ##########################################

    def get_user_name(self):
        '''
        Get the user_namename (keyboard input)
        check its length
        note getuser_namename returns empty string if no user_namename found

        user_namename is always a TLA
        '''
        #NAME=popen('getuser_namename').read()
        while(1):
            self.user_name = ((input('Enter your initials: ')).strip()).upper()
            if(len(self.user_name)==3):
                break
            print('>>>Error: invalid intials - enter the correct ones this time\n')

    def get_ser_no(self):
        '''
        Get the getser_no (keyboard input)
        check its length
        note getser_no returns empty string if no serial no found

        Serial no is always of the form AB1234

        '''

        if self.aft_test:
            all_modules_entered = False
            ix = 0
            print ('\n\nINFO: Enter each modules serial number when prompted until all serial numbers are entered')
            print ('INFO: press enter to indicate that all serial numbers have been entered\n\n')
            while not all_modules_entered:
                valid_serial_number = False
                
                while not valid_serial_number:
                    ser_no = ((input(f'Enter Test Module {ix} Module Serial Number: ' )).strip()).upper()
                    if ser_no == '':
                        all_modules_entered = True
                        valid_serial_number = True

                    if not all_modules_entered:
                        if len(ser_no) in (6,7): 
                            if input('Confirm Serial Number (y/n): ').upper() == 'Y':
                                valid_serial_number = True

                                # need to add the serial number to the list here
                                # to avoid a blank entry in the list resulting in 
                                # nonsense tests being run and reported
                                self.aft_ser_nos.append(ser_no)

                        else:        
                            print('>>>Error: invalid serial number - enter a correct one this time\n')
                            print('>>>       legal forms are AB1234 or ABC1234\n')
                ix += 1
        else:
            valid_serial_number = False
            while not valid_serial_number:
                self.ser_no = ((input('Enter Module Serial Number: ' )).strip()).upper()
                if len(self.ser_no) in (6,7): 
                    valid_serial_number = True
                else:     
                    print('>>>Error: invalid serial number - enter a correct one this time\n')
                    print('>>>       legal forms are AB1234 or ABC1234\n')

    def get_rh(self):
        '''
        Get the relative humidity and return it as an float
        '''
        rel_humidity = 0.0

        got_reading = False
        while not got_reading:
            rel_humidity_str = input('\t\tEnter Relative Humidity Reading (%): ' )
            if match('^[0-9\.]*$',rel_humidity_str) and (rel_humidity_str != '.') and (rel_humidity_str.count('.') < 2) and (rel_humidity_str != ''):
                try:
                    rel_humidity = float(rel_humidity_str)
                    if (self.humidity_limits[0] <= rel_humidity <= self.humidity_limits[1]):
                        got_reading = True
                    else :
                        print(f'\n>>>invalid humidity value entered {rel_humidity} Expected integer value in range {self.humidity_limits[0]:.1f} to {self.humidity_limits[1]:.1f}')
                except IOError:
                    print(f'\n>>>invalid humidity value  entered {rel_humidity} Expected integer value in range {self.humidity_limits[0]:.1f} to {self.humidity_limits[1]:.1f}')

        return rel_humidity

    def get_temp(self):
        '''
        Get the relative humidity and return it as an integer % value expected
        '''
        test_temp = 0

        debug = False
        msg = self.show_base_struct()
        show_debug(debug, msg)

        low_limit = self.temp_limits[0]
        high_limit = self.temp_limits[1]

        got_reading = False
        while not got_reading:
            test_temp_str = input('\n\n\t\tEnter Test Environment Temperature(C): ' )
            if match('^[0-9\.]*$',test_temp_str) and (test_temp_str != '.')  and (test_temp_str.count('.') < 2) and (test_temp_str != ''):
                test_temp = float(test_temp_str)
                try:
                    if (low_limit <= test_temp <= high_limit):
                        got_reading = True
                    else :
                        print(f'\n>>>invalid temperature value entered {test_temp} Expected value in range, {low_limit:.1f} to {high_limit:.1f} C')
                except IOError:
                    print(f'\n>>>invalid temperature value entered {test_temp} Expected value in range, {low_limit:.1f} to {high_limit:.1f} C')

        return test_temp


    def rh_table(self):
        '''
        output the humidity limits in a readable format.
        '''

        rh_heading = ['Start RH(%)', 'End RH(%)', 'Lower Limit(%)', 'Upper Limit(%)']
        rh_values  = [self.start_rh, self.end_rh,self.humidity_limits[0], self.humidity_limits[1]]

        env_list = [rh_heading, rh_values]

        return env_list

    def temp_table(self):
        '''
        output the templerature limits in a readable format.
        '''

        temp_heading = ['Start Temp(C)', 'End Temp(C)', 'Lower Limit(C)', 'Upper Limit(C)']
        temp_values = [self.start_temp, self.end_temp, self.temp_limits[0], self.temp_limits[1]]

        env_list = [temp_heading, temp_values]

        return env_list


    def get_test_seq(self, get_name):
        '''
        Method to get sequence number of the function in the test list. Handy if db_test_list being changed.

        Should be run before any test so that the test sequence number is available when the test is being run

        '''
        seq=0 
        for test in self.test_list:
            test_name= test[1]
            if str(get_name)in str(test_name):
                break

            seq += 1
        self.test_seq_number = seq

        
    def set_interim_rpt_path(self, test_name):
        '''
        Standardised code for setting the interim report path and creating the folder if it does not exist
        '''

        if self.test_temperature_point == '':    
            self.interim_rpt_path = sanitise_folder_name(self.test_folder + '/' + self.results_folder + '/' + self.ser_no + '/' + test_name)
        else: 
            self.interim_rpt_path = sanitise_folder_name(self.test_folder + '/' + self.results_folder + '/' + self.ser_no + '/' + self.test_temperature_point)        
            check_folder(self.interim_rpt_path)
            self.interim_rpt_path = sanitise_folder_name(self.interim_rpt_path + '/' + test_name)        
            
        check_folder(self.interim_rpt_path)

    def set_test_name(self,this_test_name):
        '''
        Set the test name if its not already defined at a higher level
        add the temperature 
        '''
        if self.this_test_name == '':
            self.this_test_name = this_test_name 

    def write_interim_report(self, heading, res):
        '''
        Generate the interim test report
        '''
        
        logging.basicConfig(format='%(levelname)s:%(message)s', level=self.logging_level)
        
        # generate the interim test report name
        # temperature information is added at the test level, not here
   
        report_name = f'{self.ser_no}_{self.this_test_name}.txt'        

        int_test_report_name = f'{self.interim_rpt_path}/{report_name}'
        int_test_report_name = sanitise_folder_name(int_test_report_name)
        
        # save it so it can be shared with other functions/methods
        self.int_test_report_name = int_test_report_name
        
        logging.info(f'Creating new results file {int_test_report_name}\n')

        #generate the contents first, and remove the text file type and replace underscores with spaces
        test_title_text = report_name.replace('.txt','')
        rep_contents = section_heading(test_title_text.replace('_',' '))
        rep_contents += f'\n[Module]:{self.product_name}\t\n'
        rep_contents += f'[Serial]:{self.ser_no}\t\n'
        rep_contents += f'[User name]  :{self.user_name}\t\n'
        
        time_msg      = strftime("%B %d, %Y\t%H:%M:%S")
        
        rep_contents += f'[Date]  :{time_msg}\t\n'
        rep_contents += f'[TestName]:{heading}\n\n\n'
        rep_contents += 'Test Result: \n\n'
        rep_contents += f'{res}'

        # now write the test report
        
        chdir(self.interim_rpt_path)
        with open(report_name,'w', encoding='utf-8') as int_test_report:
            int_test_report.write(rep_contents)

        chdir(self.test_folder)
        
        #Associate the interim results with a serial number in a dictionary in the database
        #so that the results can be pulled together when its time to generate the final report
        #
        self.results_dict[self.ser_no][self.test_seq_number][1] = True
        self.results_dict[self.ser_no][self.test_seq_number][2] = int_test_report_name

        # clear the test name in case we are testing over temperature
        # or testing multiple modules (AFT)
        # needs to be done here so that it can't be forgotten about 
        # when writing new tests.
        self.this_test_name = ''        
        
    def read_interim_report(self, report_name):
        '''
        read an interim report and return its contents and whether it has passed or not
        '''
        
        logging.basicConfig(format='%(levelname)s:%(message)s', level=self.logging_level)

        test_passed = False
        test_text   = ''

        with open(report_name, 'r', encoding='utf-8') as test_report:
            logging.debug(f'read_interim_report: opening and reading  {report_name}')
            test_text = test_report.readlines()

        fail_count = 0
        for line in test_text:
            if line.upper().find('FAIL') != -1:
                fail_count += 1
 
        if fail_count == 0:
            test_passed = True

        return [test_text, test_passed]
        

    # Functions relating to the operating system check
    def check_os(self):
        '''
        checks if Windows 7 is being run and configures the tftp_path if its not.
        '''
        self.win7check = False
        if 'Windows-7' in platform():
            self.win7check = True

        if not self.win7check:
            system32 = 'SysNative' if architecture()[0] == '32bit' else 'System32'
            self.tftp_path = path.join(environ['SystemRoot'], system32)  

    def verify_acra_library(self, min_library_version):
        '''
        Verifies the minimum revision of the acra utilities that are required for the test
        to run.
        
        exits with warning message if the version is too early
        '''
        #only run the check if the version is not null
        
        mismatch = False
        if min_library_version != '':
            e_ver_list = min_library_version.split('.')
            e_major = int(e_ver_list[0], 10)
            e_minor = int(e_ver_list[1], 10)
            try:
                e_point = int(e_ver_list[2], 10)
            except ValueError:
                e_point = 0

            # sample version response: 'ACRA UTILS Version : 1.75.2'
            installed_msg = acra_utils_version()
            installed_version_list = installed_msg.split()
            intalled_version = installed_version_list[4]
            
            a_ver_list = intalled_version.split('.')
            a_major = int(a_ver_list[0], 10)
            a_minor = int(a_ver_list[1], 10)
            
            try:
                a_point = int(a_ver_list[2], 10)
            except ValueError:
                a_point = 0
            
            # check the Python version
            # if Pythn 3 major must be 3 or greater
            if sys.version_info[0] < 3:
                mismatch = True
            else:
                if a_major != e_major:
                    mismatch = True

            if (not mismatch):
                if a_minor < e_minor:                    
                    mismatch = True

            if not mismatch and (a_minor == e_minor) :
                    if (a_point < e_point): 
                        mismatch = True

        if mismatch:
            message  = f'\n\nERROR: Installed ACRA Utilities version ({a_major}.{a_minor}.{a_point}) is incorrect for this test'
            message += f'\n\n\tACRA Utilities version ({e_major}.{e_minor}.{e_point}) or later is required'
            message += '\n\n\tInstalled major versions should match the expected makor version'
            print (message)
            base_test_info_exit()

            

    def configure_das(self):
        '''
        Populate the DAS Studio commands required
        Standard location, only the version changes.
        
        Tested and appears to be working correctly
        
        Use the default version if use_latest_das is False, otherwise use the most recent version installed
        '''
        
        if self.use_latest_das:
        
            contents_list = listdir(self.das_root)
            
            folder_list = []
            
            for entry in contents_list:
                if path.isdir(f'{self.das_root}/{entry}'):
                    if len(entry.split('.')) == 3:
                        folder_list.append(entry)
                        

            if len(folder_list) == 0:
                print ('\n\n\n\n')
                print ('ERROR: No valid version of DAS Studio installed')
                print ('/n')
                print ('       Terminating test\n\n\n\n\n')
                base_test_info_exit()
            
            das_dict = {}       # dictionary of versions key is numeric conversion and value is the version string.
            das_list = []
            for folder in folder_list:
                major_rev, minor_rev, point_rev = folder.split('.')
                das_version = int(major_rev)*10000 + int(minor_rev)*100 + int(point_rev)
                das_list.append(das_version)
                das_dict[das_version] = folder

            # re-order the list, in reverse order
            das_list.sort(reverse = True)

            # sort the installed versions such that the highest version is first.
            #das_list.sort(reverse = True)
            highest_rev = das_list[0]

            # convert the min version into an integer for comparison
            major_rev, minor_rev, point_rev = self.das_version.split('.')
            min_das_version = int(major_rev)*10000 + int(minor_rev)*100 + int(point_rev)

            # pick the latest version and see if its recent enough.
            if highest_rev < min_das_version:
                print (f'\n\nERROR: Installed DAS Studio Version {das_dict[highest_rev]}is lower than the minimum {self.das_version} required for this test\n\n')
                base_test_info_exit()

            self.das_version = das_dict[highest_rev]
            
            print (f'configure_das: self.das_version: {self.das_version}')

        self.das_standalone_dir  = self.das_root + '\\' + self.das_version + '\\'
        self.das_standalone_cmd  = self.das_root + '\\' + self.das_version + '\\Program.exe'
        self.das_studio_msg_log  = self.das_root + '\\' + self.das_version + '\\Program\\MsgML\\Program_MsgML.xml'
        
    def golden_fpga(self):
        '''
        Generate the golden FPGA string for each FPGA in the design that can be ISP'd
        '''
        # allow for up to 2 FPGAs/design so that we can ISP them if necessary
        if self.fpga_ix == 0:
            self.golden_fpga1 = self.fpga1.replace('/', '') + 'RX' + self.fpga1_rev
        else:
            self.golden_fpga2 = self.fpga2.replace('/', '') + 'RX' + self.fpga2_rev
    
    def isp_fpga(self):
        '''
        Generate the ISP update FPGA string for each FPGA in the design that can be ISP'd
        '''    
        # allow for up to 2 FPGAs/design so that we can ISP them if necessary
        if self.fpga_ix == 0:
            self.isp_fpga1 = self.isp_fpga1.replace('/', '') + 'RX' + self.isp_fpga1_rev
        else:
            self.isp_fpga2 = self.isp_fpga2.replace('/', '') + 'RX' + self.isp_fpga2_rev
            
    def clean_up(self):
        '''
        
        Moves txt and log files to the \\results folder
        and deletes any .sys, .tmp, .xml, .xid, .v15, .adv diles
        '''
        
        debug = False
        
        # do nothing whilst I'm debugging
        if not debug:
            
            check_folder (self.test_report_folder)
            check_folder (self.results_folder)


            #for root, dirs, files in walk(getcwd()):
            #    for file in files:
            #        for file_extension in [".log", ".txt", ".rpt"]:
            #            if(file.endswith(file_extension)):
            #                #print(os.path.join(root,file))
            #                shutil.move(file, self.results_folder)        

            #move("*.txt", self.results_folder)
            #move("*.log", self.results_folder)
            #move("*.rpt", self.results_folder)
            del_file("*.sys")
            del_file("*.tmp")
            del_file("*.xml")
            del_file("*.xid")
            del_file("*.v15")
            del_file("*.adv")


    def gen_final_report(self):
        '''
        Pulls together the list of interim reports into the final report.

        Figures out whether the test has passed, or not and writes the final report to the report folder
        
        this is common to every test, hence a method of BASETESTINFO.

        '''

        logging.basicConfig(format='%(levelname)s:%(message)s', level=self.logging_level)    

        test_fails = 0

        report_body = ''

        #~~~~~~~~~~~~~~~~~~~~~~~~~
        # standalone/bench test        
        #~~~~~~~~~~~~~~~~~~~~~~~~~
        if not self.aft_test:
            for report in self.results_dict[self.ser_no]:
                if report[0] not in self.excluded_list:
                    report_file = report[2]
                    if report[1] is True:
                        logging.debug(f'2: gen_final_report: Reading Interim Report: {report_file}')
                        text, test_pass_fail = self.read_interim_report(report_file)
                        for line in text:
                            report_body += line
                        report_body += '\n\n' 

                        if not test_pass_fail:
                            test_fails += 1
                    else:
                        report_body += report[0]
                        report_body += report_file
                        report_body += '\n\n'
                        test_fails += 1 

        # AFT/ADAU test
        else:
            #for report in self.results_dict[self.ser_no]:
            #        logging.debug(f'2: gen_final_report: Reading Interim Report: {report}')
            #        text, test_pass_fail = self.read_interim_report(report)
            #        for line in text:
            #            report_body += line
            #        report_body += '\n\n' 
    
            #        if not test_pass_fail:
            #            test_fails += 1
            for report in self.results_dict[self.ser_no]:
                if report[0] not in self.excluded_list:
                    report_file = report[2]
                    if report[1] is True:
                        logging.debug(f'2: gen_final_report: Reading Interim Report: {report_file}')
                        text, test_pass_fail = self.read_interim_report(report_file)
                        for line in text:
                            report_body += line
                        report_body += '\n\n' 

                        if not test_pass_fail:
                            test_fails += 1
                    else:
                        report_body += report[0]
                        report_body += report_file
                        report_body += '\n\n'
                        test_fails += 1 

        #===================================================
        # final result at start and end of the txt report        
        # so it appears on the screen...
        #===================================================
        rpt = report_result(pass_fail(test_fails))

        hdr = report_heading(f'{self.product_name} {self.ser_no} Production Test Report')
        hdr += '\n\n'

        hdr += report_result(pass_fail(test_fails))
        hdr += '\n\n'

        summary  = self.printable_test_summary()
        summary += '\n\n' # formatting
        hdr += report_summary('Test Configuration', summary)
        hdr += '\n\n'

        rpt = hdr + rpt + report_body

        # terminate the report with the result    
        rpt += '\n\n'
        rpt += report_result(pass_fail(test_fails))
        rpt += '\n\n'


        with open(self.report_name, 'w', encoding='utf-8') as report:
            report.write(rpt)

        return rpt

    def pickledata(self):
        '''
        Pickle data. don't care what the data is
        '''
        with open(self.pickle_file, 'wb') as pfile:
            dump(self, pfile)

    def unpickledata(self):
        '''
        UnPickle data. don't care what the data is
        '''
        with open(self.pickle_file, 'rb')as pfile:
            struct = load(pfile)
        return struct

    def prepare_archive_lists(self, report_folder):
        '''
        Common code for preparing lists of files to be archived
        uses self.ser_no to create keys for the lists.
        '''
        # create an dictionary of empty lists for the interim reports associated with each serial number
        # and another of files that we want to copy to the server
        # serial number is the key.
        self.results_dict[self.ser_no] = []
        self.files_to_copy_dict[self.ser_no] = []

        results_folder = sanitise_folder_name(f'{self.test_folder}/{self.results_folder}/{self.ser_no}')

        #backup up the results folder if it already exists
        # and add it to the list of files to be copied
        previous_results_zip = time_stamp_results_folder(results_folder)

        # create the test results folder if it doesn't exist or has been backed up
        check_folder(results_folder)

        report_name = sanitise_folder_name(f'{report_folder}/{self.ser_no}_TEST_REPORT.TXT')
        last_report = time_stamp_report(report_name)

        # create a list of files to be copied to the server
        self.files_to_copy_dict[self.ser_no].append(report_name)

        # this one is not zipped, yet
        zip_name = f'{results_folder}.tar.gz'
        self.files_to_copy_dict[self.ser_no].append(zip_name)

        if path.exists(last_report):
            self.files_to_copy_dict[self.ser_no].append(last_report)

        if path.exists(previous_results_zip):
            self.files_to_copy_dict[self.ser_no].append(previous_results_zip)

        # generate a PDF report if we need it, and add it to the archive list
        if self.pdf_report:            
            report_name = sanitise_folder_name(f'{report_folder}/{self.ser_no}_TEST_REPORT.PDF')
            last_report = time_stamp_report(report_name)    
            self.files_to_copy_dict[self.ser_no].append(last_report)
            
    def backup_module_data(self):
        '''
        takes the list of files to be backed up for each module and copies them to the server.

        fall back system(copy xxx yyy) left in place just in case there are any problems.

        '''
        if path.exists(self.network_backup_folder):
            network_folder = f'{self.network_backup_folder}/{self.ser_no}'
            self.check_network_folder(network_folder)
            for item in self.files_to_copy_dict[self.ser_no]:
                if path.exists(item):
                    copy(item, network_folder) # SRM Update
                else:    
                    logging.warning(f'Source file/folder not found {item}') 
        else:
            logging.warning(f'Destination folder not found {self.network_backup_folder}')      

    def check_network_folder(self, folder_name):
        '''
        Check that the folder exists, if it doesn't create it

        returns:

        0: folder already exists
        1: Successfully created the folder
        2: failed to create folder.

        python likes unix file names. the filename is converted to unix style before anything happens

        Only works with folders, not files
        '''

        if not path.exists(folder_name):
            try:
                print('\n\t<Debug: check_network_folder> mkdir(folder_name)')
                mkdir(folder_name)
                return 1
            except IOError as check_error:
                print(f'ERROR: Unable to create folder {folder_name}')
                print(f'ERROR: full path {local_folder_name}')
                print(check_error)
                return 2
        else:
            return 0

    def configure_results_for_standalone(self, test_name):
        '''
        
        Sets up the results dictionary for running tests stand alone when being
        initially developed and tested, outside of the test framework
        
        The serial number needs to be populated before this method is called
        
        '''
        self.results_dict[self.ser_no]= [[[], [], []]]
        
        self.this_test_name = test_name

        report_name = f'{self.ser_no}_{self.this_test_name}.txt'        
        self.interim_rpt_path = f'./{self.this_test_name}'

        self.int_test_report_name = f'{self.interim_rpt_path}/{report_name}'    

        self.test_seq_number = 0            # 0 because the 
        self.results_dict[self.ser_no][self.test_seq_number][1] = True
        self.results_dict[self.ser_no][self.test_seq_number][2] = self.int_test_report_name  
        

    def print_results_dict(self):
        '''
        Prints out the results_dict contents in a neat format for debugging.
        Requires 'hit enter' to continue/exit, so purely for debug.
        '''
        
        for key, value in self.results_dict.items():
            print (f'Key: {key}\n')
            for list_entry in value:
                print (list_entry)
            print ('\n\n')
            
        input('End of self.results_dict\tHit Enter to continue')


######################################################
# End of class stuff, now for the special functions
######################################################
#
# Pickling and unpickling stuff
#
######################################################
#
#
#def pickledata(anystruct):
#    '''
#    Pickle data. don't care what the data is
#    '''
#    with open(anystruct.pickle_file, 'wb') as pfile:
#        dump(anystruct, pfile)
#
#def unpickledata(anystruct):
#    '''
#    UnPickle data. don't care what the data is
#    '''
#    with open(anystruct.pickle_file, 'rb')as pfile:
#        struct = load(pfile)
#    return struct

