'''
=============================================================================

 Name:     git_tools.py

 Author:   Simon Moran/Chris Gilbert

 Date:     November 8, 2022
 
 Copyright Curtiss Wright 2022

 Description

 Tools for interfacing to git/gitlab.
 Code copied from test_executive code and built into a 
 dedicated class/module
 
 8/11/2022:     CJG First release

=============================================================================
'''

#from os import path, getcwd, sys, chdir, environ, listdir          #system
#from shutil import copy
#from platform import platform, architecture
#from time import time, strftime    # sleep

#from sys import exit as base_test_info_exit    #stdout, argv removed from list

 

#from acra.test_framework import check_folder, del_file, py_cls, sanitise_folder_name
#from acra.common_functions import time_stamp_results_folder, time_stamp_report, contains_number

## libraries required for PDF generation
#from acra.reporting_tools import seconds_to_dmy, show_debug, section_heading, report_heading, report_summary, report_result, pass_fail     # section_result, gen_pdf_report
#from acra import serial_number_tools
#from acra.version import version as acra_utils_version


class TestIntegrityTools :
    '''
    Class for test integrity tools.
    
    Currently copies and verifies that the copy was successful.
    Created as a separate class as we may want/need to add additional 
    integrity check tools,e.g. git tag, or clone ID, or some other form of 
    integrity/version checks in the future.
    
    '''

    def __init__(self, sourcepath, dest_path):
        '''
        Base structure for git tool class.
        '''

        # test information
        self.sourcepath = sourcepath
        self.dest_path  = dest_path

        # default values for variables used internally by the class
        self.test_root  = 'C:\Acra\Test'
        self.use_git    = True              # default to expecting to use GIT


    def copy_test_code(self):
        '''
        looks to see if the test code exists on Node1.
        If it does it just copies the code directly from Node1 locally
        if not it looks on the GIT server for it and clones the project files locally.
        
        returns bool 
        
            True:  thinks the copy was successful
            False: the test code was not found.
            
        '''

        success = True      # assume everything works, until we learn otherwise
    
        # git or network path?
        if (r'dubgit01' in self.sourcepath):
                # on dubgit01
                os.chdir(rf'{{self.test_root}}')
                
                # Get the current working directory
                cwd = os.getcwd()
                # Print the current working directory
                print("Current working directory: {0}".format(cwd))

                cmd = f'git clone {self.sourcepath}'

                print('\n\t<Debug in copyTestCode> cmd is %s.....\n\n'%cmd)
                os.system(cmd)
    
        elif path.exists(self.sourcepath):
            # Not on Node1, so must be on DubGit01...
            self.use_git = False
            shutil.copytree(self.sourcepath,self.destpath)

        else:
            print (f'\n\n\nERROR: Unable to locate test source code...\n\n{self.sourcepath}\n\n\n')            
            success = False            

        return success
            

    def verify_copy_of_test_code(self):
        '''
        Confirm that the test was copied/cloned successfully
        '''

        if self.use_git:
        
            remote_reg = 'remote_reg.txt'
            local_reg  = 'local_reg.txt'

            # get the remote repositories information 
            cmd = (f'git ls-remote {self.sourcepath} > remote_reg.txt')
            os.system(cmd)
            
            cmd = (f'git ls-remote {self.dest_path} > local_reg.txt') # get the local repositories information 
            os.system(cmd)

            with open(remote_reg, 'r') as f:
                lines = f.read().splitlines()
                remote_reg_last_line = lines[-1]

            with open(local_reg, 'r') as f:
                lines = f.read().splitlines()
                local_reg_last_line = lines[-1]
          
            if remote_last_line == local_last_line:
                messagebox.showinfo("Good News Everybody","Test Code Copy was successful")
            else:
                messagebox.showerror("Error","Test Code Copy Failed")
 
        else:
            files_source = os.listdir(self.sourcepath)
            files_dest = os.listdir(dest_dir)
            
            dif_list = list(set(files_source) - set(files_dest))

            if len(dif_list) > 0:
                print('Test Code Copy to Production Machine FAILED')
                #messagebox.showerror("Error","Test Code Copy Failed")
            else:
                print('Test Code Copy to Production Machine was successful')
                #messagebox.showinfo("Good News Everybody","Test Code Copy was successful")


if __name__ == '__main__':

    sourcepath = 'http://dubgit01.INT.CW.LOCAL/DesignVerification/example_production_test.git'
    dest_path = 'C:/ACRA/Test/COPQ_Libraries/'

    test_integrity = TestIntegrityTools(sourcepath, dest_path)
    
    test_integrity.copy_test_code()
    test_integrity.verify_copy_of_test_code()
    