'''
ModeSwitcher.py
*************************************************************************************
Created by:         Chris Gilbert   [07 Jan 2022]
Last modified by:   
Version:            1.0

Description:        Python library to execute Format/Mode switching in conjunction
                    with an Arduino based jig. The jig has not been officially built yet, 
                    hence the name is not included here.
                    
Company:            Curtiss Wright, ACRA Business Unit

Copyright           2022

*************************************************************************************

Version History:
[1.0] Initial version.  Tested in Python 2.7, 3,7 and 3.9. Code is version agnostic.


'''

import serial

class ModeSwitcher():
    '''
    CLass to support mode switching using Arduino based jig.
    '''
    def __init__(self, port_id):
        '''
        initialise the serial port - basic, limited error checking

        ser = serial.Serial('COM3', 38400, timeout=0, parity=serial.PARITY_EVEN, rtscts=1)
        '''
        try:
            self.port = serial.Serial(port_id, 9600, timeout = 3)
        except serial.SerialException as ser_exc:
            print (f'ERROR: unable to open port {port_id}')
            print (ser_exc)

    def set_format(self, chs_format):
        '''
        format is an integer in the range 0 to 15, which is then
        transmitted to the Arduino as part of a formatted message

        limited error checking
        '''
       
        if chs_format not in range (0, 16):
            print (f'ERROR: invalid format {chs_format} selected') 
            # debug
        else:
            msg = f'FMT:{chs_format}\n'
            print (f'DEBUG sending: {msg}')
            self.port.write(msg.encode('UTF-8'))

    def read_resp(self):
        '''
        Simple sanity check to read whatever is returned by the Arduino
        '''
        print(self.port.readline())

    def close(self):
        '''
        Shut the port down
        '''
        self.port.close()
            
    def selftest(self):
        '''
        Simple test function to confirm operation of format switching system
        '''            
        for chs_format in range (0, 17):
            if chs_format > 15:
                test_format = 0
            else:     
                test_format = chs_format
            print (f'Format {test_format}')
            self.set_format(test_format)
            print ('Debug Resp:')
            self.read_resp()
            #sleep(2)
            input('hit enter to continue')

if __name__ == "__main__":

    #modetest = ModeSwitcher('/dev/ttyACM0')     # Linux format
    modetest = ModeSwitcher('com3')              # Windows format
    modetest.selftest()
    modetest.close()
    print ('ModeSwitcher: Selftest complete')
    
