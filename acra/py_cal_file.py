'''
#-------------------------------------------------------------------------------
# Name:        py_cal_file
# Purpose:
#
# Author:      CSomers
#
# Created:     07/06/2013
# Copyright:   (c) CSomers 2013
# Licence:     <your licence>
#
# Updated:      19/09/2020
# By:           VVB
# Reason:       To operate under Python 3
#-------------------------------------------------------------------------------
'''
import sys

class PyCalFile:
    """
    This class opens the binary file written by kfileCal and converts it to
    either an ascii file or pure binary file. You can also swap the bytes
    """
    def __init__(self,ip_file,op_file,text_mode,byte_swap): #string, string, bool, bool
        self.success = False
        self.ip_file_name = ip_file
        self.op_file_name = op_file
        self.text_mode = text_mode
        self.byte_swap = byte_swap
        self.base_address = 0
        self.data = []
        self.read_ip_file()
        self.get_base_address()
        self.get_data()
        self.write_op_file()
        self.success = True
        self.bin_values = ''

    def read_ip_file(self):
        '''
        Read in the binary calibration file
        '''
        try:
            with open(self.ip_file_name, mode ='rb', encoding=None) as self.ip_file:
                self.bin_values = self.ip_file.read()
        except IOError:
            sys.exit(f'Could not open input file {self.ip_file_name}')


    def get_base_address(self):
        '''
        Figure out the base address
        '''
        for index in range(3):
            self.base_address = (self.base_address <<8) + int(self.bin_values[2-index])

    def get_data(self):
        '''
        translate the binary data into readable form
        '''
        for index in range(12,len(self.bin_values),2):
            if not self.byte_swap:
                word = (int((self.bin_values[index+1]) <<8)) + (int(self.bin_values[index]))
            else:
                word = (int(self.bin_values[index]) <<8) + (int(self.bin_values[index+1]))
            self.data.append(word)

    def write_op_file(self):
        '''
        Write the deconstructed cal file as a regular eeprom text file
        '''
        if not self.text_mode:
            with open(self.op_file_name, mode = 'wb', encoding='latin-1') as self.op_file:
                # Convert that to a string to write it as a binary file.
                self.b_data = ''
                for datum in self.data:
                    msb = datum >>8
                    lsb = datum & 0x00FF
                    self.b_data = self.b_data +f'{msb:c}'
                    self.b_data = self.b_data +f'{lsb:c}'
                self.op_file.write(self.b_data.encode('latin-1'))
                self.op_file.flush()
        else:
            with open(self.op_file_name, 'w', encoding='utf-8') as self.op_file:
                for ix, datum in enumerate(self.data):
                    self.op_file.write(f'{self.base_address+ix:06X} {datum:04X}\n')
        
  
if __name__ == '__main__':
    _ = PyCalFile("ZC1805.cal", "ZC1805.txt",True,False)
    _ = PyCalFile("ZC1805.cal", "ZC1805.bin",False,False)
  