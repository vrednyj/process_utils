'''
=============================================================================

 Name:     test_framework.py

 Author:   Chris Gilbert

 Date:     March 25, 2022
 
 Copyright Curtiss Wright 2022

 Description

 Python 3 Library functions for menu based production tests
 
 Refer to the release notes and help(PT_Framework) for feature list
 
 Made mostly PEP8 compliant

=============================================================================
'''
import configparser

from os import path, getcwd, system, mkdir, remove
from shutil import move, copy


#from platform import platform, architecture
from time import time, ctime    # sleep, mktime, , strptime
from datetime import date                   # datetime, 
from sys import exit as framework_exit  # stdout, argv removed from list

from copy import deepcopy

import logging

from acra.common_functions import not_in_range

# libraries required for PDF generation
# section_result, gen_pdf_report
from acra.reporting_tools import show_debug, gen_pdf_report
from acra.tar_tools import tar_folder
from acra.equipment_cal_tools import get_equ_cal_info, check_equipment_in_cal, get_jig_cal_info, check_jigs_in_date


def get_base_test_info(anyinfo, test_inifile):
    '''
    Read in the basic test constraints from an INI file. Test will fail if the INI file is not passed.
    '''

    ini_file_used = True
    try:
        ini_file_name = test_inifile
    except IOError:
        print('No configuration file passed')
        ini_file_used = 'False'
        framework_exit('\n\nTerminating...\n\n')

    if ini_file_used:
        ini_file = getcwd()+rf'.\{ini_file_name}'
        if not path.exists(ini_file):
            framework_exit(
                f'The configuration file {ini_file} does not exist\n\nTerminating...')

        config_file_handle = configparser.ConfigParser()
        config_file_handle.read(ini_file_name)

        anyinfo.product_name = config_file_handle.get(
            'Generic Test Settings', 'Product Name')
        anyinfo.pickle_file = config_file_handle.get(
            'Generic Test Settings', 'Pickle File')

        if config_file_handle.has_option('Generic Test Settings', 'Use Fixed DAS Studio Version'):
            fixed_ver = config_file_handle.get(
                'Generic Test Settings', 'Use Fixed DAS Studio Version')
            if fixed_ver.upper() == "TRUE":
                anyinfo.use_latest_das = False
            else:
                anyinfo.das_version = config_file_handle.get(
                    'Generic Test Settings', 'DAS Studio Version')
                # now that we have the version configure DAS Studio paths etc
                anyinfo.configure_das()

        anyinfo.results_folder = config_file_handle.get(
            'Generic Test Settings', 'Results Folder')

        # if upper(config_file_handle.get('Generic Test Settings','Record Temp')) == 'FALSE':
        if (config_file_handle.get('Generic Test Settings', 'Record Temp')).upper() == 'FALSE':
            anyinfo.record_temp = False
        else:
            anyinfo.record_temp = True

        if (config_file_handle.get('Generic Test Settings', 'Record Equip')).upper() == 'FALSE':
            anyinfo.record_equipment = False
        else:
            anyinfo.record_equipment = True

        if (config_file_handle.get('Generic Test Settings', 'PDF Report')).upper() == 'FALSE':
            anyinfo.pdf_report = False
        else:
            anyinfo.pdf_report = True

        if config_file_handle.has_option('Generic Test Settings', 'Report Folder'):
            anyinfo.test_report_folder = config_file_handle.get(
                'Generic Test Settings', 'Report Folder')

        # aft test yes/no. Value defaults to 'False'
        if config_file_handle.has_option('Generic Test Settings', 'AFT Test'):
            if config_file_handle.get('Generic Test Settings', 'AFT Test').upper() == 'TRUE':
                anyinfo.aft_test = True

        # populate the path to the calibration database if the option exists in the INI file
        if config_file_handle.has_option('Generic Test Settings', 'Cal Database Folder'):
            anyinfo.path_to_cal_database = config_file_handle.get('Generic Test Settings', 'Cal Database Folder')


        if anyinfo.record_temp:

            humidity_limits = (config_file_handle.get(
                'Generic Test Settings', 'Humidity Limits').strip()).split(',')
            for el in humidity_limits:
                anyinfo.humidity_limits.append(float(el))

            temp_limits = (config_file_handle.get(
                'Generic Test Settings', 'Temperature Limits').strip()).split(',')
            for el in temp_limits:
                anyinfo.temp_limits.append(float(el))

        if anyinfo.record_equipment:
            raw_equipment = config_file_handle.get('Generic Test Settings', 'Equipment Required')
            raw_equipment_list = raw_equipment.strip().split(',')
            for instrument in raw_equipment_list:
                anyinfo.equipment_details.append(instrument.split(':'))

            raw_jigs = config_file_handle.get('Generic Test Settings', 'Jigs Required')
            raw_jig_list = raw_jigs.strip().split(',')
            
            #print ('DEBUG: raw_jig_list')
            
            #for item in raw_jig_list:
            #    print (item)
            
            #print ('DEBUG: \n\nanyinfo.jig_details now\n\n')            

            for jig in raw_jig_list:
                jig_entry = [jig]
                anyinfo.jig_details.append(jig_entry)                

            #for jig_entry in anyinfo.jig_details:
            #    print (jig_entry)

            #input ('DEBUG: Hit enter')

        py_cls()

        print('\n\n')
        print(config_file_handle.get('Generic Test Settings', 'Max Run Time'))
        print('\n\n')

        anyinfo.legal_time = float(config_file_handle.get(
            'Generic Test Settings', 'Max Run Time'))
        anyinfo.type_no = int(config_file_handle.get(
            'Generic Test Settings', 'Type Number'), 16)

        # check the minimum library version and exit if its not installed
        anyinfo.verify_acra_library(config_file_handle.get(
            'Generic Test Settings', 'Minimum ACRA Utils Version'))

        if config_file_handle.has_option('Generic Test Settings', 'Network Backup'):
            if config_file_handle.get('Generic Test Settings', 'Network Backup').upper() == 'TRUE':
                anyinfo.network_backup = True

        # if network backup is true  we must have a network backup folder
        if anyinfo.network_backup:
            network_backup_folder = sanitise_folder_name(config_file_handle.get('Generic Test Settings', 'Network Backup Folder'))
            anyinfo.network_backup_folder = f'/{network_backup_folder}'
            print (f'\n<Debug> network_backup_folder is : {anyinfo.network_backup_folder}')

        # report_test details
        anyinfo.template_path = config_file_handle.get(
            'Report Settings', 'Template Path')
        anyinfo.template_name = config_file_handle.get(
            'Report Settings', 'Template Name')
        anyinfo.report_front_page = config_file_handle.get(
            'Report Settings', 'Report Title Page')
        anyinfo.report_folder = config_file_handle.get(
            'Report Settings', 'Test Report Folder')

        anyinfo.fwa = config_file_handle.get(
            'Firmware Settings', 'Firmware Name')
        anyinfo.fwa_rev = config_file_handle.get(
            'Firmware Settings', 'Firmware Version')
        anyinfo.fpga1 = config_file_handle.get('FPGA Settings', 'FPGA Name')
        anyinfo.fpga1_rev = config_file_handle.get(
            'FPGA Settings', 'FPGA Version')

        anyinfo.kc_command = config_file_handle.get(
            'Command Settings', 'KC command')
        anyinfo.kup_command = config_file_handle.get(
            'Command Settings', 'KUP command')
        anyinfo.ds3_command = config_file_handle.get(
            'Command Settings', 'DS3 command')
        anyinfo.ds3_output_file = config_file_handle.get(
            'Command Settings', 'DS3 Output file')

        if config_file_handle.has_option('Command Settings', 'tftp Support'):
            anyinfo.tftp = True

        if config_file_handle.has_option('Command Settings', 'EEPROM Sector Size'):
            anyinfo.eeprom_size = int(config_file_handle.get(
                'Command Settings', 'EEPROM Sector Size'))


def enter_test_equipment(anyinfo):
    '''
    Replaces inline code to either import an equipment list from a file or for it to be entered manually.

    the list is of the form:
    Instrument type, Manufacturer, Model, FA or Serial number, next calibration date
    Power Supply,TTI,PL330D, FA1234, 01/01/2023
    Digital Multi Meter,Keithley,2700, FA2345, 01/01/2023

    i.e. all entries separated by a comma and terminated by '\n'
    and is in the same format as 

    self.equipment_details = [ ['Function', 'Make', 'Model', 'S/N'] ]

    '''

    if anyinfo.logging_level != logging.DEBUG:
        py_cls()

    prompt_msg = "\n\nImport Equipment list containing ID/Serial Numbers (y/n)?\t"

    import_equip_yes_no = input(prompt_msg)

    if (import_equip_yes_no.upper() == 'Y'):
        anyinfo.import_equipment_file = True
        enter_equipment = True
        while (enter_equipment):
            equip_file_name = input(
                '\n\nEnter the full name of the equipment file  (eg "equip.txt"):\t ')

            if path.exists(equip_file_name):
                validate_import_equipment_file(anyinfo, equip_file_name)
                enter_equipment = False
            else:
                input(
                    '\n\nERROR: The specified file has NOT been found. Please check the file name before continuing')

    else:
        anyinfo.import_equipment_file = False
        print('\n\nYou have chosen to input the data manually...')
        anyinfo.fill_equipment_details()


    # time to get calibration information and to check dates.
    # replace the equipment details with the updated details list, which includes calibration dates.
    anyinfo.equipment_details = get_equ_cal_info(anyinfo.equipment_details, anyinfo.path_to_cal_database)
    
    if not check_equipment_in_cal(anyinfo.equipment_details):
        print ('n\n\n\n\n')
        print ('ERROR: One or more pieces of equipment are out of calibration')
        print ('       Send the offending equipment for calibration\n')
        print ('       Terminating\n\n\n\n\n')
        framework_exit()

def enter_test_jigs(anyinfo):
    '''
    Import a list of jigs from a file or enter it manually.

    the list is of the form:
    Description, ID
    'ASD/MTS/002/00', 'OPS0299',
    'JIG/MBM/102/00', 'AAD0835'

    i.e. all entries separated by a comma and terminated by '\n'
    and is in the same format as 

    self.jig_details = [ ['Description', 'ID'] ]

    '''

    if anyinfo.logging_level != logging.DEBUG:
        py_cls()

    prompt_msg = "\n\nImport List of jigs containing ID/Serial Numbers (y/n)?\t"

    import_jigs_yes_no = input(prompt_msg)

    if (import_jigs_yes_no.upper() == 'Y'):
        anyinfo.imported_jig_file = True
        enter_jigs = True
        while (enter_jigs):
            jig_file_name = input(
                '\n\nEnter the full name of the jig list file  (eg "jigs.txt"):\t ')

            if path.exists(jig_file_name):
                validate_import_jig_file(anyinfo, jig_file_name)
                enter_jigs = False
            else:
                input(
                    '\n\nERROR: The specified file has NOT been found. Please check the file name before continuing')

    else:
        anyinfo.import_jig_file = False
        print('\n\nYou have chosen to input the data manually...')
        anyinfo.fill_jig_details()


    # time to get calibration information and to check dates.
    # replace the jig details with the updated details list, which includes test dates.


    #print ('DEBUG: anyinfo.jig_details')
    #print (anyinfo.jig_details)
    #input ('DEBUG: hit enter')


    anyinfo.jig_details = get_jig_cal_info(anyinfo.jig_details, anyinfo.path_to_cal_database)
    
    if not check_jigs_in_date(anyinfo.jig_details):
        print ('n\n\n\n\n')
        print ('ERROR: One or more pieces of equipment are out of calibration')
        print ('       Send the offending equipment for calibration\n')
        print ('       Terminating\n\n\n\n\n')
        framework_exit()



def validate_import_equipment_file(anyinfo, equipment_filename):
    '''
    This function checks the following
    - That the file was created in the last 12 hours
    - That all the equipment listed in the INI file is contained in the file being imported, ie nothing is missing
    - That the calibration dates of all the equipment listed are vaild

    '''

    # Checking if time file was created is valid
    hrs_equip_lst_valid = 12  # should be set to 12
    seconds_valid = hrs_equip_lst_valid * 3600
    now = time()

    # this is definitely clunky
    #today = date.today()
    #today_string = today.strftime("%d-%m-%Y")
    #dt_today = datetime.strptime(today_string, "%d-%m-%Y")

    file_creation_time_secs = path.getctime(equipment_filename)
    anyinfo.equip_file_creation_time = ctime(file_creation_time_secs)

    # if timestamp on file is valid, import the data from the text file
    if now < (file_creation_time_secs + seconds_valid):
        print('\n\nEquipment file creation time is valid')
        print('\n\n')     # formatting

        anyinfo.imported_equipment_file = equipment_filename

    else:
        print(
            f'\nERROR: The Equipment file is more than {hrs_equip_lst_valid} hours old\nPlease Create a new equipment file')
        framework_exit('Test Equipment file is invalid')

    with open(equipment_filename, 'r', encoding='utf-8') as equipment_details:
        actual_imported_list = equipment_details.readlines()

    # create a list of lists for the equipment
    imported_equipment_list = []

    # ignore blank lines and lines prefaced with '#'
    for element in actual_imported_list:
        if element != '':
            if element[0] != '#':
                #print (f'DEBUG: element {element}')
                stripped_equipment = element.strip()
                element_fields = stripped_equipment.split(',')
                imported_equipment_list.append(element_fields)

    # now we have a list of lists, a list of all the equipment and a list of parameters for each piece of equipment
    # in the same format as the base_test_info equipment list
    # Now every element in the INI is cross checked with the imported file for each instance of "Function", "Make" and "Model"

    #anyinfo.equipment_details = [ ['Function', 'Make', 'Model', 'S/N'] ]

    #print (f'DEBUG: anyinfo.equipment_details: {anyinfo.equipment_details}')

    instrument_ix = 0   # this is the index into anyinfo.equipment_details
    for required_instrument in anyinfo.equipment_details:

        instrument_match = False

        # convert everything to uppercase before performing any comparisons
        r_instrument_function = required_instrument[0].upper()
        r_instrument_function = r_instrument_function.replace(' ', '')
        r_instrument_function = r_instrument_function.replace('\t', '')
        
        r_instrument_make = required_instrument[1].upper()
        r_instrument_make = r_instrument_make.replace(' ', '')
        r_instrument_make = r_instrument_make.replace('\t', '')
        
        r_instrument_model = required_instrument[2].upper()
        r_instrument_model = r_instrument_model.replace(' ', '')
        r_instrument_model = r_instrument_model.replace('\t', '')

        #print (f'DEBUG: required_instrument: {required_instrument}')

        if r_instrument_function != 'FUNCTION':
            for supplied_instrument in imported_equipment_list:
                #print ('DEBUG')
                #print (supplied_instrument)
                #print (f'DEBUG: len(supplied_instrument): {len(supplied_instrument)}')

                if len(supplied_instrument) >= 4:

                    s_instrument_function  = supplied_instrument[0].upper()
                    s_instrument_function  = s_instrument_function.replace(' ', '')
                    s_instrument_function  = s_instrument_function.replace('\t', '')

                    s_instrument_make      = supplied_instrument[1].upper()
                    s_instrument_make      = s_instrument_make.replace(' ', '')
                    s_instrument_make      = s_instrument_make.replace('\t', '')

                    s_instrument_model     = supplied_instrument[2].upper()
                    s_instrument_model     = s_instrument_model.replace(' ', '')
                    s_instrument_model     = s_instrument_model.replace('\t', '')

                    s_instrument_serial_no = supplied_instrument[3].upper()
                    s_instrument_serial_no = s_instrument_serial_no.replace(' ', '')
                    s_instrument_serial_no = s_instrument_serial_no.replace('\t', '')

                    if r_instrument_function == s_instrument_function:
                        #print (f'DEBUG: Match: r_instrument_function {r_instrument_function} == s_instrument_function {s_instrument_function}')
                        if (r_instrument_make == s_instrument_make):
                            #print (f'DEBUG: Match: r_instrument_make {r_instrument_make} == s_instrument_make {s_instrument_make}')
                            if (r_instrument_model == s_instrument_model):
                                #print (f'DEBUG: Match: r_instrument_model {r_instrument_model} == s_instrument_model {s_instrument_model}')
                                instrument_match = True
                                break   # exit inner loop if we get a match
                    #        else:
                    #            print (f'DEBUG: NO Match: r_instrument_model {r_instrument_model} == s_instrument_model {s_instrument_model}')
                    #    else:    
                    #        print (f'DEBUG: NO Match: r_instrument_make {r_instrument_make} == s_instrument_make {s_instrument_make}')
                    #else: 
                    #    print (f'DEBUG: NO Match: r_instrument_function {r_instrument_function} == s_instrument_function {s_instrument_function}')

            if instrument_match:
                anyinfo.equipment_details[instrument_ix].append(s_instrument_serial_no)

            # if the instrument was not found exit
            else:
                print(f'Error:    Equipment {required_instrument[0]}:{required_instrument[1]}:{required_instrument[2]} not found in supplied equipment list\n\nTerminating...\n\n')
                framework_exit()

        # next instrument in the required equipment list
        instrument_ix += 1



def validate_import_jig_file(anyinfo, jig_filename):
    '''
    This function checks the following
    - That the file was created in the last 12 hours
    - That all the equipment listed in the INI file is contained in the file being imported, ie nothing is missing
    - That the calibration dates of all the equipment listed are vaild

    '''

    # Checking if time file was created is valid
    hrs_equip_lst_valid = 12  # should be set to 12
    seconds_valid = hrs_equip_lst_valid * 3600
    now = time()

    # this is definitely clunky
    #today = date.today()
    #today_string = today.strftime("%d-%m-%Y")
    #dt_today = datetime.strptime(today_string, "%d-%m-%Y")

    file_creation_time_secs = path.getctime(jig_filename)
    anyinfo.jig_file_creation_time = ctime(file_creation_time_secs)

    # if timestamp on file is valid, import the data from the text file
    if now < (file_creation_time_secs + seconds_valid):
        print('\n\nEquipment file creation time is valid')
        print('\n\n')     # formatting

        anyinfo.imported_jig_file = jig_filename

    else:
        print(
            f'\nERROR: The jig file is more than {hrs_equip_lst_valid} hours old\nPlease Create a new equipment file')
        framework_exit('Test jig file is invalid')

    with open(jig_filename, 'r', encoding='utf-8') as jig_details:
        actual_imported_list = jig_details.readlines()

    # create a list of lists for the equipment
    imported_jig_list = []

    # ignore blank lines and lines prefaced with '#'
    for element in actual_imported_list:
        if element != '':
            if element[0] != '#':
                #print (f'DEBUG: element {element}')
                stripped_jig = element.strip()
                element_fields = stripped_jig.split(',')
                imported_jig_list.append(element_fields)

    # now we have a list of lists, a list of all the jigs and a list of parameters for each piece of jig
    # in the same format as the base_test_info equipment list
    # Now every element in the INI is cross checked with the imported file for each instance of "Description"

    #jig_ix = 0   # this is the index into anyinfo.equipment_details
    for jig_ix, required_jig in enumerate(anyinfo.jig_details):
        if jig_ix != 0:

            jig_match = False

            # convert everything to uppercase before performing any comparisons
            r_jig_name = required_jig[0].upper()
            r_jig_name = r_jig_name.replace(' ', '')
            r_jig_name = r_jig_name.replace('\t', '')

            if r_jig_name != 'DESCRIPTION':
                for supplied_jig in imported_jig_list:
                    if len(supplied_jig) >= 2:

                        s_jig_name  = supplied_jig[0].upper()
                        s_jig_name  = s_jig_name.replace(' ', '')
                        s_jig_name  = s_jig_name.replace('\t', '')

                        s_jig_id = supplied_jig[1].upper()
                        s_jig_id = s_jig_id.replace(' ', '')
                        s_jig_id = s_jig_id.replace('\t', '')

                        if r_jig_name == s_jig_name:
                            jig_match = True
                            break   # exit inner loop if we get a match
                        #else: 
                        #    print (f'DEBUG: NO Match: r_jig_name {r_jig_name} != s_jig_name {s_jig_name}')

                if jig_match:
                    anyinfo.jig_details[jig_ix].append(s_jig_id)

                # if the jig was not found exit
                else:
                    print(f'Error:    Equipment {required_jig} not found in supplied jig list\n\nTerminating...\n\n')
                    framework_exit()

        # next instrument in the required equipment list
        #jig_ix += 1






def crash_handler(anymsg):
    '''
    Crashhandler, clean exit from crash, rather than crash information.
    '''
    msg = '\n\t\t\tCrash Message\n'
    msg += "="*79
    msg += f'\n\tThere was a problem with the {anymsg} test!\n'
    msg += '\tTry again or contact Test Group.\n'
    msg += '\n\nPress ENTER to continue or (Q/q) to exit: '
    sel = (input(msg)).upper()
    return sel


#################################################################
#
# Menu functions - rather functions called by the menu system
#
# The functions below are the 'top level' of each of the individual
# tests and are called directly from the menu system
#
#################################################################

def enter_test_info(anyinfo):
    '''
    Input function for user_name name and serial number (as well as temperature and humidity values)
    Clears the data structure when it is called and stores the start test time
    '''
    py_cls()
    anyinfo.clear_base_struct()
    anyinfo.get_test_time()
    anyinfo.get_user_name()
    anyinfo.get_ser_no()

    # name the results folder once we have the serial number
    # check/create the results folder root if it does not exist
    results_folder = sanitise_folder_name(
        f'{anyinfo.test_folder}/{anyinfo.results_folder}')
    check_folder(results_folder)

    # check/create the report folder
    report_folder = sanitise_folder_name(
        f'{anyinfo.test_folder}/{anyinfo.test_report_folder}')
    check_folder(report_folder)

    # only manage 1 serial number if we're not AFT or ADAU testing
    # if aft or ADAU testing create and or backup one folder for each serial number
    if not anyinfo.aft_test:

        # create an dictionary of a lists of the expected test to be run
        # This is for the interim reports associated with each serial number
        # serial number is the key.
        anyinfo.prepare_archive_lists(report_folder)
        anyinfo.results_dict[anyinfo.ser_no] = deepcopy(anyinfo.results)
    else:
        for ser_no in anyinfo.aft_ser_nos:
            # map the serial no to anyinfo/BaseTestInfo
            # before doing anything else
            anyinfo.ser_no = ser_no
            anyinfo.prepare_archive_lists(report_folder)

            # create an dictionary of a lists of the expected test to be run
            # This is for the interim reports associated with each serial number
            # serial number is the key.
            # anyinfo.results_dict[ser_no] = []
            anyinfo.results_dict[ser_no] = deepcopy(anyinfo.results)

    if anyinfo.record_temp:

        anyinfo.start_rh = anyinfo.get_rh()
        anyinfo.start_temp = anyinfo.get_temp()

    if anyinfo.record_equipment:
        enter_test_equipment(anyinfo)
        enter_test_jigs(anyinfo)

    return True


def gen_test_report(any_info):
    '''
    Generates the test report in TXT format.
    '''
    # this needs to be imported here to avoid a circular reference.
    # must admit I'm not sure why

    null = 0.0

    any_info.check_test_time()
    if any_info.record_temp:
        if any_info.end_rh == null:
            any_info.end_rh = any_info.get_rh()
        if any_info.end_temp == null:
            any_info.end_temp = any_info.get_temp()

        if not_in_range(any_info.start_rh, any_info.humidity_limits[0], any_info.humidity_limits[1]):
            print(
                f'\n\n>>>Error: Humidity levels out of range {any_info.start_rh:.1f}, Limits: {any_info.humidity_limits[0]:.1f} {any_info.humidity_limits[1]:.1f}\n\n')

        if not_in_range(any_info.start_temp, any_info.temp_limits[0], any_info.temp_limits[1]):
            print(
                f'\n\n>>>Error: Temperature levels out of range {any_info.start_temp:.1f}, Limits: {any_info.temp_limits[0]:.1f} {any_info.temp_limits[1]:.1f}\n\n')

    debug = False
    msg = any_info.show_base_struct()
    show_debug(debug, msg)

    if not any_info.aft_test:
        report_name = f'{any_info.ser_no}_TEST_REPORT.TXT'
        any_info.report_name = sanitise_folder_name(
            f'{any_info.test_folder}/{any_info.test_report_folder}/{report_name}')

        msg = any_info.gen_final_report()
        with open(any_info.report_name, 'w', encoding='utf-8') as repfile:
            repfile.write(msg)

        if any_info.pdf_report:
            any_info.populate_assembly_info()
            gen_pdf_report(any_info)

        # zip the results etc
        zip_results(any_info)

        # back up the results, ini file and reports etc to the network
        if any_info.network_backup:
            any_info.backup_module_data()

        if not any_info.running_all:
            msg += '\n\nPress Enter to continue :'
            input(msg)

    else:
        for ser_no in any_info.aft_ser_nos:
            # populate the 'stand alone' fields for each module in turn
            # so that the stand alone functionality can be re-used for bench, aft and adau tests
            any_info.ser_no = ser_no

            report_name = f'{any_info.ser_no}_TEST_REPORT.TXT'
            any_info.report_name = sanitise_folder_name(
                f'{any_info.test_folder}/{any_info.test_report_folder}/{report_name}')

            msg = any_info.gen_final_report()
            with open(any_info.report_name, 'w', encoding='utf-8') as repfile:
                repfile.write(msg)

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # generate PDFs after the event in case there is a problem with the PDF generation
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if any_info.pdf_report:
                any_info.populate_assembly_info()
                gen_pdf_report(any_info)

        # zip the results etc for each module tested
        for ser_no in any_info.aft_ser_nos:
            any_info.ser_no = ser_no
            zip_results(any_info)

            # back up the results, ini file and reports etc to the network
            # input(f'\n<Debug: gen_test_report> network_backup is : {any_info.network_backup}')
            if any_info.network_backup:
                any_info.backup_module_data()


#####################################################################

#
# dummy functions to complete all required function definitions
#
#####################################################################
def run_all():
    '''
    dummy function for completeness
    if this ever runs there is a big problem...
    '''
    framework_exit('Exiting. Catastrophic test failure...')


def quit_test():
    '''
    dummy function for completeness
    '''
    framework_exit('Exiting. Catastrophic test failure...')


################################################################################################################
#
# Test Menu list. This is a list of tests, their menu messages and top level function names
# As written assumes that last 3 entries are always gen_report_test, run_all, and quit_test
#
# Each message/function name are a list within the test_list list
#
# We can't use dictionaries for this as we can't guarantee an order when we read back the dictionary contents.
#
################################################################################################################

def run_test(any_testinfo, an_inifile):
    '''
    Run the menu system for production tests
    takes as arguement the test class/structure and takes things from there.


    '''

    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=any_testinfo.logging_level)

    # for debug
    debug = False

    # pickling removed for debug
    no_pickle = False

    # Disable Crash Handler
    no_crash = True

    get_base_test_info(any_testinfo, an_inifile)
    # read in the test specific setup information
    any_testinfo.get_test_info(any_testinfo, an_inifile)
    any_testinfo.template_results()

    if debug:
        msg = any_testinfo.show_base_struct()
        input(msg)

    # generate the list of valid keys for exception handling
    any_testinfo.set_valid_keys()
    any_testinfo.clean_up()

    run_the_test = True
    ix = 0
    m_ix = 0    # message index
    c_ix = 1    # command index

    while run_the_test:
        sel = any_testinfo.main_menu(any_testinfo.test_list)

        msg = f'DEBUG: Test Sel: ...{sel}...'
        #show_debug(debug, msg)
        logging.debug(msg)

        if sel == any_testinfo.quit_test:
            run_the_test = False

        elif sel == any_testinfo.run_all_tests:           # run_all_tests branch works
            any_testinfo.running_all = True
            for ix in range(0, (len(any_testinfo.test_list)-2)):
                if (ix != 0) and not no_pickle:   # was 1
                    if path.exists(any_testinfo.pickle_file):
                        any_testinfo = any_testinfo.unpickledata()

                if not no_crash:
                    any_testinfo.test_seq_number = ix
                    any_testinfo.get_test_seq(any_testinfo.test_list[ix][c_ix])

                    try:
                        any_testinfo.test_list[ix][c_ix](any_testinfo)

                    except RuntimeError:
                        sel = crash_handler(any_testinfo.test_list[ix][m_ix])
                        if sel == 'Q':
                            break
                else:
                    any_testinfo.test_seq_number = ix
                    any_testinfo.get_test_seq(any_testinfo.test_list[ix][c_ix])
                    any_testinfo.test_list[ix][c_ix](any_testinfo)

                # tidy up
                any_testinfo.clean_up()
                if not no_pickle:
                    any_testinfo.pickledata()

            # exit when everything is done
            run_the_test = False
            any_testinfo.running_all = False

        elif sel in any_testinfo.valid_keys:        # run step by step also works
            if sel == any_testinfo.report_test:
                ix = len(any_testinfo.test_list)-3
            else:
                ix = int(sel)-1

            if ix != 0:    # first option is always 'Enter Test Info'
                if not no_pickle and path.exists(any_testinfo.pickle_file):
                    any_testinfo = any_testinfo.unpickledata()
                    msg = any_testinfo.show_base_struct()
                    show_debug(debug, msg)

                if not any_testinfo.struct_empty():
                    py_cls()
                    msg = f'\n\n\t\tRunning {any_testinfo.test_list[ix][m_ix]}'
                    print(msg)

                    # crash handler disabled during debugging

                    if not no_crash:
                        any_testinfo.test_seq_number = ix
                        any_testinfo.get_test_seq(
                            any_testinfo.test_list[ix][c_ix])
                        try:
                            any_testinfo.test_list[ix][c_ix](any_testinfo)
                        except RuntimeError:
                            crash_handler(any_testinfo.test_list[ix][m_ix])

                    else:
                        any_testinfo.test_seq_number = ix
                        any_testinfo.get_test_seq(
                            any_testinfo.test_list[ix][c_ix])
                        any_testinfo.test_list[ix][c_ix](any_testinfo)

                else:
                    py_cls()
                    msg = '\n\n\t\tEnter Test Info before Continuing\n\t\tPress enter to continue'
                    input(msg)
            else:
                py_cls()
                msg = f'\n\n\t\tRunning {any_testinfo.test_list[ix][m_ix]}\n\n '
                print(msg)

                if not no_crash:
                    any_testinfo.test_seq_number = ix

                    try:
                        any_testinfo.test_list[ix][c_ix](any_testinfo)

                    except RuntimeError:
                        crash_handler(any_testinfo.test_list[ix][m_ix])
                else:
                    any_testinfo.test_seq_number = ix
                    any_testinfo.test_list[ix][c_ix](any_testinfo)

            # tidy up
            any_testinfo.clean_up()
            if not no_pickle:
                any_testinfo.pickledata()

        else:
            py_cls()
            msg = f'\n\n\n\n\t\tERROR: Invalid selection {sel}\n\t\tPress enter to continue'
            input(msg)


def py_cls():
    '''
    Python Clear Screen function call
    '''
    cmd = 'cls'
    system(cmd)


def py_pause():
    '''
    Python equivalent of 'pause' in DOS
    '''
    input('Press Enter to Continue...')


def backup_file(name):
    '''
    Create a backup copy a file
    should be using del_file for the delete
    '''
    backup_name = f'backup_{name}'
    del_file(backup_name)

    if path.exists(name):
        copy(f'{name}', f'{backup_name}')
        #cmd = f'copy /Y {name} {backup_name}'
        #system(cmd)


def del_file(some_file_name):
    '''
    If the filename exists, delete it, quietly and forcefully

    should be using shutil.remove
    '''
    if path.exists(some_file_name):
        #cmd = f'del /Q /F  {some_file_name}'
        #system(cmd)
        remove(f'{some_file_name}')


def mov(some_file_name, destination):
    '''
    Moves Filename to the destinaton folder
    '''
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    logging.getLogger(__name__)

    if path.exists(some_file_name):
        move(some_file_name, destination)
    else:
        logging.warning(
            f'File not found: FAILED: move {some_file_name} {destination}')


def sanitise_folder_name(folder_name):
    '''
    turns the folder name into a linux/unix style string
    '''

    logging.basicConfig(format='%(levelname)s:%(message)s',level=logging.INFO)
    logging.getLogger(__name__)

    # get rid of dots in the path
    local_folder_name = folder_name.replace('.\\', '/')

    # unix/linux style slashes
    local_folder_name = local_folder_name.replace('\\', '/')

    # we've managed to end up with multiple slashes...
    local_folder_name = local_folder_name.replace('//', '/')
    local_folder_name = local_folder_name.replace('//', '/')

    logging.debug(
        f'sanitise_folder_name: local_folder_name: {local_folder_name}')

    return local_folder_name


def check_folder(folder_name):
    '''
    Check that the folder exists, if it doesn't create it

    returns:

    0: folder already exists
    1: Successfully created the folder
    2: failed to create folder.

    python likes unix file names. the filename is converted to unix style before anything happens

    Only works with folders, not files
    '''
    local_folder_name = sanitise_folder_name(folder_name)

    if not path.exists(local_folder_name):
        try:
            mkdir(local_folder_name)
            return 0
        except IOError as check_error:
            print(f'ERROR: Unable to create folder {folder_name}')
            print(f'ERROR: full path {local_folder_name}')
            print(check_error)
            return -1
    else:
        return 0


def find_folder(a_path, log_id):
    '''
    Imported from Process_Utils

    Function to create the results folders that the logged date and result files will be dumped to - Tradionally used by the Analogue DV program
    '''
    num_folder = 0
    found_folder = False
    while (not found_folder):
        num_folder = num_folder+1
        folder_name = str(f'{a_path}/{log_id}_{num_folder:02d}')

        if (path.isdir(folder_name)):
            found_folder = False
        else:
            folder = folder_name
            found_folder = True

    return (folder, num_folder)


def check_dir(a_path):
    '''
    Imported from Process_Utils

    Function to check for the existance of a directory (as string passed in)
    '''
    return path.exists(a_path)


def zip_results(anyinfo):
    '''
    Zip the results folder, based on anyinfo.ser_no
    '''

    #input ('zip_results called')
    results_folder = sanitise_folder_name(
        f'{anyinfo.test_folder}/{anyinfo.results_folder}/{anyinfo.ser_no}')
    zip_name = f'{results_folder}.tar.gz'
    #input (f'Zip folder name: {zip_name}')
    tar_folder(results_folder, zip_name)
