'''
Module to compress a folder for later retrieval,
courtesy of tutorialspoint
'''

import os
import tarfile
def tar_folder(path, tar_name):
    '''
    TAR is an archive. The name is an abbreviation of "Tape ARchive"
    It creates a stream of data that can then be compressed using other formats.
    Using TAR to perform this simplifies the task of providing the data that needs to be compressed,
    i.e. files do not need to be read and streamed to the compression algorithm
    
    this function turns the passed folder into a data stream and then zips it into a GZ format zip file
    
    '''
    with tarfile.open(tar_name, "w:gz") as tar_handle:
        for root, _, files in os.walk(path):
            for file in files:
                #print (f'DEBUG: tar_folder: adding {file} to {tar_name}')
                tar_handle.add(os.path.join(root, file))
    tar_handle.close()                


if __name__ == '__main__':                
    MY_ZIP_FOLDER = 'C:/ACRA/Test/COPQ_Libraries/Python3_PEP8/example_production_test/example_production_test/RESULTS/ABC1234_2022_03_30_16_39_15'
    ZIP_FILE = 'ABC1234_2022_03_30_16_39_15.tar.gz'
    print (f'Folder: {MY_ZIP_FOLDER}')
    print (f'Zip File: {ZIP_FILE}')
    tar_folder(MY_ZIP_FOLDER, ZIP_FILE)
