'''
Name:        DAS_Studio_Tools.PY

Author:      Chris Gilbert

Created:     Jan 18, 2021

Copyright:   (c) Curtiss Wright Corporation ACRA BU 2021

Description:
    Wrappers and interfaces to the standalone compiler

-------------------------------------------------------------------------------
History:
   18-Jan-2021:   Chris Gilbert      Created, based on functions taken from the
                                     AXN/TCG/401 production test.

--------------------------------------------------------------------------------
'''

from os import getcwd, chdir, popen
from re import compile as re_compile, I as re_I

# this is a utility from Vitalij
# here for refernece if there are problems found with the existing functions.
# def prog_das_standalone(anyinfo, xidml_name: str, apply_calibration: bool = False) -> bool:
#    """
#    Compiles the passed XIDML file and programs the chassis using the DAS Studio standalone compiler.
#    Returns:
#        ProgOK: bool
#    """
#
#    xidml_name = path.join(anyinfo.base_folder, 'XIDML', xidml_name)
#    prog_ok = False
#    chdir(anyinfo.das_studio_dir)
#    if apply_calibration:
#        program_cmd = f"Program.exe -GN -MP -T {xidml_name} -UN -RN -FY"
#    else:
#        program_cmd = f"Program.exe -GN -MP -T {xidml_name} -UN -RN -FN"
#
#    print(f"\n\nProgramming with: {program_cmd} ")
#    kup_status = acraexec(program_cmd)
#
#    msg = f"Func Test KUPCmd: {program_cmd} (Status: {kup_status})"
#    print(msg)
#    if int(kup_status) != -1:
#        program_msg = f"{anyinfo.das_studio_dir}\\Program\\MsgML\\Program_MsgML.xml"
#        with open(program_msg, 'r', encoding="utf8", errors="ignore") as file:
#            line = file.readlines()
#            if len(line) > 0:
#                lastline = line[-5]
#                if "<Text>Compile and programming complete</Text>" in lastline:
#                    prog_ok = True
#                elif "<Text>Compile and programming completed with errors</Text>" in lastline:
#                    prog_ok = False
#            else:
#                prog_ok = False
#    chdir(anyinfo.base_folder)
#    return prog_ok


def acraexec(command):
    """
    This takes acra command (kc,hound,ds3) as string, executes it,
    and returns a result as tuple(hints, warnings, errors).
    If the execution fails, returns -1.

    example:
    from acra import utils
    result = utils.acraexec('kc -f test.xid')
    errors = result[2] #number of compilation errors
    """
    pipe = popen(command)
    lines = pipe.readlines()
    if len(lines) > 0:
        lastline = lines[-1]
    else:
        return -1
    pipe.close()
    mask = re_compile(r"(\d+)\D*hint\D*(\d+)\D*warning\D*(\d+)\D*error", re_I)
    result = mask.search(lastline)
    if result:
        return int(result.group(1)), int(result.group(2)), int(result.group(3))
    return -1


def kc_exec(kc_command, anyxid):
    '''
    This function takes a file and checks if it exists and
    if so runs KC with the file.
    '''

    cmd = f'{kc_command} -f {anyxid}'

    result = acraexec(cmd)

    if result[2] != 0:
        msg = '\n###################################################\n'
        msg += '# INFO: Compilation FAILED \n'
        msg += '#       KcCommand: {KcCommand}\n'
        msg += '###################################################\n'
        return (False, msg)

    msg = '\n###################################################\n'
    msg += '# INFO: Compilation completed\n'
    msg += '###################################################\n'

    return (True, msg)


def das_standalone_compile(anyinfo, xidml_name):
    '''
    Compiles the passed XIDML file using the DAS Studio standalone compiler.

    anyinfo.DasStandAloneDir  = 'C:\\TEST_DAS\\StandaloneCompiler\\' + self.DasVersion + '\\'
    anyinfo.DasStandaloneCmd  = 'C:\\TEST_DAS\\StandaloneCompiler\\' + self.DasVersion + '\\Program.exe'
    anyinfo.DasStudioMsgLog   = 'C:\\TEST_DAS\\StandaloneCompiler\\' + self.DasVersion + '\\Program\\MsgML\\Program_MsgML.xml'  


    DasStudio StandAlone Dir  = C:\TEST_DAS\StandaloneCompiler\       3.4.18    \
    DasStudio Standalone Cmd  = C:\TEST_DAS\StandaloneCompiler\       3.4.18    \Program.exe
    DasStudio Msg Log         = C:\TEST_DAS\StandaloneCompiler\       3.4.18    \Program\MsgML\Program_MsgML.xml    

    Program.exe help:

        Verbosity:              -V <Verbosity level>  {H | M | L, i.e. Chatter,Normal Minimal, default = 'H'}

        Display:                -G <Display mode> {A  |P | N, i.e. Full U.I., Progress bar only, None, default = 'A'}

        Task:                   -T [Task file]

        CmdMLFilePath:          -C [CmdML File Path]

        ProgramMode:            -M <Program mode>, { V | P | B , i.e. Compile only, Compile and Program, Program Only, , default = 'V'}

        RunValidation:          -A <Run validation before schedulers> , { Y | N, default = 'Y'}

        UseMCS:                 -U <Use MCS during compilation> , { Y | N, default = 'Y'}

        SaveChanges:            -R <Save the changes made by the MCS and serial number synchronizer to the task file> , { Y | N, default = 'Y'}

        FetchCalibration:       -F <Synchronize serial numbers and fetch calibration>, { Y | N, default = 'N'}

        CalibrationRepositoryPath:-K <Alternative path for calibration repository location>, {default is application path }

        CustomXDefMLPath:       -O <Alternative path for custom XDefML files>, {default is .\XDefML\Custom }

        CompileInParallel:      -P <Compile the Chassis(s) in Parallel> , { Y | N, default = 'N'}

        BasePath:               -L <Alternative base path were all files are written to>, {default is <application path>>}

        StopOnDefaultCalibration:-D <Stop on default calibration>, { Y | N, default = 'N'}

        StopOnCalibrationErrors:-E <Stop on calibration errors>, { Y | N, default = 'N'}

        StopOnSerialNumberErrors:-N <Stop on serial number errors>, { Y | N, default = 'N'}

        EthProgConnectionChassis:-X [Ethernet to PROG programming link chassis], { 0 to 63, default = '0'}    

    Only 1 shot at compilation. If compilation fails the test should fail.    

    '''

    # for whatever reason the standalone compiler needs to run in the Standalone compiler folder
    test_folder = getcwd()
    chdir(anyinfo.das_standalone_dir)

    print(f'getcwd(): {getcwd()}')
    input(f'anyinfo.das_standalone_dir: {anyinfo.das_standalone_dir}')

    compile_command = f'{anyinfo.das_standalone_cmd} -GN -MV -T {xidml_name} -UN -RN'

    compile_ok = False       # assume compilation failed

    compile_status = acraexec(compile_command)

    #print(f'Compile Command Status: {compile_status}')

    if int(compile_status) == -1:
        compile_ok = True

    # cd back to the test folder before returning
    chdir(test_folder)
    return compile_ok


def das_standalone_prog(anyinfo, xidml_name, verbosity='H', display='A',
                           program_mode='B', validation='N', usemcs='Y',
                           save_change='Y', fetch_cal='N', stop_def_cal_error = 'N',
                           stop_cal_error = 'N', stop_sn_error = 'N', eth_prog_chas = 0
                           ):
    '''
    Programs the chassis using the DAS Studio standalone compiler, using the default cache.

    anyinfo.DasStandAloneDir  = 'C:\\TEST_DAS\\StandaloneCompiler\\' + self.DasVersion + '\\'
    anyinfo.DasStandaloneCmd  = 'C:\\TEST_DAS\\StandaloneCompiler\\' + self.DasVersion + '\\Program.exe'
    anyinfo.DasStudioMsgLog   = 'C:\\TEST_DAS\\StandaloneCompiler\\' + self.DasVersion + '\\Program\\MsgML\\Program_MsgML.xml'  

    Program.exe help:

        Verbosity:              -V <Verbosity level>  {H | M | L, i.e. Chatter,Normal Minimal, default = 'H'}

        Display:                -G <Display mode> {A  |P | N, i.e. Full U.I., Progress bar only, None, default = 'A'}

        Task:                   -T [Task file]

        CmdMLFilePath:          -C [CmdML File Path]

        ProgramMode:            -M <Program mode>, { V | P | B , i.e. Compile only, Compile and Program, Program Only, , default = 'V'}

        RunValidation:          -A <Run validation before schedulers> , { Y | N, default = 'Y'}

        UseMCS:                 -U <Use MCS during compilation> , { Y | N, default = 'Y'}

        SaveChanges:            -R <Save the changes made by the MCS and serial number synchronizer to the task file> , { Y | N, default = 'Y'}

        FetchCalibration:       -F <Synchronize serial numbers and fetch calibration>, { Y | N, default = 'N'}

        CalibrationRepositoryPath:-K <Alternative path for calibration repository location>, {default is application path }

        CustomXDefMLPath:       -O <Alternative path for custom XDefML files>, {default is .\XDefML\Custom }

        CompileInParallel:      -P <Compile the Chassis(s) in Parallel> , { Y | N, default = 'N'}

        BasePath:               -L <Alternative base path were all files are written to>, {default is <application path>>}

        StopOnDefaultCalibration:-D <Stop on default calibration>, { Y | N, default = 'N'}

        StopOnCalibrationErrors:-E <Stop on calibration errors>, { Y | N, default = 'N'}

        StopOnSerialNumberErrors:-N <Stop on serial number errors>, { Y | N, default = 'N'}

        EthProgConnectionChassis:-X [Ethernet to PROG programming link chassis], { 0 to 63, default = '0'}    

    Only 1 shot at compilation. If compilation fails the test should fail.    

    '''
    # Assumes the compile command has been run to configure DAS Studio standalone compiler
    # paths and commands based on the version in the INI file

    # Configure the command arguements

    # run compile and program
    # -GN: No display
    # -MB: Program only
    # -AN: No verify

    # for whatever reason the standalone compiler needs to run in the Standalone compiler folder
    test_folder = getcwd()
    chdir(anyinfo.das_standalone_dir)

    # was prog_command = f'{anyinfo.das_standalone_cmd}  -T {xidml_name} -GA -MB -AN'
    
    prog_command = (f"{anyinfo.das_standalone_cmd} -T{xidml_name} -V{verbosity} "
                    f"-G{display} -M{program_mode} -A{validation} "
                    f"-U{usemcs} -R{save_change} -F{fetch_cal} -D{stop_def_cal_error} "
                    f"-E{stop_cal_error} -N{stop_sn_error} -X{eth_prog_chas}")
    print(prog_command)
    prog_ok = False       # assume compilation failed

    # allow for retries if programming fails
    retry_count = 1
    retry_programming = True

    while retry_programming:

        print(f'INFO: Programming: Attempt:{retry_count}')
        _ = acraexec(prog_command)

        with open(anyinfo.das_studio_msg_log, "r", encoding='utf-8') as log_file:
            line = log_file.readlines()

        if (len(line) > 0):
            lastline = line[-5]

            if "<Text>Compile and programming complete</Text>" in lastline:
                prog_ok = True
                retry_programming = False

            elif "<Text>Compile and programming completed with errors</Text>" in lastline:
                prog_ok = False
        else:
            prog_ok = False

        # increment the retry counter if programming fails
        # but exit if it succeeds
        if not prog_ok:
            if retry_count < anyinfo.programming_retry_count:
                retry_count += 1
            else:
                retry_programming = False

    chdir(test_folder)

    if prog_ok:
        print('INFO: Programming was successful')
    else:
        print(f'ERROR: Programming Failed after {retry_count} retries')

    return [prog_ok, retry_count]


def das_standalone_compile_prog(anyinfo, xidml_name):
    '''
    Wrapper around Program Compile and Program Program, 
    rather than using the compile and program switch

    Returns success/failure with number of programming attempts for logging:

    "True",  Retry Count: Programming succeeded
    "False", Retry Count: Programing failed

    Won't attempt to program if compilation fails.
    '''

    prog_ok = False
    retry_count = 0

    compile_ok = das_standalone_compile(anyinfo, xidml_name)

    if compile_ok:
        prog_ok, retry_count = das_standalone_prog(anyinfo, xidml_name)

    return [prog_ok, retry_count]


if __name__ == '__main__':

    #from subprocess import Popen

    TEST_INIFILE = 'debugging.ini'

    from acra.base_test_info import BaseTestInfo
    from acra.test_framework import get_base_test_info

    testinfo = BaseTestInfo()
    get_base_test_info(testinfo, TEST_INIFILE)

    # set up the information required in BASETESTINFO to
    # run this module standalone
    # none required if DAS Studio 3.4.19 is installed

    print(
        f'DEBUG: After INIT: Standalone Compiler Version: {testinfo.das_version}')
    input('hit enter to continue')

    #mytask = 'C:\\ACRA\\Test\\AXNTDC401A\\XIDML\\k-type_external_compensation.xidml'
    MYTASK = 'C:\\ACRA\\Test\\ADC_404\\SINAD.xidml'

    # test the code...
    resp = das_standalone_compile_prog(testinfo, MYTASK)
    if resp[0]:
        print('Compilation and programming succeeded')
    else:
        print(f'Compilation and programming FAILED\n\n{resp[1]} retries')
