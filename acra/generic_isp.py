'''
Name:     generic_isp.py
Author:   Chris Gilbert
Date:     March 25, 2022

Created    25 March 2022
Copyright Curtiss Wright 2022

Description

Generic ISP/SPI function for standardisation across all AXON and ADAU tests.

Taken from the TCG/401/B test code and modified to use existing functions.

This module programs the golden image into the module so that the 
ISP process will function correctly.
 
SPI is the extension Libero generates (like stp) when this file is generated. 
It indicates that the file is used for programming of the FPGA over its SPI interface. 
It's really a raw image that's written to an SPI eeprom that the FPGA can read back and upgrade itself.

The SPI file itself is a meta file that contains the FPGA image and meta data 
that is relevant to the use of that FPGA image during the in-service programming activity.

=============================================================================
'''
from time import sleep
from acra.reporting_tools import pass_fail, str_com_result
from axon_utils.axon_tools import read_module_tic, prog_fpg_golden, zero_fpga,\
    prog_fpg_current, very_fpga
from acra.common_functions import valid_file_type


def test_isp(bcu_ip, slot_number, my_file, fpga_name: str,
             fpga_rev: str, fpga_name_2='', fpga_rev_2='', rpt='', section_fails=0):
    '''
    Generic function for programming the SPI golden image on AXON and ADAU products
    requires acra utilities 4.0.3 or later and axon_utils.axon_tools 1.2.0 or later
    parameters:
        bcu_ip: '192.168.28.1' your bcu ip ->str
        slot_number: the slot number of the module to be programmed ->int
        my_file: the spi file to be programmed -> str
        fpga_name: expected fpga name after the programing 'TIC/AA/111'
        fpga_rev: expected fpga revision in format '0x50'
        fpga_name_2: expected fpga name of the second FPGA on the board 
        fpga_rev_2: expected fpga revision of the second FPGA on the board 
    NOTE1: Only one fpga on Axon modules can be ISP'ed.
    NOTE2: This function does not support the method, when golden and updated
    images to be updated with different fpga revisions. 
    Less likely this ever happens.

    returns: tuple (section_fails, rpt)

    '''

    try:
        if not valid_file_type(my_file):
            section_fails += 1
            msg = (
                f'ERROR: The file {my_file} is not valid or does not exist!')
            print(msg)
            rpt = + msg
            return section_fails, rpt

        print('Zerroing FPGA. Please wait...')
        result, output = zero_fpga(bcu_ip, slot_number)
        my_result = str_com_result('Zeroing FPGA', pass_fail(result), 'PASS')
        rpt += my_result[1]
        section_fails += my_result[0]
        print(output)
        sleep(2)
        print('Updating FPGA with Golden image. Please wait...')
        result, output = prog_fpg_golden(
            bcu_ip, slot_number, my_file)
        my_result = str_com_result('Updating FPGA with Golden image',
                                   pass_fail(result), 'PASS')
        rpt += my_result[1]
        section_fails += my_result[0]
        print(output)        
        my_result = str_com_result('Updating FPGA with Update image',
                                   pass_fail(result), 'PASS')
        rpt += my_result[1]
        section_fails += my_result[0]
        sleep(2)
        print('Updating FPGA with Update image. Please wait...')
        result, output = prog_fpg_current(
            bcu_ip, slot_number, my_file)

        my_result = str_com_result('Updating FPGA with Update image',
                                   pass_fail(result), 'PASS')
        section_fails += my_result[0]
        rpt += my_result[1]
        print(output)
        sleep(2)
        print('Verifying FPGA revisions. Please wait...')
        result, output, goldenimage, updateimage = very_fpga(
            bcu_ip, slot_number)
        print(output)

        if result == 0:
            my_result = str_com_result('Golden Image Revision',
                                       goldenimage, fpga_rev.upper())
            section_fails += my_result[0]
            rpt += my_result[1]

            my_result = str_com_result('Update Image Revision',
                                       updateimage, fpga_rev.upper())
            section_fails += my_result[0]
            rpt += my_result[1]
        else:
            rpt += 'ERROR: Unable to determine image versions\n'
            section_fails += 1
        sleep(5)
        print('Reading FPGA on the module.')
        tic_name, tic_rev, tic_name_2, tic_rev_2 = read_module_tic(
            bcu_ip, slot_number)

        my_result = str_com_result('Module FPGA',
                                   f"{tic_name} 0X{tic_rev:x}",
                                   f"{fpga_name} {fpga_rev.upper()}")
        rpt += my_result[1]

        if tic_name_2 != '':
            my_result = str_com_result('Module FPGA',
                                       f"{tic_name_2} 0X{tic_rev_2:x}",
                                       f"{fpga_name_2} {fpga_rev_2.upper()}")
        rpt += my_result[1]
        section_fails += my_result[0]

    except Exception as error:
        msg = f"ERROR: Error {error} occured during this test. "
        section_fails += 1
        rpt += msg
        print(msg)

    return (section_fails, rpt)


if __name__ == "__main__":

    test_isp(any_info)
