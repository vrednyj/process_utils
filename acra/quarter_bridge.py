'''

=============================================================================

 Name:     quarter_bridge_class.py

 Author:   Alan McCarthy

 Date:     April 2021
 
 Copyright Curtiss Wright 2022

 Description

 Python 3 for quarter bridge functions
 Put together for QB350 tests
 
 

=============================================================================
'''

class QuarterBridge :
    '''
    Quarter bridge class
    
    Functions associated with predicting the output of 
    an ADC/109 style quarter bridge circuit
    
    init requires the excitation/current source resistance 
    and defaults to the ADC/109/QB350 value of 349 ohms
    
    The  bridge completion resistors values can also be applied.
    These are defaulted to 22000 as per the ADC/109/QB350 and are used to calculate 
    the 'hidden' side of the bridge using the equation 
    
    ratio = 1/(r_completion1/(r_completion1 + r_completion2))
    
    ratio is not really a ratio, it is the divisor as a result of the ratio chosen
    if R1=R2        then ratio = 2, 
    if R1 = 2R2     then ratio = 1.5
    if R1 = 0.5R2   then ratio = 3 
    etc
    
    Note that upper case V/R/I indicates a value/variable and lower case indicates a method
    
    Rref    : Bridge reference resistor. For the ADC/109/QB350 this is 349 ohms
    Vbridge : Bridge Excitation voltage between Vexc+ and Vexc-
    Vm      : Voltage at midpoint of Rcomplete1 and Rcomplete2
    Vin     : Input voltage - voltage at the "top" of the strain gage
    Vdiff   : Difference voltage across the bridge = Vm - Vin
    Iexc    : Excitation current, as configured by test/task

    vbridge(self, Rsg)  : Method for calculating Vbridge
    vin(self, Rsg)      : Method for calculating Vin
    vm(Vbridge, ratio)  : Method for calculating Vm
    vdiff(self)         : Method for calculating Vdiff

    
    
    '''

    def __init__(self, r_ref=349, r_completion1 = 22000, r_completion2 = 22000):
        '''
        INIT takes the fixed values of the quarter bridge design.
        i.e. strain gage values and excitation current are excluded from the init.
        
        Remember to set the excitation current before performing any calculations
        '''
        self.r_ref    = r_ref
        self.ratio   = float(r_completion1 + r_completion2)/float(r_completion1)
        self.v_bridge = 0.0      # bridge voltage - difference voltage between Vexc+ and Vexc-
        self.v_measure      = 0.0      # Measurement Voltage - mid point of bridge completion resistors
        self.v_in     = 0.0      # Input voltage - as measured at the EXC+ side of the strain gage
        self.v_diff   = 0.0      # Difference voltage across the bridge = Vm - Vin
        self.i_exc    = 0.0      # Excitation current

    def set_iexc(self,i_exc):
        '''
        set the excitation current level
        '''
        self.i_exc = i_exc

    def vbridge(self, r_sg):
        '''
        calculates the bridge voltage based on the strain gage resistance
        '''
    
        self.v_bridge = self.i_exc*(r_sg+self.r_ref)
    
    def vin(self, r_sg):
        '''
        Returns the input voltage for any 
        excitation current and strain gage value
        '''
    
        self.v_in = self.i_exc*r_sg
        return self.v_in
    
    def v_m(self):
        '''
        returns the reference side of the bridge
        '''
        self.v_measure = float(self.v_bridge)/float(self.ratio)
        return self.v_measure
        
    def vdiff(self, r_sg):
        '''
        returns the differential input voltage of at the IA.
        '''
        self.v_bridge(r_sg)
        self.v_diff = self.v_m() - self.vin(r_sg)
        
        
        
    def show_contents(self):
        '''
        Debug feature to show the values of the class variables.
        '''
        
        msg  = ''
        msg += f'r_ref          : {self.r_ref:4.1f}\n'
        msg += f'ratio          : {self.ratio:4.1f}\n' 
        msg += f'v_bridge       : {self.v_bridge:1.6f}\n'
        msg += f'v_measure      : {self.v_measure:1.9f}\n'     
        msg += f'v_in           : {self.v_in:1.9f}\n'    
        msg += f'v_diff         : {self.v_diff:1.9f}\n'
        msg += f'i_exc          : {self.i_exc:1.6f}\n'
        
        return msg

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# test/debug area        
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
if __name__ == '__main__':
    current_levels = [0.002, 0.005, 0.0145]
    res_sgs    = [340, 350, 357]
    
    #I_levels = [0.002]
    #R_sgs    = [350]    
    
    my_qb350 = QuarterBridge()

    for current in current_levels:
        for res in res_sgs:
            my_qb350.set_iexc(current)
            my_qb350.vdiff(res)
            
            dbg_msg = f'Rsg      : {res:3.1f}'
            print (dbg_msg)
            print (my_qb350.show_contents())
            



#Rsg      : 350.0        OK
#Rref    : 349.0         OK
#ratio   :  2.0          OK
#Vbridge : 1.398000      OK
#Vm      : 0.699000000   OK
#Vin     : 0.700000000   OK
#Vdiff   : -0.001000000  OK
#Iexc    : 0.002000      OK
