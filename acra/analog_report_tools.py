""" 

Analog_Report_Gen.py

This is the old DV_Report_Gen.PY script incorporated into ACRA UTILS.

DV_Report_Gen.py {script file that houses all the local functions required by the DV_Prog script to generate Report spreadsheets or text files)

************************************** Version 1.0.1 ***************************************************

***************************************** Functions that are included in this file ******************************************

    1 GenCMRRSpreadSheet    - Generates a Spreadsheet based on calculated CMRR test results
    2 GenALRSpreadSheet     - Generates a Spreadsheet based on calculated ALR test results
    3 GenCalIniFile     - Generates a Cal.ini file based on the above functions for gain/offset coefficients
    4 GenIVEXTSpreadSheet   - Generates a Spreadsheet based on calculated IVEXT test results

****************************************************************************************************************************

[History]           
30-05-2008 - Robert Farrell - Created. This script takes away all report spreadsheet or text file generation duties from the
                  main DV script. This makes the main script more readable and the file generation more moduler
03-06-2008 - Robert Farrell - Added function to generate spreadsheet for the IVEXT test
04-06-2008 - Robert Farrell - Expanded the GenIVEXTSpreadSheet test to include resistance values
13-06-2008 - Robert Farrell - GenCalIniFile - print Offset coefficients as float instead of integers
30-06-2008 - Robert Farrell - GenCalIniFile - Expanded to include gain and offset coefficients for gains of 10,100,1000 - no temp points though yet
22-08-2008 - Robert Farrell - Version 1.0.0 - Added version numbering scheme (started at Version 1.0.0)
                        - GenIVEXTSpreadSheet - Small update to make it clear FSR error is been displayed
13-02-2014 - Robert Farrell - Version 1.0.1 - GenCalIniFile - Updated to generate the AGx stings based on power of two gains 

09-03-2020 - Chris Gilbert  - Migrated to Python 3. Function headers remodelled to show in in help(DV_Report_Gen)

****************************************************************************************************************************
"""

from os import system

def gen_cmrr_spreadsheet(log_folder,ident,numfold,cmrrdb,channels,f_list):
    '''
    gen_cmrr_spreadsheet - Generates a spreadsheet that displays the CMRR (dB) vs test frequency for n channels
    Format:
               Freq    Ch0     Chn
               XHz     XdB     XdB         for Freq in Input frequency (Flist) range

    No graph is currently plotted

    '''

    system('cls') 
    print ('Generating the results spreadsheet')
   # sleep(3)
    ### Setting of the result file
   # try:
   #     res_cmrr = open(f'{log_folder}\\{str(ident)}_{str(numfold)}.xls','w', encoding='utf-8')  # File for excel
   # except IOError:
   #     print('Cant open the excel result file')
   #     art_exit()

    #File construction    
    spreadsheet_contents = '\t\t\tCMRR Test Results\n'
    spreadsheet_contents += 'Freq\t'

    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'

    spreadsheet_contents += '\n'

    for ix, freq in enumerate(f_list):
        spreadsheet_contents += str(f'{float(freq)}\t')
        for channel in range(channels):
            spreadsheet_contents += str(f'{cmrrdb[ix][channel]:3.3d}\t')
        spreadsheet_contents += '\n'

    #res_cmrr.close()
    
    with open(f'{log_folder}\\{str(ident)}_{str(numfold)}.xls','w', encoding='utf-8') as res_cmrr: # File for excel
        res_cmrr.write(spreadsheet_contents) 


def gen_alr_spreadsheet(log_folder,ident,numfold,channels,v_in,v_meas,channel_avg,conv_to_volts,conv_v_meas_to_counts,channel_avg_adj,fsr_error,cal_gain_offset_coeff,offset_coeff,gain_coeff,fsr_error_go_adj):
    '''
    gen_alr_spreadsheet - Generates a spreadsheet that displays the FSR Error among others
    Format:
               Vin     Vmeas    Ch0     Chn
                V        V       X       X      For Vin in Input Voltage range

    This format is repeated for the following values of X: -
    X (Standard)  = 1-Average Counts, 2-Counts converted to voltages, 3-Adjusted average counts, 4-FSR Error
    X (Optional)  = 5-Gain/Offset Coeffients, 6-FSR Error after G/O Coeff applied

    No graph is currently plotted

    ***** Generate excel files based on results *****

    '''



    #Initial Excel file header construction
    spreadsheet_contents = '\t\t\t\tAverage Vin Count Value per Channel\t\t\t\t\t\t\t\t\tCounts to Voltage Conversion\n'
    spreadsheet_contents += '\t'
    spreadsheet_contents += 'Vmeas\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t' 
    spreadsheet_contents += '\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'
    spreadsheet_contents += '\n'
    
    #Build the rows and columns with Vmeas vs [Raw Average counts per channel] [Counts to Volts Conversion per channel]
    #for ix in range(len(v_in)):
    for ix, v_in_ix in enumerate(v_in):
        spreadsheet_contents += f'{v_in_ix}\t'
        spreadsheet_contents += f'{v_meas[ix]}\t'
        for channel in range(channels):
            spreadsheet_contents += f'{channel_avg[ix][channel]}\t'
        spreadsheet_contents += '\t'
        for channel in range(channels):
            spreadsheet_contents += f'{conv_to_volts[ix][channel]}\t'
        spreadsheet_contents += '\n'

    #Build the rows and columns with Vmeas_Count vs [Raw Average counts adjusted for channel range values]
    spreadsheet_contents += '\n\n\t\t\t\tVmeas Conv to Counts + Average Adjust\n'
    spreadsheet_contents += '\tVm_CNT\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'
    spreadsheet_contents += '\n'
    
    for ix, v_in_ix in enumerate(v_in):
        spreadsheet_contents += f'{v_in_ix}\t'
        spreadsheet_contents += f'{conv_v_meas_to_counts[ix]}\t'
        for channel in range(channels):
            spreadsheet_contents += f'{channel_avg_adj[ix][channel]}\t'
        spreadsheet_contents += '\n'

    #Build the rows and columns with FSR Error based on average counts vs Vmeas counts
    spreadsheet_contents += '\n\n\t\t\t\t\tFSR Error\n\t\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'
    spreadsheet_contents += '\n'
    for ix, v_in_ix in enumerate(v_in):
        spreadsheet_contents += f'{v_in_ix}\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'{fsr_error[ix][channel]}\t'
        spreadsheet_contents += '\n'

    system('cls')

    if(cal_gain_offset_coeff.upper() == 'YES'):

        #Append the Gain and Offset co-efficients to the existing Excel spreadsheet
        spreadsheet_contents += '\n\n\t\t\t\tGain and Offset Coefficients\n\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'CH{channel}\t'
        spreadsheet_contents += '\n'
        spreadsheet_contents += 'OFFSET\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'{offset_coeff[channel]}\t'
        spreadsheet_contents += '\n'
        spreadsheet_contents += 'GAIN\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'{gain_coeff[channel]}\t'

        #Append the adjusted error after applying the gain and offset adjustments
        spreadsheet_contents += '\n\n\n\t\t\t\tAdjusted Error after applying Gain and Offset Coefficients\n\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'CH{channel}\t'
        spreadsheet_contents += '\n'
        for ix, v_in_ix in enumerate(v_in):
            spreadsheet_contents += f'{v_in_ix}\t\t'
            for channel in range(channels):
                spreadsheet_contents += f'{fsr_error_go_adj[ix][channel]}\t'
            spreadsheet_contents += '\n'

        print ('Appending Calculated Offset and Gain Coefficients and adjusted error to the existing result spreadsheet\n')
   
    with open (f'{log_folder}\\{ident}_{str(numfold)}_results.xls','w', encoding='utf-8') as res_file: # Excel file with counts measurement
        res_file.write(spreadsheet_contents)


def gen_alr_spread_sheet_mux(log_folder,ident,numfold,channels,v_in,f_list,v_meas,channel_avg,conv_to_volts,conv_v_meas_to_counts,channel_avg_adj,fsr_error,cal_gain_offset_coeff,offset_coeff,gain_coeff,fsr_error_go_adj):
    '''
    gen_alr_spread_sheet_mux - Generates a spreadsheet that displays the FSR Error among others for MULTIPLEXED Channels
    Format:
              Vin     Vmeas    Ch0     Chn
               V        V       X       X      For Vin in Input Voltage range

    This format is repeated for the following values of X: -
    X (Standard)  = 1-Average Counts, 2-Counts converted to voltages, 3-Adjusted average counts, 4-FSR Error
    X (Optional)  = 5-Gain/Offset Coeffients, 6-FSR Error after G/O Coeff applied

    No graph is currently plotted

    ***** Generate excel files based on results *****

    '''

    #try:
    #    res_file = open(f'{log_folder}\\{ident}_{str(numfold)}_results.xls','w', encoding='utf-8')  # Excel file with counts measurement
    #except IOError:
    #    print('Cant open the excel result file')
    #    art_exit()

    #Initial Excel file header construction
    spreadsheet_contents ='\t\t\t\tAverage Vin Count Value per Channel\t\t\t\t\t\t\t\t\tCounts to Voltage Conversion\n'
    spreadsheet_contents += '\t'

    for channel in range(channels):
        spreadsheet_contents += f'Vmeas_CH{channel}\t'
        spreadsheet_contents += '\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'
    spreadsheet_contents += '\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'
    spreadsheet_contents += '\n'
     
    #Display raw measurements and counts values
    for freq in range(len(f_list)):
        spreadsheet_contents += f'{v_in[freq]}\t'

        for channel in range(channels):
            spreadsheet_contents += f'{v_meas[channel][freq]}\t'
            spreadsheet_contents += '\t'

        for channel in range(channels):
            spreadsheet_contents += f'{channel_avg[channel][freq]}\t'
            spreadsheet_contents += '\t'

        for channel in range(channels):
            spreadsheet_contents += f'{conv_to_volts[channel][freq]}\t'

        spreadsheet_contents += '\n'


    #Build the rows and columns with Vmeas_Count vs [Raw Average counts adjusted for channel range values]
    spreadsheet_contents += '\n\n\t\t\t\tVmeas Conv to Counts + Average Adjust\n'
    for channel in range(channels):
        spreadsheet_contents += f'Vm_CNT_CH{channel}\t'
    spreadsheet_contents += '\n'

	#Display raw measurements and counts values
    for freq in range(len(f_list)):
        spreadsheet_contents += f'{v_in[freq]}\t'

        for channel in range(channels):
            spreadsheet_contents += f'{conv_v_meas_to_counts[channel][freq]}\t'
            spreadsheet_contents += '\t'
        for channel in range(channels):
            spreadsheet_contents += f'{channel_avg_adj[channel][freq]}\t'

        spreadsheet_contents += '\n'

    #Build the rows and columns with FSR Error based on avarage counts vs Vmeas counts
    spreadsheet_contents += '\n\n\t\t\t\t\tFSR Error\n\t\t'
    for channel in range(channels):
        spreadsheet_contents += f'CH{channel}\t'
    spreadsheet_contents += '\n'

    for freq in range(len(f_list)):
        spreadsheet_contents +=f'{v_in[freq]}\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'{fsr_error[channel][freq]}\t'
        spreadsheet_contents += '\n'

    system('cls')

    if(cal_gain_offset_coeff.upper() == 'YES'):

        #Append the Gain and Offset co-efficients to the existing Excel spreadsheet
        spreadsheet_contents +='\n\n\t\t\t\tGain and Offset Coefficients\n\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'CH{channel}\t'
        spreadsheet_contents += '\n'
        spreadsheet_contents += 'OFFSET\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'{offset_coeff[channel]}\t'
        spreadsheet_contents += '\n'
        spreadsheet_contents += 'GAIN\t\t'
        for channel in range(channels):
            spreadsheet_contents += f'{gain_coeff[channel]}\t'

        #Append the adjusted error after applying the gain and offset adjustments
        spreadsheet_contents +='\n\n\n\t\t\t\tAdjusted Error after applying Gain and Offset Coefficients\n\t\t'
        for channel in range(channels):
            spreadsheet_contents +=f'CH{channel}\t'
        spreadsheet_contents +='\n'

        #Display raw measurements and counts values
        for freq in range(len(f_list)):
            spreadsheet_contents += f'{v_in[freq]}\t'
            for channel in range(channels):
                spreadsheet_contents += f'{fsr_error_go_adj[channel][freq]}\t'
            spreadsheet_contents += '\n'

        print ('Appending Calculated Offset and Gain Coefficients and adjusted error to the existing result spreadsheet\n')

    #res_file.close()
    
    with open(f'{log_folder}\\{ident}_{str(numfold)}_results.xls','w', encoding='utf-8') as res_file:  # Excel file with counts measurement
        res_file.write(spreadsheet_contents)

def gen_cal_ini_file(log_folder,channels,primary_gain,gain_coeff,offset_coeff):
    '''
    Function to auto generate a Cal.ini file in the format normally used for these fileS.
    The user is prompted to enter module type (ie -KAD/ADC/021), module serial number, and initials
    Probably needs expansion in the future to include G/O coeff for different temperature points
    '''

    print ('*** Generating a Cal.ini File! ***')

    mod_type    = input('\nEnter Module Type (i.e - 0x08F8):')
    module      = input('\nEnter Module Name (i.e - KAD.ADC.136):')
    ser         = input('\nEnter Module Serial number:')
    tester      = input('\nEnter a User Name (Initials):')

    gain_pwr_2_array = [2, 4, 8, 16, 32, 64, 128, 256, 512]     #Gain 2 = AG1, Gain 4 = AG2 etc.
    gain_pwr_10_array = [10, 100, 1000]             #Gain 10 = AG1, Gain 100 = AG2 etc.

    track = 1
    gain_type = 0

    if(primary_gain == 1):
        gain_type = "AG0"
    
    if(primary_gain != 1):
        for gain in gain_pwr_2_array:
            if(primary_gain == gain):
                gain_type = f'AG{track}'
            track += 1

        track = 1

        for gain in gain_pwr_10_array:
            if(primary_gain == gain):
               gain_type = f'AG{track}'
            track += 1

        #Did not find - set to default
        if(gain_type == 0):
            print ('Invalid Primary Gain - Setting Gain Type to AG0')
            gain_type = "AG0"
    
    
    cal_file_contents = '[GENERAL]\n'
    cal_file_contents += f'ModuleType={mod_type}\n'
    cal_file_contents += f'ModuleName={module}\n'
    cal_file_contents += f'SN={ser}\n' %(ser)
    cal_file_contents += f'UserName={tester}\n'
    cal_file_contents += 'Company=ACRA\n'
    cal_file_contents += 'VersionNumber=1\n'
    cal_file_contents += 'LastStationID=AFT1\n'
    cal_file_contents += 'RunNumber=0\n'
    cal_file_contents += '[TEMP]\n'
    cal_file_contents += 'T0=0\n'
    cal_file_contents += '[CAL_T0]\n'

    for channel in range(channels):
        cal_file_contents += f'{gain_type}Ch{channel}GainCalCoefficient={gain_coeff[channel]}\n'
        cal_file_contents += f'{gain_type}Ch{channel}OffsetCalCoefficient={offset_coeff[channel]}\n'

    with open(f'{log_folder}\\Cal.ini','w', encoding='utf-8') as cal_file:
        cal_file.write(cal_file_contents)


def gen_iv_ext_spreadsheet(log_folder,ident,numfold, readings,ave_list,err_list,no_of_readings,k_unit,res_load_readings,res_list):
    '''
    gen_iv_ext_spreadsheet - Generates a spreadsheet that displays the readings, average, and error from a number of DMM samples 
    Format:
             Vmeas    
    Resistance     N       (N = N/A + resistances read from user (optional))      
    Meas_x     V       (x = Numof Readings)
    Average    V
    Error      %

    No graph is currently plotted - Generate excel files based on results.

    '''

    #try:
    #    res_file = open(f'{log_folder}\\{ident,str(numfold)}_results.xls', 'w', encoding='utf-8')  # Excel file with counts measurement
    #except IOError:
    #    print('Cant open the excel result file')
    #    art_exit()

    spreadsheet_contents = f'\tDMM Readings (Measured units in {k_unit})\n\n'

    if(res_load_readings.upper() == 'YES'):
        spreadsheet_contents += 'ResVal\t'
        spreadsheet_contents += 'N/A\t'
        for result in res_list:
            spreadsheet_contents += f'{result}\t'
        spreadsheet_contents += '\n'
        for ix in range(no_of_readings):
            ref = ix
            spreadsheet_contents +=f'Meas_{ix}'
            for _ in range(len(res_list)+1):
                spreadsheet_contents += f'\t{readings[ref]}'
                ref+=no_of_readings
            spreadsheet_contents += '\n'  
        spreadsheet_contents += '\nAverage'
        for ix in range(len(res_list)+1):
            spreadsheet_contents += f'\t{ave_list[ix]}'
        spreadsheet_contents += '\nFSRError'
        for ix in range(len(res_list)+1):
            spreadsheet_contents += f'\t{(err_list[ix])}'
    else:
        spreadsheet_contents += 'ResVal\tN/A\n'
        for ix in range(no_of_readings):
            spreadsheet_contents += f'Meas_{ix}'
            spreadsheet_contents += f'\t{readings[ix]}\n'
        spreadsheet_contents += f'\nAverage\t{ave_list[0]}\n'
        spreadsheet_contents += f'FSRError\t{err_list[0]}\n'
    

    with open(f'{log_folder}\\{ident,str(numfold)}_results.xls', 'w', encoding='utf-8') as res_file: # Excel file with counts measurement
        res_file.write(spreadsheet_contents)

