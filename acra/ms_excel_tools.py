'''

 Name:     ms_excel_tools.py

 Author:   Chris Gilbert

 Date:     September 16, 2022

 Copyright Curtiss Wright 2022

 Description

 Python 3 PEP8 Library class for Excel

  help(modulename) returns useful information

 Useful references:

    https://docs.microsoft.com/en-us/office/vba/api/word.wdbuiltinstyle

 History:

   1.0  CJG 23/09/2022  New library module created


'''

from os import path

import win32com.client
from win32com.client import constants as excel_constants



class ExcelWrapper():
    '''
    Excel class for creating spreadsheets - esp writing data and formatting them
    Based in part on #http://snippets.dzone.com/posts/show/2036,
    and http://www.markcarter.me.uk/computing/python/excel.html
    
    This command needs to be run before attempting to run this library
    
    python -m win32com.client.makepy "Excel.Application"
    '''

    def __init__(self, file_name, make_visible=False):
        '''

        Create an empty workbook containing a single spreadsheet

        The filename must be in windows format

        filename = 'C:\\ACRA\\Test\\COPQ_Libraries\\Python3_PEP8\\PROCESS_UTILS\\acra\\excel_debug.xlsx'

        init is debugged and working.

        Creates an empty dictionary to allow access to the sheets of the workbook, based on the sheet name

        # if customer facing reports are to be generated these fileds need to be populated.        
        self.product_name            = ''
        self.ser_no                  = ''
        self.customer_details        = ''
        self.date_time               = ''
        self.user_name               = ''
        self.user_title              = ''
        self.product_description     = ''
        self.reference_spec          = ''
        
        self.test_temperature        = ''
        self.test_humidity           = ''
        self.report_note             = []           

        '''
        self.file_name = file_name
        self.worksheet = None
        self.worksheet_dict = {}
        
        # default locations for header, cover page and last page images
        self.left_header_image  = 'C:\\ATG\\AFT_Station\\Product\\red_left.png'
        self.right_header_image = "C:\\ATG\\AFT_Station\\Product\\cw.png"
        self.back_cover_image   ="C:\\ATG\\AFT_Station\\Product\\back-cover.png"
        self.company_logo       = 'C:\\ATG\\AFT_Station\\Product\\back-cover.png'
        
        self.front_page_headings     = ['Module ', 'Board S/N ', 'Customer ',  'Test Date','Test Time','Tested By ','Signature','Job Title', 'Description','Reference Specification ']
        
        self.product_name            = ''
        self.ser_no                  = ''
        self.customer_details        = ''
        self.test_date               = ''
        self.test_time               = ''
        self.user_name               = ''
        self.user_signature          = ''
        self.signature               = ''
        self.user_title              = ''
        self.product_description     = ''
        self.reference_spec          = '' 
        
        self.test_temperature        = ''
        self.test_humidity           = ''
        self.report_note             = []   
        
        self.equipment_list = [ ['Function', 'Make', 'Model', 'S/N', 'Calibration Date', 'Next Calibration Date'] ]
        self.document_list = [ ['Document Description', 'Document Reference', 'Issue Date', 'Issue By'] ]
        
        
    
        # crank up Excel
        #self.xcl = win32com.client.dynamic.Dispatch("Excel.Application")
        self.xcl = win32com.client.Dispatch("Excel.Application")
        #self.xcl = win32com.client.gencache.EnsureDispatch("Excel.Application")
        # DEBUG
        #print(win32com.__gen_path__)

        # if in debug mode see everything and interact with everything
        # otherwise do everything on the QT
        if make_visible:
            self.xcl.Visible = 1  # fun to watch!

        else:
            self.xcl.DisplayAlerts = False

        # if the file already exists open it, otherwise create a new spreadsheet
        if path.exists(file_name):
            self.workbook = self.xcl.Workbooks.Open(file_name)

        else:
            self.workbook = self.xcl.Workbooks.Add()

    def save_as(self):
        '''
        Save the workbook with the name supplied
        '''
        self.workbook.SaveAs(self.file_name)

    def close_excel(self):
        '''
        Shut down Excel
        '''
        self.xcl.Application.Quit()

    def list_sheets(self):
        '''
        populate a dictionary of sheets named in the workbook
        dictionary key is the sheet name, data is the sheet object.
        '''

        # clear the dictionary first.
        self.worksheet_dict = {}

        for my_sheet in (self.workbook.Sheets):
            self.worksheet_dict[my_sheet.Name] = my_sheet

    def add_cell(self, column: int, row: int, label):
        '''
        Method to add cells to the sheet.
        '''
        self.worksheet.Cells(row, column).Value = label

    def add_row(self, start_column: int, start_row: int, data: list):
        '''
        Method to add a row to the sheet.

        '''
        for indx, my_data in enumerate(data):
            self.add_cell(start_column+indx, start_row, my_data)

    def add_chart(self, name, sprow, spcell, eprow, epcell, my_chart,
                  x_scale, y_scale, x_ax_title, y_ax_title, chart_type=0x4a):
        '''
        Method to add chart.
        name - the name of the sheet
        sprow - position of the first cell of the row to be added to the plot.
        spcell - position of the first cell of the column to be added
                 to the plot.
        eprow - position of the last cell of the row to be added to the plot.
        epcell - position of the last cell of the column to be added
                 to the plot.
        my_chart - name on the chart
        x_scale - make x axis logarithmic if set to 1
        y_scale -make y axis logarithmic if set to 1
        x_ax_title - sets the name of x axis on the plot
        y_ax_title - sets the name of y axis on the plot
        chart_type = 0x4a by defult. 
        '''

        chart = self.xcl.Charts.Add()
        chart.Activate()
        chart.ChartType = chart_type
        
        # y-Axis Attributes
        #x_axis = chart.Axes(excel_constants.xlCategory, excel_constants.xlPrimary)
        y_axis = chart.Axes(excel_constants.xlValue, excel_constants.xlPrimary)
        if y_scale == 1:
            y_axis.ScaleType = 1
            y_axis.HasMajorGridlines = True
            y_axis.HasMinorGridlines = True
        y_axis.HasTitle = True
        y_axis.AxisTitle.Text = f"{y_ax_title}"

        # Y-Axis Attributes
        #x_axis = chart.Axes(excel_constants.xlValue, excel_constants.xlPrimary)
        x_axis = chart.Axes(excel_constants.xlCategory, excel_constants.xlPrimary)
        if x_scale == 1:
            x_axis.ScaleType = 1
            x_axis.HasMajorGridlines = True
            x_axis.HasMinorGridlines = True
        x_axis.HasTitle = True
        x_axis.AxisTitle.Text = f"{x_ax_title}"
        
        chart.Name = f"{name}"
        chart.HasLegend = True
        chart.HasTitle = True
        chart.ChartTitle.Text = f"{my_chart}"
        chart.ChartTitle.Font.Size = 11
        data_range = self.worksheet.Range(self.worksheet.Cells(sprow, spcell),
                                          self.worksheet.Cells(eprow, epcell))
        chart.SetSourceData(data_range, 2)
        
    def add_chart_to_sheet(self, name, sprow, spcell, eprow, epcell, my_chart,
                  x_scale, y_scale, x_ax_title, y_ax_title, chart_type=0x4a,
                  left=20, top=60, width=420, height=240):
        '''
        Method to add chart to an existing sheet..
        name - the name of the sheet
        sprow - position of the first cell of the row to be added to the plot.
        spcell - position of the first cell of the column to be added
                 to the plot.
        eprow - position of the last cell of the row to be added to the plot.
        epcell - position of the last cell of the column to be added
                 to the plot.
        my_chart - name on the chart
        x_scale - make x axis logarithmic if set to 1
        y_scale -make y axis logarithmic if set to 1
        x_ax_title - sets the name of x axis on the plot
        y_ax_title - sets the name of y axis on the plot
        chart_type = 0x4a by defult. 
        '''
       
        chart_objects = self.worksheet.ChartObjects()
        chart_object = chart_objects.Add(left, top, width, height)
        chart = chart_object.Chart

        chart.ChartType = chart_type
        
        # y-Axis Attributes
        #x_axis = chart.Axes(excel_constants.xlCategory, excel_constants.xlPrimary)
        y_axis = chart.Axes(excel_constants.xlValue, excel_constants.xlPrimary)
        if y_scale == 1:
            y_axis.ScaleType = 1
            y_axis.HasMajorGridlines = True
            y_axis.HasMinorGridlines = True
        y_axis.HasTitle = True
        y_axis.AxisTitle.Text = f"{y_ax_title}"

        # Y-Axis Attributes
        #x_axis = chart.Axes(excel_constants.xlValue, excel_constants.xlPrimary)
        x_axis = chart.Axes(excel_constants.xlCategory, excel_constants.xlPrimary)
        if x_scale == 1:
            x_axis.ScaleType = 1
            x_axis.HasMajorGridlines = True
            x_axis.HasMinorGridlines = True
        x_axis.HasTitle = True
        x_axis.AxisTitle.Text = f"{x_ax_title}"
        
        #chart.Name = f"{name}"
        chart.HasLegend = True
        chart.HasTitle = True
        chart.ChartTitle.Text = f"{my_chart}"
        chart.ChartTitle.Font.Size = 11
        data_range = self.worksheet.Range(self.worksheet.Cells(sprow, spcell),
                                          self.worksheet.Cells(eprow, epcell))
        chart.SetSourceData(data_range, 2)

    def add_column(self, start_column: int, start_row: int, data: list):
        '''
        Add solumn to the sheet
        '''
        for indx, my_data in enumerate(data):
            self.add_cell(start_column, start_row+indx, my_data)

    def activate_sheet(self, sheet_name):
        '''
        make the named sheet the active sheet
        No error checking. The named sheet must exist.
        '''
        self.worksheet = self.workbook.Worksheets(sheet_name)
        self.worksheet.Activate()

    def add_sheet(self, sheet_name):
        '''
        Add a new sheet to the workbook
        
        If the sheet already exists in teh workbook, open it, 
        otherwise see if Sheet1 exists. If it does rename it and make it the active sheet, 
        else create the new sheet and activate it.
        '''

        self.list_sheets()

        if sheet_name in self.worksheet_dict:
                self.worksheet = self.workbook.Worksheets(sheet_name)
                self.worksheet.Activate()

        elif "Sheet1" in self.worksheet_dict:
            self.worksheet = self.workbook.Worksheets("Sheet1")
            self.worksheet.Activate()        
            self.worksheet.Name = sheet_name

        else:
            # newest sheet is the active sheet
            self.worksheet = self.workbook.Worksheets.Add()
            self.worksheet.Name = sheet_name

    def add_dotted_line(self, cell, row):
        '''
        Adds a dotted line to the cell, row specified in the active sheet
        '''
        self.worksheet.Cells(cell, row).Borders(
            excel_constants.xlEdgeBottom).LineStyle = excel_constants.xlDot
        self.worksheet.Cells(cell, row).Borders(
            excel_constants.xlEdgeBottom).Weight = excel_constants.xlThin

    def align_cell_horiz_centre(self, cell, row):
        '''
        Set the cell to horizontally align centre
        '''
        self.worksheet.Cells(
            cell, row).HorizontalAlignment = excel_constants.xlCenter

    def cell_left_border(self, cell, row):
        '''
        highlight the left cell border with a continuous line
        '''
        self.worksheet.Cells(cell, row).Borders(
            excel_constants.xlEdgeLeft).LineStyle = excel_constants.xlContinuous

    def cell_right_border(self, cell, row):
        '''
        highlight the right cell border with a continuous line
        '''
        self.worksheet.Cells(cell, row).Borders(excel_constants.xlEdgeRight).LineStyle = excel_constants.xlContinuous

    def cell_top_border(self, cell, row):
        '''
        highlight the top cell border with a continuous line
        '''
        self.worksheet.Cells(cell, row).Borders(excel_constants.xlEdgeTop).LineStyle = excel_constants.xlContinuous

    def cell_bottom_border(self, cell, row):
        '''
        highlight the top cell border with a continuous line
        '''
        self.worksheet.Cells(cell, row).Borders(excel_constants.xlEdgeBottom).LineStyle = excel_constants.xlContinuous

    def cell_all_border(self, cell, row):
        '''
        Horizontally align the cell for centre
        add all borders to the cell
        '''
        # ws.Cells(2+i,1).HorizontalAlignment = win32.constants.xlCenter

        self.align_cell_horiz_centre(cell, row)

        self.cell_left_border(cell, row)
        self.cell_right_border(cell, row)
        self.cell_top_border(cell, row)
        self.cell_bottom_border(cell, row)
        
    def heading_centre(self):
        '''
        create the page header centre text
        '''
        c_header =  f'Module: {self.product_name}\n'
        c_header += f'Verification Report Number: {self.ser_no}\n'
        c_header += f'{self.test_date} {self.test_time}'
        return c_header
        
    def footer_centre(self):
        '''
        create the footer centre text
        '''
        c_footer = ''
        return c_footer        

    def footer_right(self):
        '''
        create the footer right text
        '''
        r_footer =  f'{self.worksheet.Name}'
        return r_footer        



    def common_sheet_format(self):
        '''
        Common/standard formatting for each sheet

        does not include images

        Zoom defaulted to 'false'

        '''

        self.worksheet.PageSetup.LeftHeader = "&G"
        self.worksheet.PageSetup.CenterHeader = self.heading_centre()
        self.worksheet.PageSetup.RightHeader = "&G"
        self.worksheet.PageSetup.LeftFooter = ""
        self.worksheet.PageSetup.CenterFooter = self.footer_centre()
        self.worksheet.PageSetup.RightFooter = self.footer_right()
        self.worksheet.PageSetup.LeftMargin = self.xcl.InchesToPoints(0.25)
        self.worksheet.PageSetup.RightMargin = self.xcl.InchesToPoints(0.25)
        self.worksheet.PageSetup.TopMargin = self.xcl.InchesToPoints(0.75)
        self.worksheet.PageSetup.BottomMargin = self.xcl.InchesToPoints(0.75)
        self.worksheet.PageSetup.HeaderMargin = self.xcl.InchesToPoints(0.3)
        self.worksheet.PageSetup.FooterMargin = self.xcl.InchesToPoints(0.3)
        self.worksheet.PageSetup.PrintHeadings = False
        self.worksheet.PageSetup.PrintGridlines = False

        self.worksheet.PageSetup.CenterHorizontally = False
        self.worksheet.PageSetup.CenterVertically = False

        self.worksheet.PageSetup.Draft = False

        self.worksheet.PageSetup.BlackAndWhite = False
        self.worksheet.PageSetup.Zoom = False

        self.worksheet.PageSetup.OddAndEvenPagesHeaderFooter = False
        self.worksheet.PageSetup.DifferentFirstPageHeaderFooter = False
        self.worksheet.PageSetup.ScaleWithDocHeaderFooter = True
        self.worksheet.PageSetup.AlignMarginsHeaderFooter = True
        self.worksheet.PageSetup.EvenPage.LeftHeader.Text = ""
        self.worksheet.PageSetup.EvenPage.CenterHeader.Text = ""
        self.worksheet.PageSetup.EvenPage.RightHeader.Text = ""
        self.worksheet.PageSetup.EvenPage.LeftFooter.Text = ""
        self.worksheet.PageSetup.EvenPage.CenterFooter.Text = ""
        self.worksheet.PageSetup.EvenPage.RightFooter.Text = ""
        self.worksheet.PageSetup.FirstPage.LeftHeader.Text = ""
        self.worksheet.PageSetup.FirstPage.CenterHeader.Text = ""
        self.worksheet.PageSetup.FirstPage.RightHeader.Text = ""
        self.worksheet.PageSetup.FirstPage.LeftFooter.Text = ""
        self.worksheet.PageSetup.FirstPage.CenterFooter.Text = ""
        self.worksheet.PageSetup.FirstPage.RightFooter.Text = ""

    def contents_sheet_format(self):
        '''
        Changes the page borders specifically for the contents page
        Must be called AFTER common_sheet_format

        '''

        self.worksheet.PageSetup.LeftMargin = self.xcl.InchesToPoints(1.0)
        self.worksheet.PageSetup.RightMargin = self.xcl.InchesToPoints(1.0)
        

    def sheet_format(self): 
        '''
        Format the page so that it is pretty in the final spreadsheet
        
        '''
        # start with the common format then add page specific features.
        self.common_sheet_format()

        try:
            self.worksheet.PageSetup.LeftHeaderPicture.Filename = self.left_header_image

        except IOError:
            print (f'Left Header Picture not found: {self.left_header_image}')

        try:
            self.worksheet.PageSetup.RightHeaderPicture.Filename = self.right_header_image
        except IOError:
            print (f'Right Header Picture not found: {self.right_header_image}')

    def last_sheet_format(self):
        '''
        Format the last page of the report
        
        This is ALWAYS the FIRST page to be added when creating a workbook
        '''

        self.common_sheet_format()

        try:
            self.worksheet.PageSetup.LeftHeaderPicture.Filename = self.back_cover_image
        except IOError:
            print (f'Last page image not found: {self.back_cover_image}')
        
        #need to populate a single cell so that the page prints
        self.worksheet.Cells(1, 1).Value = (".")

        # change the zoom to 100%
        self.worksheet.PageSetup.Zoom = 100
        
    
    def add_contents_sheet(self):
        '''
        Add the contents sheet
        
        Excluding the front page and contesnts page,
        all of the other sheets in the report must exist, 
        before the contents page is added to the workbook
        
        This should be the second last page to be added to a workbook 
        '''
        
        # list all of the available sheets BEFORE adding the contents page...
        self.list_sheets()
        
        self.add_sheet('2. Contents Page')
        self.sheet_format()
        self.contents_sheet_format()

        # column definitions        
        col_a = 1
        col_b = 2
        col_c = 3

        #row variable
        row_1 = 4
        row = row_1
        sheet_ix = 1

        self.worksheet.Cells(row,col_a).Value = 'Contents'
        self.worksheet.Cells(row,col_c).Value = 'Page'
        self.worksheet.Range(f"A{row}", f"C{row}").Font.Bold = True
        
        row += 2

        self.worksheet.Cells(row,col_a).Value = 'Module'
        self.worksheet.Cells(row,col_b).Value = self.product_name
        self.worksheet.Cells(row,col_c).Value = f'{sheet_ix}'

        row += 1
        
        self.worksheet.Cells(row,col_a).Value = 'Board S/N'
        self.worksheet.Cells(row,col_b).Value = self.ser_no
        self.worksheet.Cells(row,col_c).Value = f'{sheet_ix}'

        row += 1
        
        self.worksheet.Cells(row,col_a).Value = 'Test Date'
        self.worksheet.Cells(row,col_b).Value = f'{self.test_date} {self.test_time}'
        self.worksheet.Cells(row,col_c).Value = f'{sheet_ix}'

        row += 1

        self.worksheet.Cells(row,col_a).Value = 'Tested By'
        self.worksheet.Cells(row,col_b).Value = self.user_name
        self.worksheet.Cells(row,col_c).Value = f'{sheet_ix}'

        row += 1
        sheet_ix += 1

        self.worksheet.Cells(row,col_a).Value = 'Contents'
        self.worksheet.Cells(row,col_c).Value = f'{sheet_ix}'
        self.add_dotted_line(row,col_a)
        self.add_dotted_line(row,col_b)
        self.add_dotted_line(row,col_c)


        sheet_ix += 1
        row += 1
        
        for key, _ in self.worksheet_dict.items():

            self.worksheet.Cells(row,col_a).Value = f'{key}'
            self.add_dotted_line(row,col_a)
            self.worksheet.Cells(row,col_c).Value = f'{sheet_ix}'
            self.add_dotted_line(row,col_a)
            self.add_dotted_line(row,col_b)
            self.add_dotted_line(row,col_c)
            sheet_ix += 1
            row += 1

        self.worksheet.Range("A1", f"A{row-1}").ColumnWidth = 27
        self.worksheet.Range("B1", f"B{row-1}").ColumnWidth = 27
        self.worksheet.Range("C1", f"C{row-1}").ColumnWidth = 15
        
        self.worksheet.Range("A1", f"A{row-1}").Font.Color = excel_constants.rgbRed
        self.worksheet.Range("B1", f"B{row-1}").HorizontalAlignment = excel_constants.xlCenter
        self.worksheet.Range("C1", f"C{row-1}").HorizontalAlignment = excel_constants.xlRight
        self.worksheet.Range("A1", f"C{row-1}").VerticalAlignment = excel_constants.xlCenter
        
    def add_front_sheet(self):
        '''
        Add a standard front page
        using the headings in the list.
        
        this is ALWAYS the LAST sheet to be added when creating a workbook
        '''

        self.add_sheet('1. Front Page')
        self.sheet_format()

        # self.worksheet.col(1).width = 14000
        # self.worksheet.col(2).width = 3000
        # self.worksheet.col(3).width = 7000

        front_page_info = [ self.product_name,
                            self.ser_no,
                            self.customer_details,
                            self.test_date,
                            self.test_time,
                            self.user_name,
                            self.user_signature,
                            self.user_title,
                            self.product_description,
                            self.reference_spec 
                          ]

        col_a = 1
        col_b = 2

        # start on row 4 so that header has enough room on front page.
        row_1 = 4
        row = row_1         
        
        #print (f'len(front_page_info) : {len(front_page_info)}')
        for index, key in enumerate (self.front_page_headings):
        
            #print (f'index: {index}\tkey: {key}\tfront_page_info[index]: {front_page_info[index]}')
            
            self.worksheet.Cells(row, col_a).Value = key
            
            if key.upper() == 'SIGNATURE':
                self.add_dotted_line(row, col_b)
            
            else:    
            
                self.worksheet.Cells(row, col_b).Value = front_page_info[index]
            
            row += 1
                
        self.worksheet.Range(f"A{row_1}", f"A{row-1}").Font.Color = excel_constants.rgbRed
        self.worksheet.Range(f"A{row_1}", f"A{row}").Font.Bold = True
        self.worksheet.Range(f"B{row_1}", f"C{row-1}").HorizontalAlignment = excel_constants.xlCenter
        self.worksheet.Range(f"A{row_1}", f"C{row-1}").VerticalAlignment = excel_constants.xlCenter
        
        
        self.worksheet.Columns.AutoFit()
        

        # Now add the company stamp/logo
        row += 2

        left = self.worksheet.Cells(row, col_a).Left
        top = self.worksheet.Cells(row, col_a).Top

        # width and height need to be adjusted for the correct stamp
        # the numbers below are just a wild guess.
        width = 50
        height = 50
        
        self.worksheet.Shapes.AddPicture(self.company_logo,LinkToFile=False, SaveWithDocument=True, Left=left, Top=top, Width=width, Height=height)    
        

    def add_last_sheet(self):
        '''
        Add a standard last page to the formal report
        this page must always be the first page added to a spreadsheet.
        This renames "Sheet1" and configures it as the last page in the workbook
        '''
 
        self.add_sheet('Last page')
        self.last_sheet_format()        


    def add_equipment_sheet(self):        
        '''
        Add the contents of the equipment list to a sheet in standard format.
        '''
        
        self.add_sheet('3. Equipment Used')
        self.sheet_format()
        
        # column definitions        
        col_a = 1
        col_b = 2
        col_c = 3
        
        degree = chr(0xB0)

        #row variable
        row_1 = 4
        row = row_1
        
        self.worksheet.Cells(row,col_a).Value = 'Verification Conditions'
        self.worksheet.Cells(row,col_a).Font.Color = excel_constants.rgbRed
        self.worksheet.Range(f"A{row}", f"A{row}").Font.Bold = True
        
        row += 1

        self.worksheet.Cells(row,col_a).Value = 'Quantity'
        self.worksheet.Cells(row,col_b).Value = 'Precision'
        self.worksheet.Cells(row,col_c).Value = 'Measured'
        self.worksheet.Range(f"A{row}", f"C{row}").Font.Bold = True
        self.worksheet.Cells(row,col_a).HorizontalAlignment = excel_constants.xlLeft
        self.cell_all_border(row,col_a)
        self.cell_all_border(row,col_b)
        self.cell_all_border(row,col_c)

        row += 1

        self.worksheet.Cells(row,col_a).Value = 'Temperature'
        self.worksheet.Cells(row,col_b).Value = f'+/-5{degree}C'
        self.worksheet.Cells(row,col_c).Value = f' {self.test_temperature}{degree}C'
        self.worksheet.Cells(row,col_a).HorizontalAlignment = excel_constants.xlLeft
        self.cell_all_border(row,col_a)
        self.cell_all_border(row,col_b)
        self.cell_all_border(row,col_c)

        row += 1

        self.worksheet.Cells(row,col_a).Value = 'Relative Humidity'
        self.worksheet.Cells(row,col_b).Value = '20% RH to 80% RH'
        self.worksheet.Cells(row,col_c).Value = f' {self.test_humidity}%RH'
        self.worksheet.Cells(row,col_a).HorizontalAlignment = excel_constants.xlLeft
        self.cell_all_border(row,col_a)
        self.cell_all_border(row,col_b)
        self.cell_all_border(row,col_c)

        row += 3

        self.worksheet.Cells(row,col_a).Value = 'Equipment Used'
        self.worksheet.Cells(row,col_a).Font.Color = excel_constants.rgbRed
        self.worksheet.Range(f"A{row}", f"A{row}").Font.Bold = True

        row += 1
        
        # Unroll the equipment list.
        # First row is bold
        header = True
        for equipment_details in self.equipment_list:
            col = 1
            for entry in equipment_details:
                self.worksheet.Cells(row,col).Value = entry
            
                if header:
                    self.worksheet.Cells(row,col).Font.Bold = True    
        
                self.cell_all_border(row,col)

                if col == 1:
                    self.worksheet.Cells(row,col).HorizontalAlignment = excel_constants.xlLeft                
                
                col += 1
                
            row += 1
            
            # special formatting on the first row only
            if header:
                header = False

        row += 3

        self.worksheet.Cells(row,col_a).Value = 'Documents Used'
        self.worksheet.Cells(row,col_a).Font.Color = excel_constants.rgbRed
        self.worksheet.Range(f"A{row}", f"A{row}").Font.Bold = True

        row += 1

        # Unroll the document list.
        # First row is bold
        header = True
        for document_details in self.document_list:
            col = 1
            for entry in document_details:
                self.worksheet.Cells(row,col).Value = entry
            
                if header:
                    self.worksheet.Cells(row,col).Font.Bold = True    
        
                self.cell_all_border(row,col)
                
                if col == 1:
                    self.worksheet.Cells(row,col).HorizontalAlignment = excel_constants.xlLeft
                
                col += 1
                
            row += 1
            
            # special formatting on the first row only
            if header:
                header = False

        self.worksheet.Columns.AutoFit()
        
        # this bit does need some cleaning up
        # now for standard notes
        standard_note =  [[f'The production test will test to a tighter specification that the Data sheet to allow for a{chr(0x0A)}']]
        standard_note.append([f'number of factors, one of which is to try to ensure any drift over time will not impact the{chr(0x0A)}'])
        standard_note.append(['accuracy of the module.'])
        
        row += 3
        
        self.worksheet.Cells(row,col_a).Value = 'Note'
        self.worksheet.Cells(row,col_a).Font.Bold = True
        
        row += 1
        
        for line in standard_note:
            self.worksheet.Cells(row,col_a).Value = line
            self.worksheet.Cells(row,col_a).WrapText = False
            row += 1

        
        # if there is a report note add it. 
        # Assumes the first line is a heading of some sort.
        if len(self.report_note) != 0:
            header = True
            row += 2
            for line in self.report_note:
                self.worksheet.Cells(row,col_a).Value = line
                self.worksheet.Cells(row,col_a).WrapText = False
                if header:
                    self.worksheet.Cells(row,col_a).Font.Bold = True
                    header = False
                row += 1
            


if __name__ == '__main__':

    # filename must incude the path, otherwise it will be saved in "documents"

    from time import sleep
    from random import randint
    
    from acra.debugging_tools import dummy_ac_accuracy

    FILENAME = 'C:\\ACRA\\Test\\COPQ_Libraries\\Python3_PEP8\\PROCESS_UTILS\\acra\\excel_debug.xlsx'
    MAKE_VISIBLE = True

    my_excel = ExcelWrapper(FILENAME, MAKE_VISIBLE)
    
    # front page headings is the heading, front_page_info is the data that corresponds to those headings
    # information that is needed for an excel report to be generated.
    my_excel.product_name            = 'KAD/ABC/123/E/FG1/00'
    my_excel.ser_no                  = 'ABC1234'
    my_excel.customer_details        = 'Curtiss Wright Corporation,\nUnit 5 Richview Office Park,\nClonskeagh D14X981,\nIreland'
    my_excel.test_date               = 'September 26, 2022'
    my_excel.test_time               = '12:34:56'
    my_excel.user_name               = 'Joe Bloggs'
    my_excel.user_title              = 'Senior Test Technician'
    my_excel.product_description     = 'One line description from datasheet'
    my_excel.reference_spec          = '5 May 2019 | DST/AB/123'    
    
    my_excel.test_temperature = '23'   
    my_excel.test_humidity    = '60'
    
    # report_note is a list of lines...
    # otherwise Excel will run the line across the width of the page and beyond
    my_excel.report_note   = [[f'Datasheet Specifications{chr(0x0A)}']]
    my_excel.report_note.append([f'RTD inputs DC error{chr(0x0A)}'])
    my_excel.report_note.append([f'For FSR of -200{chr(0xB0)} to 660{chr(0xB0)} maximum 0.27 %%FSR With 1mA excitation current set.{chr(0x0A)}'])
    my_excel.report_note.append([f'For FSR of -60{chr(0xB0)} to 250{chr(0xB0)} maximum 0.35 %%FSR With 2mA excitation current set.'])
    

    # dummy equipment and document lists 
    my_excel.equipment_list.append(['Multimeter 8.5 Dig OPT 2', 'HP/Agilent', '3458A', '2823A23633', '24-5-2019', '24-5-2020'])
    my_excel.equipment_list.append(['Signal Generator', 'SRS (Stanford)', 'DS360', '123522', '30-9-2019', '30-9-2020'])
    my_excel.equipment_list.append(['Power Supply unit', 'GW-Instek', 'EP84', '2955', '10-1-2020', '10-1-2021'])
    my_excel.equipment_list.append(['Test PC ACPI Multiprocessor PC', 'N/A', 'No', 'N/A', 'N/A', 'N/A'])
    my_excel.equipment_list.append(['DAS Studio 3.3', 'Curtiss Wright', 'N/A', 'N/A', 'N/A', 'N/A'])
    my_excel.equipment_list.append(['KAD/BCU/140/D', 'Curtiss Wright', 'AAB5247', 'N/A', 'N/A', 'N/A'])

    my_excel.document_list.append(['AFT Test Procedure', 'ATI/AC/052', '20 June 2016', 'SRM'])
    my_excel.document_list.append(['AFT Test Description', 'DOC/RPT/024/AH', '23 February 2015', 'SRM'])


    # Start populating the sheets from the last page and work in reverse order to the front page
    my_excel.add_last_sheet()
    

    sheetnames = ['7. AC Accuracy', '6. DC Accuracy' ]
    _data = ['Freq', 'CH0', 'CH1', 'CH2', 'Typ_Spec', 'Min_Spec']

    for sheet in sheetnames:
        my_excel.add_sheet(sheet)
        my_excel.common_sheet_format()
        
        if sheet == '7. AC Accuracy':
        
            COLUMN = 1
            ROW    = 25
            NO_CHS = 16
            
            no_rows = ROW
        
            result_data = dummy_ac_accuracy(NO_CHS)
            
            #print (result_data)
            #input ('hit enter')
            
            for row in result_data:
                my_excel.add_row(COLUMN, no_rows, row)
                no_rows += 1
                no_cols = len(row)
   
                    #add_chart_to_sheet( name,       sprow, spcell, eprow, epcell, my_chart, x_scale, y_scale, x_ax_title, y_ax_title
            my_excel.add_chart_to_sheet('AC Accuracy', ROW,      COLUMN, no_rows,    no_cols, 'AC Accuracy Chart', 0, 0, 'Frequency', 'Attenuation')
    
        sleep(1)

    fake_data = [2, 3, 4, 5, 6, 7, 8, 9]
    my_excel.add_column(1, 2, fake_data)

    fake_data = ['A', 'B', 'C', 'D', 'E', 'F']
    my_excel.add_row(1, 1, fake_data)

    my_excel.add_sheet('5. SINAD')

    COLUMN = 1
    ROW = 1
    my_excel.add_row(COLUMN, ROW, _data)
    
    for var in range(2, 8):
        my_excel.add_row(
            COLUMN, var, [var*10,
                          randint(60, 90),
                          randint(60, 90),
                          randint(60, 90), 90, 70])

    my_excel.add_chart('4. SINAD_CHART', 1, 1, var, len(_data), 'MY_SINAD_CHART',
                       0, 1, 'Y-axis', 'X-axis')

 
 
    my_excel.add_equipment_sheet()
    my_excel.add_contents_sheet()   
    my_excel.add_front_sheet()

    my_excel.save_as()
    my_excel.close_excel()
