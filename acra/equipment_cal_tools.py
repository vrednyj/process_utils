'''

=============================================================================

 Name:     equipment_cal_tools.py

 Author:   Chris Gilbert

 Date:     October 21, 2022
 
 Copyright Curtiss Wright 2022

 Description

 Tools to make tracking calibration and test dates of equipment easier
 
 No direct calls to excel, instead uses ms_excel_tools to access the 
 calibration database.
 
=============================================================================

'''

from sys import exit as cal_tools_exit
from os import path
from time import time, strptime, mktime

#from time import time, mktime, strftime, strptime, ctime

from acra.ms_excel_tools import ExcelWrapper as BaseExcelWrapper


def get_equ_cal_info(equipment_list, path_to_cal_db = ''):
    '''
    Pull the calibration information from the database
    
    Assumes the equipment list is limited to the first enteries
    
    'Function', 'Make', 'Model', 'S/N'
    
    Appends last calibration date first, then next calibration date to complete the #
    table
    
    'Function', 'Make', 'Model', 'S/N', 'Calibration Date', 'Next Calibration Date'
    
    Returns the equipment list, but with last calibration and next calibration dates populated
    
    If the make and/or model do not match between the equipment list and the calibration 
    database prints an error to the screen and returns invalid next calibration date which 
    will cause the test framework to exit without running the test.
    
    Make and Model have to be an exact match between the database and the equipment list.
    
    if path_to_cal_db = '' the path defaults to the spreadsheet on Node1. 
    
    '''
    
    debug = False
    
    if path_to_cal_db == '':
        path_to_cal_db = '\\\\node1\\calibration\\DBS 0001 (Calibration Database).xlsx'

    if not path.exists(path_to_cal_db):
        print (f'Unable to access calibration database:{path_to_cal_db}\nExiting test')
        cal_tools_exit()
    
    else:
        # read the contents of the equipment sheet into the calibration database 
        # AND THEN CLOSE THE EXCEL file!
        make_visible = False
        sheet_name   = 'Equipment'
        
        # assume the serial number and any FA/OPS/APPS ID are NOT in the same column
        separate_sn = True
        
        local_equipment_list = equipment_list
        
        cal_db = BaseExcelWrapper(path_to_cal_db, make_visible)
        cal_db.list_sheets()
        
        if sheet_name in cal_db.worksheet_dict:
                cal_db.worksheet = cal_db.workbook.Worksheets(sheet_name)
                cal_db.worksheet.Activate()        
        
        else:
            print (f'Unable to access Equipment sheet in calibration database:{path_to_cal_db}\nExiting test')
            cal_tools_exit()


        # check the format of the sheet
        # A1 will either be 
        #   "ID No." (new format)
        #       or 
        #   "S/N (or ID No.)" (old format)
        content = str(cal_db.worksheet.Cells(1, 1))
        
        if "S/N" in content:
            separate_sn = False
            if debug:
                print ('DEBUG: ID and serial are in the same column')
        else:    
            if debug:
                print ('DEBUG: ID and serial are in NOT the same column')
        
        cal_data = cal_db.worksheet.UsedRange

        # don't want to read any cols after "N" as they can contain 
        # degree and other non UTF-8 characters
        max_cols_of_interest = 14
        
        cal_cols = cal_data.Columns.Count
        if cal_cols < max_cols_of_interest:
            max_cols_of_interest = cal_cols
        
        # figure out the offsets, based on the format of the calibration database  
        # all offsets are defined relative to each other rather than as absolute values 
        # to allow for the different formats possible for the calibration database
        id_os      = 1                      # col A/1
        sn_os      = id_os 
        
        if separate_sn:
            sn_os  += 1  
            
        make_os    = sn_os + 6              # col G/7
        model_os   = make_os + 1            # Col H/8
        lastcal_os = model_os + 1           # col I/9
        nextcal_os = lastcal_os + 3         # col L/12
        
        if debug:
            print (f'id_os      : {id_os}')
            print (f'sn_os      : {sn_os}')
            print (f'make_os    : {make_os}')
            print (f'model_os   : {model_os}')
            print (f'lastcal_os : {lastcal_os}')
            print (f'nextcal_os : {nextcal_os}')
        
       
        for ix, equip in enumerate(local_equipment_list):
            if len(equip) > 0:
                
                # only the ID is used so far.
                # use uppercase so that type case issues are avoided
                eq_type  = equip[0].upper()
                eq_manu  = equip[1].upper()
                eq_model = equip[2].upper()
                eq_id    = equip[3].upper()
                
                if eq_id == 'N/A':
                    local_equipment_list[ix].append('N/A')
                    local_equipment_list[ix].append('N/A')
                
                else:
                    eq_type = eq_type.replace('\t', '')
                    eq_type = eq_type.replace(' ', '')

                    eq_manu = eq_manu.replace('\t', '')
                    eq_manu = eq_manu.replace(' ', '')

                    eq_model = eq_model.replace('\t', '')
                    eq_model = eq_model.replace(' ', '')


                    # Is the identification a serial number or an FA/OPS/APPS ID?
                    is_id = False
                    id_found = False            # this ID has not bee found in the cal database yet...

                    for pre_id in ['FA', 'OPS', 'APPS']:
                        if pre_id in eq_id:
                            is_id = True
                            break

                    if debug:
                        print(f'DEBUG checking cal database for {eq_type} {eq_manu} {eq_model} {eq_id}')

                    # assume the equipment details in the equipment list and calibration database match                
                    equip_match = True
                    for cell in range(2, cal_data.Rows.Count+1):
                        # if the cal database has separate columns for ID and serial number
                        # and a serial number has been provided check the serial number only column
                        # otherwise just check A1 as it is either ID or S/N(ID)
                        if not is_id and separate_sn:
                            content = (str(cal_db.worksheet.Cells(cell, sn_os))).upper()        # start at A2
                        else:
                            content = (str(cal_db.worksheet.Cells(cell, id_os))).upper()        # start at A1

                        if debug:
                            print (f'DEBUG: cal db ID: {content} eq_id {eq_id}')

                        if eq_id in (content):
                            if debug:
                                print(f'DEBUG: Found {eq_id} in calibration database')

                            # pull the make and model details from the cal database
                            # make everything uppercase befor eperforming comparison
                            db_manu = str(cal_db.worksheet.Cells(cell, make_os)).upper()
                            db_model = str(cal_db.worksheet.Cells(cell, model_os)).upper()

                            # ignore any decimal points in cal database models
                            # whether placed by excel or deliberately entered into the cal db
                            if db_model.find('.'):
                                split_model = db_model.split('.')
                                db_model = split_model[0]

                            if debug:
                                print (f'DEBUG: cal_database Make: {db_manu} Model : {db_model}')

                            # if the make and/or model do not match then flag an error and set the next calibration 
                            # date to one which will cause the framework to exit.

                            cmp_eq_man = eq_manu.replace('\t', '')
                            cmp_eq_man = cmp_eq_man.replace(' ', '')

                            cmp_db_man = db_manu.replace('\t', '')
                            cmp_db_man = cmp_db_man.replace(' ', '')

                            if cmp_eq_man != cmp_db_man:
                                print (f'ERROR: Equipment Manufacturer Mismatch ({eq_manu} vs {db_manu}) between equipment list and calibration database for {eq_id}')
                                equip_match = False


                            cmp_eq_model = eq_model.replace('\t', '')
                            cmp_eq_model = cmp_eq_model.replace(' ', '')

                            cmp_db_model = db_model.replace('\t', '')
                            cmp_db_model = cmp_db_model.replace(' ', '')

                            if cmp_eq_model != cmp_db_model:
                                print (f'ERROR: Equipment Model Mismatch ({eq_model} vs {db_model}) between equipment list and calibration database for {eq_id}')
                                equip_match = False

                            for pos in range(max_cols_of_interest):
                                if pos in [lastcal_os,nextcal_os]:       # last cal date is Col I, next cal date is Col L, start counting from 0
                                    if debug:
                                        date_string = str(cal_db.worksheet.Cells(cell, pos))
                                        print (f'DEBUG: date_string: {date_string}')
                                    date,_ = str(cal_db.worksheet.Cells(cell, pos)).split()

                                    # default date format from excel is yyyy-mm-dd
                                    if not equip_match:
                                        date = '2000-01-01'            

                                    local_equipment_list[ix].append(date)

                                    if debug:
                                        print (f'DEBUG: Date: {date}')

                            # no need to keep scrolling through the cal databse if we've found the equipment
                            id_found = True
                            break

                # if the equipment has not been found in the database force the test to exit
                if (not id_found) and (eq_type.upper() != 'FUNCTION'):
                    date = '2000-01-01'
                    local_equipment_list[ix].append(date)       # invalid last cal
                    local_equipment_list[ix].append(date)       # invalid next cal

        
        cal_db.close_excel()        
        
        return local_equipment_list
        


def check_equipment_in_cal(local_equipment_list):
    '''
    Takes the populated equipment list and checks the calibration due date
    
    if any instrument is out of cal returns 'False', otherwise returns 'True'
    
    '''
    
    # assume everything is in calibration, until we learn otherwise
    return_value = True
    
    # need todays date to check if calibration is valid
    now = time()
    
    # the first line in the list is a header.
    list_header = True
    
    for equipment in local_equipment_list:
        if not list_header:
            #print ('DEBUG')
            #print (equipment)
            #print ('DEBUG')
            eq_type     = equipment[0]
            eq_make     = equipment[1]
            eq_model    = equipment[2]
            eq_id       = equipment[3]
            
            if eq_id != 'N/A':
                #eq_last_cal = equipment[4]
                eq_next_cal = equipment[5]

                eq_caldate =  mktime(strptime(eq_next_cal, "%Y-%m-%d"))   

                if (eq_caldate < now):
                    return_value = False
                    print(f'ERROR: Equipment {eq_type}:{eq_make}:{eq_model} {eq_id} is overdue calibration {eq_next_cal}\n')
                
        list_header = False

    return return_value
    

def get_jig_cal_info(jig_list, path_to_cal_db = ''):
    '''
    Sister function to get_equ_cal_info() for jigs
    
    Has to be a different funciton as jigs can exist on 2 sheets in the calibration database,
    and the format of those sheets is different...
    
    for jig in test:
        if not (get last test and next test dates from sheet 1)
            if not (get last test and next test dates from sheet 2)
                no info on jig found, so exit
    
    '''
    
    make_visible = False         # turn on for debug.
    debug        = False
    
    max_cols_of_interest  = 9
    
    sheet_names = ['JIGS For test only', 'JIGS For Calibration']

    # bail out if the path is incorrect, or any of the sheet names does not exist
    valid_sheet = True        
    if not path.exists(path_to_cal_db):
        print (f'Unable to access calibration database:{path_to_cal_db}\nExiting test')
        valid_sheet = False
    
    else:    
        cal_db = BaseExcelWrapper(path_to_cal_db, make_visible)
        cal_db.list_sheets()
        
        for sheet_name in sheet_names:
            if debug:
                input (f'DEBUG: sheet_name {sheet_name}')
            if sheet_name not in cal_db.worksheet_dict:
                valid_sheet = False
                
    if not valid_sheet:
        print ('ERROR: spreadsheet or one or more sheet names are invalid')
        cal_tools_exit()
    
    if debug:
        for key in cal_db.worksheet_dict:
             print(key, '->', cal_db.worksheet_dict[key])
        
        
    # the first line in the list is a header.
    list_header = True
    
    for ix, jig in enumerate(jig_list):
        if not list_header:
            jig_description = jig[0]
            jig_id          = jig[1]

            is_id = False               # determines which columns to search
            id_found = False            # this ID has not been found in the cal database yet...

            for pre_id in ['FA', 'OPS', 'APPS']:
                if pre_id in jig_id:
                    is_id = True
          
            if jig_id == 'N/A':
                jig_list[ix].append('N/A')
                jig_list[ix].append('N/A')

            else:
                for sheet_name in sheet_names:   
                    if debug:
                        print ('DEBUG: iterating through sheet_names')
                        print(sheet_name, '->', cal_db.worksheet_dict[sheet_name])
                        input ('DEBUG: Hit enter')

                    cal_db.worksheet = cal_db.workbook.Worksheets(sheet_name)
                    cal_db.worksheet.Activate()    

                    cal_data = cal_db.worksheet.UsedRange

                    if debug:
                        input ('DEBUG: Hit enter')

                    content = str(cal_db.worksheet.Cells(1, 1))

                    # new or old format of Cal database
                    if "S/N" in content:
                        separate_sn = False
                        if debug:
                            print ('DEBUG: ID and serial are in the same column')
                    else:    
                        if debug:
                            print ('DEBUG: ID and serial are in NOT the same column')   

                    # now figure out the offsets we need to use
                    id_os      = 1                      # col A/1
                    sn_os      = id_os 

                    if separate_sn:
                        sn_os  += 1  

                    desc_os     = sn_os + 3             # col D/4
                    lasttest_os = desc_os + 1           # col E/5
                    nexttest_os = lasttest_os + 3       # col H/8

                    #equip_match = True
                    for cell in range(2, cal_data.Rows.Count+1):
                        # if the cal database has separate columns for ID and serial number
                        # and a serial number has been provided check the serial number only column
                        # otherwise just check A1 as it is either ID or S/N(ID)
                        if not is_id and separate_sn:
                            content = (str(cal_db.worksheet.Cells(cell, sn_os))).upper()        # start at A2
                        else:
                            content = (str(cal_db.worksheet.Cells(cell, id_os))).upper()        # start at A1


                        db_name = str(cal_db.worksheet.Cells(cell, desc_os)).upper()
                        cmp_eq_name = db_name.replace('\t', '')
                        cmp_eq_name = cmp_eq_name.replace(' ', '')

                        lst_jig_name = jig_description.replace('\t', '')
                        lst_jig_name = lst_jig_name.replace(' ', '')

                        if debug:
                            print (f'DEBUG: cal db ID: {content} jig_id {jig_id}')

                        if jig_id in (content):
                            id_found = True
                            if debug:
                                print(f'DEBUG: Found {jig_id} in calibration database')

                            if lst_jig_name != cmp_eq_name:
                                print (f'ERROR: Jig Name/Description Mismatch ({jig_description} vs {db_name}) between equipment list and calibration database for {jig_id}')
                                id_found = False                            

                            for pos in range(max_cols_of_interest):
                                if pos in [lasttest_os,nexttest_os]:
                                    if debug:
                                        date_string = str(cal_db.worksheet.Cells(cell, pos))
                                        print (f'DEBUG: date_string: {date_string}')
                                    date,_ = str(cal_db.worksheet.Cells(cell, pos)).split()

                                    # this sheet is dd-mm-yyyy
                                    if not id_found:
                                        date = '01-01-2000'            

                                    jig_list[ix].append(date)                            

                        # no need to continue iterating if we've got a match
                        # so break out of sheet loop
                        if id_found:
                            break

                    #break out of sheet loop and move on to next ID
                    if id_found:
                        break

        list_header = False       # ignore the first line in the list
    
    cal_db.close_excel() 
    
    return jig_list

def check_jigs_in_date(jig_list):
    '''
    Takes the populated equipment list and checks the calibration due date
    
    if any instrument is out of cal returns 'False', otherwise returns 'True'
    
    '''
    
    # assume everything is in test, until we learn otherwise
    return_value = True
    
    # need todays date to check if calibration is valid
    now = time()
    
    # the first line in the list is a header.
    list_header = True
    
    for jig in jig_list:
        if not list_header:
            jig_name      = jig[0]
            jig_id        = jig[1]
            
            #print (jig)
            #input (f'DEBUG: jig_id {jig_id}')
            
            if jig_id != 'N/A':
                jig_next_test = jig[3]

                jig_testdate =  mktime(strptime(jig_next_test, "%Y-%m-%d"))   

                if (jig_testdate < now):
                    return_value = False
                    print(f'ERROR: Jig {jig_name}:{jig_id} is overdue test. Test was due: {jig_next_test}\n')
                
        list_header = False

    return return_value

if __name__ == '__main__':

        #TEST_PATH_TO_CAL_DB = '\\\\node1\\calibration\\DBS 0001 (Calibration Database).xlsx'
        TEST_PATH_TO_CAL_DB = 'C:\\ACRA\\Test\\COPQ_Libraries\\Python3_PEP8\\ADAU\\cal_database.xlsx'
        
        test_equipment_list = [ [ 'Function','Manufacturer','Model','ID'],
                                [ 'Power Supply','TTI AIM','PL330QMD','FA3872'],
                                [ 'Digital Multi Meter','Keithley','2700', '4040285']
                              ]

        test_jig_list       = [ ['Description', 'ID'],
                                ['ASD/MTS/002/00', 'OPS0299'],
                                ['JIG/MBM/102/00', 'AAD0835'] 
                              ]

        
        
        #print(get_equ_cal_info(test_equipment_list, TEST_PATH_TO_CAL_DB))
        
        
        updated_jig_list = get_jig_cal_info(test_jig_list, TEST_PATH_TO_CAL_DB)
        
        print (updated_jig_list)
        
        RESP = check_jigs_in_date(updated_jig_list)
        
        if(RESP):
            print ('Jigs in date')
        else:    
            print ('Jigs NOT in date')            
