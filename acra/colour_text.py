'''
COLOUR.py
*************************************************************************************
Created by:         WCA  [11 SEPT 2014]
Last modified by:   WCA  [11 SEPT 2014]
Version:            1.0

Description:        Allows easy screen printout in colour
                    
Company:            Acra Control Ltd.
*************************************************************************************
Version History:
[1.0] Functions for setting/resetting colour or printing out one-liners in colour
      Copied from COLOUR.py and made PEP8 compliant
'''
from ctypes import windll

    
def set_colour(colour):
    '''
    sets colour
    
    0x01 = blue
    0x02 = green
    0x03 = turquoise
    0x04 = red
    0x05 = pink
    0x06 = yellow
    0x07 = highlight    
    
    '''
    std_output_handle = -11

    stdout_handle = windll.kernel32.GetStdHandle(std_output_handle)  # @UndefinedVariable
    set_console_text_attribute = windll.kernel32.SetConsoleTextAttribute  # @UndefinedVariable

    colour = colour|0x08
    set_console_text_attribute(stdout_handle, colour)

def set_dark_colour(colour):
    '''
    sets dark version of the colour
    '''
    std_output_handle = -11

    stdout_handle = windll.kernel32.GetStdHandle(std_output_handle)  # @UndefinedVariable
    set_console_text_attribute = windll.kernel32.SetConsoleTextAttribute  # @UndefinedVariable

    set_console_text_attribute(stdout_handle, colour)

def reset_colour():
    '''
    resets to standard console text colour
    '''
    std_output_handle = -11

    stdout_handle = windll.kernel32.GetStdHandle(std_output_handle)  # @UndefinedVariable
    set_console_text_attribute = windll.kernel32.SetConsoleTextAttribute  # @UndefinedVariable

    set_console_text_attribute(stdout_handle, 0x07)

def print_colour(text,colour):
    '''
    print out 1 line in colour
    resets to standard colour after
    '''
    set_colour(colour)
    print(text)
    reset_colour()

def print_dark_colour(text,colour):
    '''
    print out 1 line in dark colour
    resets to standard colour after
    '''
    set_dark_colour(colour)
    print(text)
    reset_colour()
    
if __name__ == '__main__':
    colour_list = [1,2,3,4,5,6,7]
    for test_colour in colour_list:
        set_colour(test_colour)
        print ('This is test text')
        reset_colour()
        print ('Colour reset')
        set_dark_colour(test_colour)
        print ('This is test text')
        reset_colour()
        print ('Colour reset')
        msg = f'This is colour : {test_colour}'
        print_colour(msg,test_colour)
        print ('This is test text')
        reset_colour()
        print ('Colour reset')
        msg = f'This is dark colour : {test_colour}'
        print_dark_colour(msg,test_colour)
        print ('This is test text')
        reset_colour()
        print ('Colour reset')
