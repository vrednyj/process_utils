'''

 Name:     reporting_tools.py

 Author:   Chris Gilbert

 Date:     June 21, 2022

 Copyright Curtiss Wright 2022

 Description

 Python 3 Library functions for generating reports in standard formats, 
 and for generating word and PDF versions of test reports

 Library/calls based on Mark Hammonds 'easyword.py' examples

 Added comments so help(modulename) returns useful information
 
 Useful references:
 
    https://docs.microsoft.com/en-us/office/vba/api/word.wdbuiltinstyle
 
 History:
   
   1.0  CJG 19/09/2019  Migrated to Python 3. Word and PDF sections not tested
   1.1  CJG 21/06/2022  Removed test functions. 
                        Word and PDF support not debugged/tested.
   1.2  CJG 16/09/2022  MS Word functionality moved to ms_office_tools
 
'''

from os     import path, remove         # rename

from time   import strftime, localtime
from sys    import exit as reporting_tools_exit

from acra.ms_word_tools import MSWordWrapper

#----------------------------------------------------------------------------------------------
# PDF Report related functions now
#----------------------------------------------------------------------------------------------
def seconds_to_dmy(seconds):
    '''
    Input is a time in seconds (since the start of the epoch)
    returns a string in DD-MM-YYYY format for display purposes.
    '''   
    return strftime("%d-%m-%Y", localtime(seconds))


def silent_delete(filename):
    '''
    deletes filename
    Returns:
        0: success
        1: file doesn't exist
        2: delete failed
    '''
    if path.isfile(filename):
        try:
            remove(filename)
            return 0
        except OSError:
            return 2
    else:
        return 1


def template_exist(templates):
    '''
    check the template files exist and bail out of they do not
    Missing template identified in error message:
    '''
    it_exists = True
    for template in templates:
        if not path.isfile(template):
            it_exists = False
            print(f'ERROR: File {template} not found')
    return  it_exists
    

def show_debug(debug, msg):
    '''
    If debugging is enabled print the message to the console
    '''
    if debug:
        input(msg)      
    
def read_txt_report(txt_report_name):
    '''
    Read in the input report (txt format)
    and return it as a list of lines.
    
    Each line in the report is returned as an element of the list.
    '''
    
    with open(txt_report_name, 'r', encoding='utf-8') as ifile:
        report = ifile.readlines()
    return report
    
#----------------------------------------------------------------------------------------------
# Generate the PDF report now
#----------------------------------------------------------------------------------------------

def gen_pdf_report(test_class):
    '''
    As input takes the template path, template name and the name of the input text report
    The output PDF name is generated from the text report name, by changing the extension
    
    A word document is generated during this function, but it is deleted after 
    the PDF file is generated.
    
    Note, the full path to the template doc is required, otherwise Word looks in 'My Documents'
    
    This is a lot more convoluted than I would like, but it works.
    
    Step 1: generate all file names and delete all temporary files if they exist
    Step 2: open a blank word document and load the test results into it
    Step 3: Save this temporary file and close the doc.
    Step 4: Open a new blank document, using the correct template file
    Step 5: Import the temporary file which has the test results
    Step 6: Import the PTR front page template
    Step 7: Generate the PDF version of this combined file
    Step 8: Save the doc version and close the doc
    Step 9: Delete the temporary docx versions
    
    
    required variables in test_class:
    
    template_path     =  test_class.template_path 
    template_name     =  test_class.template_name 
    report_front_page =  test_class.report_front_p
    dummy_report      =  test_class.dummy_report 
    assembly_list     =  test_class.assembly_list 
    equipment_details =  test_class.equipment_details

    Examples of what the 'assembly' and 'equipment' lists should be are shown below
    
    assembly_list = [ ['Component', 'Name', 'Serial Number'],
                      ['Product', 'KAD/ADC/123/B/05', 'ABC1234'],
                      ['Motherboard', 'KDM/ADC/103/B/01', 'DEF5678'],
                      ['DaugherBoard', 'KDD/ADC/123/B/05', 'GHI9876']
                    ]

    equipment_list = [ ['Function','Manufacturer', 'Model', 'ID', 'Calibration Date'],
                       ['Power Supply', 'TTi',' PL330D', 'FA1234', '14 Jan 2015'],
                       ['Signal Generator', 'Stanford','DS360', 'FA4321', '15 Feb 2015'],
                       ['Voltmeter', 'Keithley','2700', 'FA5678', '16 Mar 2015'],
                       ['Calibrator', 'Martell','2700', 'FA2468', '17 Apr 2015']
                     ]

    Generation is a bit convoluted and 2 word docs ar generated, the second is imported into the first, 
    with the test front page document.
    
    Its done this way, because Word seems to get a little upset and formats the report body as th eheader of the front page.
    Its currently unknown why this is the case.
    
    uses 'doc' format for temporary storage to avoid zip/unzip cycles (from docx to doc and back)
    
    '''  
    
    # map class variables to local variables for ease of typing
    template_path     =  test_class.template_path 
    template_name     =  test_class.template_name 
    front_page        =  test_class.report_front_page
    txt_report_name   =  test_class.report_name
    #report_folder     =  test_class.reportfolder
    assembly_list     =  test_class.assembly_list 
    equipment_details =  test_class.printable_equip_list()
    test_tech         =  test_class.user_name
    
    # Turn on debug features
    debug = False

    # establish what the CWD is and generate the required filenames
    #cwd = getcwd()

    word_template_name = template_path + '\\' + template_name
    test_frontpage     = template_path + '\\' + front_page

    file_name, _ = path.splitext(txt_report_name)
    pdf_file_name           = file_name + '.pdf'
    #word_file_name          = file_name + '.docx'
    word_file_name          = file_name + '.doc'
    #temp_file_name          = file_name + '_temp' + '.docx'
    temp_file_name          = file_name + '_temp' + '.doc'

    #pdf_report_name    = cwd + '\\' + pdf_file_name
    #word_report_name   = cwd + '\\' + word_file_name
    #temp_report_name   = cwd + '\\' + temp_file_name

    # turn relative paths into absolute paths
    pdf_report_name    = path.abspath(pdf_file_name)
    word_report_name   = path.abspath(word_file_name)
    temp_report_name   = path.abspath(temp_file_name)
    
    # check the templates exist: embedded debug messages not helpful
    if not template_exist([word_template_name, test_frontpage]):
        reporting_tools_exit(1)
    # ensure that no word doc with this name exists
    silent_delete(temp_report_name)
    silent_delete(word_report_name)

    # read in the text report file before we start word.
    txt_report = read_txt_report(txt_report_name)
    
    # open the required template in word
    word_instance = MSWordWrapper(word_template_name)
    
    # Open word onscreen so we can see what is happening
    # for debug only, we don't want this running
    # under normal operation.
    if debug:
        word_instance.show()
     
    
    #Add title for the imported data text, using syles in the template.
    # as well as the equipment and and sub components.
    _, fname = path.split(pdf_file_name)
    doc_name_heading = f'Document Name:\t{fname}'
    word_instance.add_styled_para(doc_name_heading, 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    test_technician = f'Test Technician:\t{test_tech}'
    word_instance.add_styled_para(test_technician, 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    word_instance.add_styled_para("Product Details", 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    word_instance.add_table(assembly_list, 37)                      # magic number, there are 42 built in table styles to choose from...
    word_instance.add_styled_para("Test Equipment Details", 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
        
    word_instance.add_table(equipment_details, 37)                  # magic number, there are 42 built in table styles to choose from...
    
    # temperature and humidity information
    if test_class.record_temp:
        word_instance.add_styled_para("Temperature and Humidity", 'Heading 2')
        word_instance.add_styled_para("\n", 'Report Text')            # formatting
        word_instance.add_table(test_class.rh_table(), 37)           # magic number, there are 42 built in table styles to choose from...
        
        word_instance.add_styled_para("\n", 'Report Text')            # formatting
        word_instance.add_table(test_class.temp_table(), 37)         # magic number, there are 42 built in table styles to choose from...
    
    
    word_instance.add_styled_para("\n\n\n", 'Report Text')            # formatting    
    word_instance.add_styled_para("Test Results", 'Heading 2')

    # add in the report text
    for line in txt_report:
        word_instance.add_styled_para(line, 'Report Text')
        
    msg = f'DEBUG: Saving {temp_report_name}'
    show_debug(debug, msg)

    word_instance.save_as(temp_report_name)
    word_instance.close_doc()
    
    word_instance = MSWordWrapper(word_template_name)
    
    word_instance.word_doc.Application.Selection.Range.InsertFile(temp_report_name)
    
    word_instance.word_doc.Application.Selection.Range.InsertFile(test_frontpage)
            
    msg = 'DEBUG: Saving {pdf_report_name}'
    show_debug(debug, msg)
    
    word_instance.save_as(pdf_report_name) # comment out for debug

    msg = 'DEBUG: Saving {word_report_name}'
    show_debug(debug, msg)

    word_instance.save_as(word_report_name)
    word_instance.close_doc()   # comment out for debnug

    silent_delete(word_report_name)
    silent_delete(temp_report_name)   

    msg = f'DEBUG: Deleted as {word_report_name}'
    show_debug(debug, msg)



#----------------------------------------------------------------------------------------------
# Functions for writing reports now
#----------------------------------------------------------------------------------------------

def report_heading(heading):        # Tested and working
    '''
    Standard format for final report heading
    
    heading is a string
    
    Output heading is in the format shown

    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        REPORT HEADING: <heading>
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    
    
    '''
    msg  = '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n'
    msg += f'\tREPORT HEADING: {heading}\n'
    msg += '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n'
    return msg

def report_result(result):
    '''
    Standard format for final report result
    
    result is a string, PASS/FAIL

    Outputs the report result into a standard format
    ****************************************************************************
        REPORT RESULT: PASS
    ****************************************************************************
    
    '''
    result_list = ['PASS', 'FAIL']
    if result.upper() in result_list:    
        answer = result.upper()
    else:
        answer = 'FAIL'

    msg  = '****************************************************************************\n'
    msg += f'\tREPORT RESULT: {answer}\n'
    msg += '****************************************************************************\n'
    
    if answer == 'FAIL':
        msg += '\n\n\tRefer to Section 4.3 Failure Conditions of PTC/PVR for debug information\n\tshould any section of this test fail\n\n'
    
    return msg
    
def report_summary(summary_heading, summary):
    '''
    Standard format for final report heading

    summary is a preformatted string.
    
    Adds a standard header to the summary and outputs the data inthe form
    ////////////////////////////////////////////////////////////////////////////////////////////////
        Summary: <heading>
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    blah blah blah
    blah
    blah blah
    blah blah blah blah
    blah
    
    '''
    msg  = '////////////////////////////////////////////////////////////////////////////////////////////////\n'
    msg += f'\tSummary: {summary_heading}\n'
    msg += '////////////////////////////////////////////////////////////////////////////////////////////////\n\n'
    msg += summary
    
    return msg

def section_heading(some_section_heading):      # Tested and Working
    '''
    Standard format for section heading

    output is of the form
    ============================================================================
        Test Section: <heading>
    ============================================================================    
    
    '''
    msg  = '============================================================================\n'
    msg += f'\tTest Section: {some_section_heading}\n'
    msg += '============================================================================\n'
    return msg

def section_result(some_section_heading, result):   # Tested and Working
    '''
    Standard format for section result
    
    output is of the form

    ------------------------------------------------------------------------------------------------
        Test Section: <heading> Result: <result>
    ------------------------------------------------------------------------------------------------
    
    '''
    result_list = ['FAIL']
    
    msg  = '------------------------------------------------------------------------------------------------\n'
    msg += f'\tTest Section: {some_section_heading}\tResult: {result}\n'
    msg += '------------------------------------------------------------------------------------------------\n'
    
    
    if result.upper() in result_list:
        msg += '\n\n\tRefer to Section 4.3 Failure Conditions of PTC/PVR for debug information for any test fail\n\n'   
    
    return msg

def pass_fail(result):      # Tested and Working
    '''
    Takes an integer argument. 
    If the argument == 0 (no errors) the function returns the string 'PASS'
    otherwise it returns the string 'FAIL'

    '''
    retval = 'FAIL'
    if result == 0:
        retval = 'PASS'
    return retval        


def integer_range_result(comment, measured, int_min, int_max, units):       # tested and working
    '''
    All inputs expected to be integers.
    If the count range is within the range of min/max the result is a pass, otherwise it is a fail.
    Function returns a list with an error count value and a message string, of the form

    e.g.
    
    [count, 'PASS : Channel 02 : Mesured 32.768 Range: Min 12.345 Max 54.321 (Counts)']
    
    channel:    integer
    comment:    string - something meaningful please
    measured:   integer - the measured value in integer form
    int_min:        integer - the minimum acceptable value
    int_max:        integer - the maximum accpetable value
    units       string - the measurement units - volts, amps, ohms, etc
    
    '''
    
    if measured in range(int_min, int_max-1):
        msg = 'PASS'
        retval = 0
    else:        
        msg = 'FAIL'
        retval = 1
        
    msg += f' : {comment} : Measured {measured:5d} Range: Min {int_min:5d} Max {int_max:5d} ({units})\n'
    return [retval, msg]
    
def integer_not_range_result(comment: str, measured: int, min_input: int, max_input: int, units: str) -> list:
    """
    All inputs expected to be integers.
    If the count range is not within the range of min/max the result is a pass, otherwise it is a fail.
    Function returns a list with an error count value and a message string, of the form

    e.g.

    [count, 'PASS : Channel 02 : Mesured 32.768 Not in Range: Min 12.345 Max 54.321 (Counts)']

    channel:    integer
    comment:    string - something meaningful please
    measured:   integer - the measured value in integer form
    min:        integer - the minimum acceptable value
    max:        integer - the maximum acceptable value
    units       string - the measurement units - volts, amps, ohms, etc

    """

    if measured in range(min_input, max_input - 1):
        msg = 'FAIL'
        retval = 1
    else:
        msg = 'PASS'
        retval = 0
    msg += f" : {comment} : Measured {measured:05d} Not in Range: Min {min_input:05d} Max {max_input:05d} ({units})\n"
    return [retval, msg]   

def float_range_result(comment, measured, float_min, float_max, units):         # Tested and Working
    '''
    All inputs expected to be floats.
    If the measured is within the range of min/max the result is a pass, otherwise it is a fail.
    Function returns a list with an error count value and a message string, of the form
    [integer, string]
    
    e.g.
    
    [count, 'PASS : Channel 02 : Mesured 32768 Range: Min 12345 Max 54321 (Counts)']

    channel:    integer
    comment:    string - something meaningful please
    measured:   float - the measured value in float form
    float_min:        float - the minimum acceptable value
    float_max:        float - the maximum accpetable value
    units       string - the measurement units - volts, amps, ohms, etc

    '''
    
    if ((float_min <= measured <= float_max)):
        msg = 'PASS'
        retval = 0
    else:        
        msg = 'FAIL'
        retval = 1
        
    msg += f' : {comment} : Measured {measured:5.6f} Range: Min {float_min:5.6f} Max {float_max:5.6f} ({units})\n'
    return [retval, msg]
    


def greater_than_result_int(comment, measured, min_value, units):
    '''
    Takes the measured value and compares it to the minimum acceptable value (min_value)
    '''
       
    if measured >= min_value:
        msg = 'PASS'
        retval = 0
    else:        
        msg = 'FAIL'
        retval = 1
 
    msg += f' : {comment} : Measured {int(measured):4d} Minimum Expected Value {int(min_value):4d} ({units})\n'
    return [retval, msg]

def greater_than_result_float(comment, measured, min_value, units):
    '''
    Takes the measured value and compares it to the minimum acceptable value (min_value)
    
    Tested and Working
    
    '''
       
    if measured >= min_value:
        msg = 'PASS'
        retval = 0
    else:        
        msg = 'FAIL'
        retval = 1
 
    msg += f' : {comment} : Measured {measured:5.6f} Minimum Expected Value {min_value:5.6f} ({units})\n'
    return [retval, msg]

def less_than_result_int(comment, measured, max_value, units):          # Tested and Working
    '''
    Takes the measured value and compares it to the minimum acceptable value (min_value)
    '''
       
    if measured <= max_value:
        msg = 'PASS'
        retval = 0
    else:        
        msg = 'FAIL'
        retval = 1
 
    msg += f' : {comment} : Measured {int(measured):4d} Maximum Expected Value {int(max_value):4d} ({units})\n'
    return [retval, msg]

def less_than_result_float(comment, measured, max_value, units):        # Tested and Working
    '''
    Takes the measured value and compares it to the minimum acceptable value (min_value)
    
    '''
       
    if measured <= max_value:
        msg = 'PASS'
        retval = 0
    else:        
        msg = 'FAIL'
        retval = 1
 
    msg += f' : {comment} : Measured {measured:5.6f} Maximum Expected Value {max_value:5.6f} ({units})\n'
    return [retval, msg]

def channel_integer_range_result(channel, comment, measured, min_value, max_value, units):
    '''
    return channel integer range result
    '''
    result = integer_range_result(comment, measured, min_value, max_value, units)
    msg = f'Channel: {channel:2d}: {result[1]}'
    return (result[0], msg)
    
def channel_float_range_result(channel, comment, measured, min_value, max_value, units):
    '''
    return channel integer range result
    '''
    result = float_range_result(comment, measured, min_value, max_value, units)
    msg = f'Channel: {channel:2d}: {result[1]}'
    return (result[0], msg) 
    
def channel_greater_than_result_int(channel, comment, measured, min_value, units):
    '''
    return channel integer greater than result
    '''
    result = greater_than_result_int(comment, measured, min_value, units)
    msg = f'Channel: {channel:2d}: {result[1]}'
    return (result[0], msg)     

def channel_less_than_result_int(channel, comment, measured, max_value, units):
    '''
    return channel integer less than result
    '''
    result = less_than_result_int(comment, measured, max_value, units)
    msg = f'Channel: {channel:2d}: {result[1]}'
    return (result[0], msg)
    
def channel_greater_than_result_float(channel, comment, measured, min_value, units):
    '''
    return channel float greater than result
    '''
    result = greater_than_result_float(comment, measured, min_value, units)
    msg = f'Channel: {channel:2d}: {result[1]}'
    return (result[0], msg)     

def channel_less_than_result_float(channel, comment, measured, max_value, units):
    '''
    return channel float less than result
    '''
    result = less_than_result_float(comment, measured, max_value, units)
    msg = f'Channel: {channel:2d}: {result[1]}'
    return (result[0], msg)
    
def str_com_result(comment, measured, value):
    '''
    Takes the measured string and compares it to the supplied value
    
    '''
    if str(measured) == str(value):
        msg = 'PASS'
        retval = 0
    else:
        msg = 'FAIL'
        retval = 1
    #if len(str(measured)) > 4:
    msg += f' : {comment} : Recorded: {measured} Expected: {value}\n'
    #else:
    #    msg += f' : %s : Recorded: %s Expected: %s\n' %(comment, measured, value)

    return [retval, msg]
    

def get_utc_time():
    '''
    Return the PC time as a string in the form HH MM SS
    '''
    return_time = strftime("%H:%M:%S")
    return return_time


def get_utc_date():
    '''
    Return the PC date as a string in the form YYYY MM DD
    YYYY expected to be post 2000
    '''

    utc_date = strftime("20%y:%m:%d")
    return utc_date


# testing time....

def report_debug(test_class):
    '''
    cut down version of gen_pdf_report for debugging
    
    
    required variables in test_class:
    
    template_path     =  test_class.template_path 
    template_name     =  test_class.template_name 
    report_front_page =  test_class.report_front_p
    dummy_report      =  test_class.dummy_report 
    assembly_list     =  test_class.assembly_list 
    equipment_details =  test_class.equipment_details

    Examples of what the 'assembly' and 'equipment' lists should be are shown below
    
    assembly_list = [ ['Component', 'Name', 'Serial Number'],
                      ['Product', 'KAD/ADC/123/B/05', 'ABC1234'],
                      ['Motherboard', 'KDM/ADC/103/B/01', 'DEF5678'],
                      ['DaugherBoard', 'KDD/ADC/123/B/05', 'GHI9876']
                    ]

    equipment_list = [ ['Function','Manufacturer', 'Model', 'ID', 'Calibration Date'],
                       ['Power Supply', 'TTi',' PL330D', 'FA1234', '14 Jan 2015'],
                       ['Signal Generator', 'Stanford','DS360', 'FA4321', '15 Feb 2015'],
                       ['Voltmeter', 'Keithley','2700', 'FA5678', '16 Mar 2015'],
                       ['Calibrator', 'Martell','2700', 'FA2468', '17 Apr 2015']
                     ]

    
    Generation is a bit convoluted and 2 word docs ar generated, the second is imported into the first, 
    with the test front page document.
    
    Its done this way, because Word seems to get a little upset and formats the report body as th eheader of the front page.
    Its currently unknown why this is the case.
    
    uses 'doc' format for temporary storage to avoid zip/unzip cycles (from docx to doc and back)
    
    '''  
    
    # map class variables to local variables for ease of typing
    template_path     =  test_class.template_path 
    template_name     =  test_class.template_name 
    front_page        =  test_class.report_front_page
    #report_folder     =  test_class.reportfolder
    #assembly_list     =  test_class.assembly_list 
    #equipment_details =  test_class.printable_equip_list()
    test_tech         =  test_class.user_name

    txt_report_name   =  test_class.report_name
    
    # Turn on debug features
    debug = False

    word_template_name = template_path + '\\' + template_name
    test_frontpage     = template_path + '\\' + front_page

    file_name, _ = path.splitext(txt_report_name)
    pdf_file_name           = file_name + '.pdf'
    
    word_file_name          = file_name + '.doc'
    
    temp_file_name          = file_name + '_temp' + '.doc'

    # turn relative paths into absolute paths
    pdf_report_name    = path.abspath(pdf_file_name)
    word_report_name   = path.abspath(word_file_name)
    temp_report_name   = path.abspath(temp_file_name)
    
    # check the templates exist: embedded debug messages not helpful
    if not template_exist([word_template_name, test_frontpage]):
        reporting_tools_exit(1)
    # ensure that no word doc with this name exists
    silent_delete(temp_report_name)
    silent_delete(word_report_name)

    # read in the text report file before we start word.
    txt_report = read_txt_report(txt_report_name)
    
    # open the required template in word
    word_instance = MSWordWrapper(word_template_name)
        
    # Open word onscreen so we can see what is happening
    # for debug only, we don't want this running
    # under normal operation.
    if debug:
        word_instance.show()       
    
    # Add the title for the imported data text, using styles in the template.
    # as well as the equipment and sub components.
    #
    # this is the 'data' part of the report. 
    # This is generated as a temporary document before inserting it 
    # and the frontpage into the final word doc, which is then 
    # saved as PDF
    
    _, fname = path.split(pdf_file_name)
    
    doc_name_heading = f'Document Name:\t{fname}'

    #Add the report text to the document first,as Word inserts from the start of the document.
    # the report is generated in reverse order...
    
    word_instance.add_styled_para("\n", 'Report Text')
    
    word_instance.add_styled_para(doc_name_heading, 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    test_technician = f'Test Technician:\t{test_tech}'
    word_instance.add_styled_para(test_technician, 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    word_instance.add_styled_para("Product Details", 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    word_instance.add_styled_para("Test Equipment Details", 'Heading 2')
    word_instance.add_styled_para("\n", 'Report Text')               # formatting
    
    # temperature and humidity information
    if test_class.record_temp:
        word_instance.add_styled_para("Temperature and Humidity", 'Heading 2')
        word_instance.add_styled_para("\n", 'Report Text')            # formatting
        
        word_instance.add_styled_para("\n", 'Report Text')            # formatting
    
    
    word_instance.add_styled_para("\n\n\n", 'Report Text')            # formatting    
    word_instance.add_styled_para("Test Results", 'Heading 2')

    # add in the report text
    for line in txt_report:
        word_instance.add_styled_para(line, 'Report Text')
    
    if debug:
        msg = f'DEBUG: Saving {temp_report_name}'
        print (msg)
        print(show_debug(debug, msg))
        input('hit enter')

    word_instance.save_as(temp_report_name)
    word_instance.close_doc()
    
    if debug:
        msg = f'DEBUG: {temp_report_name} Saved and CLOSED'
        print (msg)
        input('hit enter')    
    
    # now put the two temporary docs together, the front page and the report body
    # before esaving in PDF form
    word_instance = MSWordWrapper(word_template_name)
    
    word_instance.word_doc.Application.Selection.Range.InsertFile(temp_report_name)
    word_instance.word_doc.Application.Selection.Range.InsertFile(test_frontpage)
            
    if debug:
        msg = f'DEBUG: Saving {pdf_report_name}'
        show_debug(debug, msg)
    
    word_instance.save_as(pdf_report_name) # comment out for debug
    word_instance.save_as(word_report_name)

    if debug:
        msg = f'DEBUG: Saving {word_report_name}'
        show_debug(debug, msg)

    word_instance.close_doc()   # comment out for debug

    silent_delete(word_report_name)
    silent_delete(temp_report_name)   

    if debug:
        msg = f'DEBUG: Deleted as {word_report_name}'
        show_debug(debug, msg)


if __name__ == '__main__':

    from os import getcwd           #system

    from acra.base_test_info import BaseTestInfo
    
    my_test_class = BaseTestInfo()
    
    my_test_class.template_path = getcwd()
    #my_test_class.template_name = 'PTR_Template.dotx'
    my_test_class.template_name = 'debug_template.dot'
    my_test_class.report_front_page = 'PTRFrontPage.doc'
    my_test_class.report_name = 'release_notes.txt'
    my_test_class.reportfolder = getcwd()
    my_test_class.assembly_list  = []
    my_test_class.printable_equip_list()
    my_test_class.user_name = 'ASD'

    #gen_pdf_report(my_test_class)
    report_debug(my_test_class)
