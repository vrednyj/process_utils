""" utils.py

01 MAY 2007, Milos Melicher, Acra Control Ltd.
09 MAY 2008, Robert Farrell, Acra Control Ltd. - Altered class to add function (getParamMS) to strip multiple samples for NxChannels
15 MAY 2008, Robert Farrell, Acra Control Ltd. - Further alterations for the DS3Parser to accept parameters passed from test scripts
14 OCT 2010, Robert Farrell, Acra Control Ltd. - Created a NWDS3Parser Class to compliment new logging methods from packets
03 OCT 2019, Chris Gilbert, Curtiss Wright.    - Migrated to Python 3. Needs to be debugged as some calls I'm not sure about.
08 JUN 2022, Vitaliy Baseckas, Curtiss Wright. - Code Refactoring to meet PEP requirements.
17 Jun 2022, Chris Gilbert, Curtiss Wright.    - names made PEP8 compliant. Functions that belong elsewhere moved there.

Copyright Curtiss-Wright Corporation 2022.

"""

from time import sleep

import re


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


class DS3Parser():
    """
    DS3Parser is a class that enables to parse tagged
    logfiles created with DS3 utility (DS3 -X <filename> -l <no of frames>).

    example:
    # this prints first and second word from each frame
    # assumes that the log file contain data in hex

    from acra.utils import DS3Parser
    p = DS3Parser('c:/mylogfile.log', radix='hex')
    if (p.inlock):
        for frame in p.data:
            print hex(frame[0]), hex(frame[1])
    else:
        print "Error: cannot log data"
    """

    rawdata = None
    filename = None

    def __init__(self, filename=None, radix='hex', num_of_channels=1,
                 num_of_frames=1024, num_sam_per_frame=1, num_of_mf_words=32,
                 first_chan_offset=2):
        if filename is not None:
            self.load(filename, radix)
            self.num_of_channels = num_of_channels
            self.num_of_frames = num_of_frames
            self.num_sam_per_frame = num_sam_per_frame
            self.num_of_mf_words = num_of_mf_words
            self.first_chan_offset = first_chan_offset

    def load(self, filename, radix='hex'):
        """
        Loads logfile specified by <filename>. It's get called from
        __init__() if filename is specified when object is instancied.
        """
        self.filename = filename
        try:
            with open(filename, 'r', encoding="utf8") as fhandle:
                self.rawdata = fhandle.read()
        except OSError as my_error:
            print(f"ERROR:{my_error}")
            return False

        fhandle.close()
        self.radix = radix.lower()
        return True

    def refresh(self):
        """
        Reloads data from logfile.
        """
        return self.load(self.filename)

    def is_in_lock(self):
        """
        Returns True if the module is in lock,and False if decoder
        is out of lock, or no correct <SYNC> statement found.
        This methond is also maped as object property <object>.inLock.
        """
        if self.rawdata:
            inlock = re.compile("<INLOCK>YES</INLOCK>", re.IGNORECASE)
            result = inlock.search(self.rawdata)
        else:
            return False

        if result:
            return True
        return False

    def get_data(self):
        """
        Returns data from <DATA> section as tuple of frames as lists.
        Returns False if no correct <DATA> section found.
        This methond is also maped as object property <object>.data.
        """
        if self.rawdata:
            data = re.compile(r"<DATA>\n([\w\s]{0,})\n</DATA>", re.IGNORECASE)
            result = data.search(self.rawdata)
        else:
            return False
        if not result or result.group(1).strip() == '':
            return False
        lines = result.group(1).split('\n')
        while '' in lines:
            lines.remove('')
        datalist = []
        for line in lines:
            datalist.append(tuple(line.split()))
        return tuple(datalist)

    def get_frame_count(self):
        """
        Returns number of minor frames logged.This method
        is also maped as object property <object>.frames.
        """
        if self.rawdata:
            frames = re.compile(r"<FRAMES>(\d+)</FRAMES>", re.IGNORECASE)
            result = frames.search(self.rawdata)
        else:
            return False
        if result:
            return int(result.group(1))
        return False

    def get_param_count(self):
        """
        Returns number of paramaters in the first frame.This method
        is also maped as object property <object>.params.
        """
        return len(self.data[0])

    def get_frame(self, ix_frame, data_format='str'):
        """
        Returns a frame with index <ix_frame> as tuple of strings or int
        """
        try:
            frame = self.data[ix_frame]
        except IndexError as my_error:
            print(f"ERROR: {my_error}")
            return None
        if data_format in ('int', 'i'):
            return tuple([int(word, 16) for word in frame])
        return tuple(frame)

    def get_word(self, ix_frame, ix_word, data_format='str'):
        """
        Returns a word as string or int on position <ix_word>
        in minor frame <ix_frame>
        """
        try:
            frame = self.data[ix_frame]
            word = frame[ix_word]
        except IndexError as my_error:
            print(f"ERROR: {my_error}")
            return None
        if data_format in ('int', 'i'):
            return int(word, 16)
        return word

    def get_param(self):
        """
        Returns a tuple of lists-parameters
        """
        parlist = [[] for p in self.data[0]]

        radix = 16
        if self.radix == 'dec':
            radix = 10

        for frame in self.data:
            for index, word in enumerate(frame):
                parlist[index].append(int(word, radix))
        return tuple(parlist)

    def get_param_ms(self):
        """
        Strips the samples for a specific channel in a frame
        for N*Frames and returns a tuple for each channel with
        samples in order for that channel.

        """
        parlist = [[] for p in self.data[0]]

        radix = 16
        if self.radix == 'dec':
            radix = 10

        # Create a list of lists of frame columns
        for frame in self.data:
            for index, word in enumerate(frame):
                parlist[index].append(int(word, radix))

        # Strip the consecutive channel samples from a multisample frame
        #  - if one sample per frame, return just that
        splice = [[] for p in range(self.num_of_channels)]

        for channel in range(self.num_of_channels):
            for frindex in range(self.num_of_frames):
                for frame_sampl in range(self.num_sam_per_frame):
                    splice[channel].append(parlist[channel +
                                                   self.first_chan_offset +
                                                   (frame_sampl*self.num_of_mf_words)][frindex])

        return tuple(splice)

    def get_min(self):
        """
        Returns a tuple of minimum values for each parameter.
        """
        minlist = []
        for par in self.param:
            minlist.append(min(par))
        return tuple(minlist)

    def get_max(self):
        """
        Returns a tuple of maximum values for each parameter.
        """
        maxlist = []
        for par in self.param:
            maxlist.append(max(par))
        return tuple(maxlist)

    def get_avg(self):
        """
        Returns a tuple of average values for each parameter.
        """
        avglist = []
        for par in self.param:
            avglist.append(sum(par)/len(par))
        return tuple(avglist)

    def get_time(self, ix_hi, ix_lo, ix_mi):
        """
        Returns a tuple of time in usec calculated from time tags
        """
        timelist = []
        for frame in self.data:
            time = self._masktag(frame[ix_hi], 7, 12) * 3600e6
            time += self._masktag(frame[ix_hi], 0, 6) * 60e6
            time += self._masktag(frame[ix_lo], 8, 14) * 1e6
            time += self._masktag(frame[ix_lo], 0, 7) * 1e4
            time += int(frame[ix_mi])
            timelist.append(int(time))
        return tuple(timelist)

    def _masktag(self, tag, startbit, stopbit):
        mask = 0
        for bit in range(startbit, stopbit + 1):
            mask += pow(2, bit)
            maskedtag = hex((int(tag, 16) & mask) >> startbit)[2:]
        return int(maskedtag)

    inlock = property(is_in_lock)
    frames = property(get_frame_count)
    params = property(get_param_count)
    data = property(get_data)
    param = property(get_param)
    param_ms = property(get_param_ms)
    minpar = property(get_min)
    maxpar = property(get_max)
    avgpar = property(get_avg)

# ### WIP ###


class NWInspectParser():
    """
    Class that enables parses logfiles created with the Network Inspect Function
    (REF_NWDS3.exe -f <NWLogFile> -X <UDP> -I <Num_Packets> -i <Adapter>).

    Current NW inspect log file is structured like this:-

    Packet <Packet Number> Sample <Sample_in_Packet_Number> :
       Sample<N>_CH0   Sample<N>_CH1   Sample<N>_CH2 ...... Sample<N>_CHX

    Note: This is just for one module only at the moment: it will change
    drastically when intermixing packets from different analogue modules
    """
    rawdata = None
    filename = None

    def __init__(self, filename=None, radix='hex', num_of_channels=1,
                 num_of_packets=1, num_sam_per_packet=1):
        if filename is not None:
            self.load(filename, radix)
            self.num_of_channels = num_of_channels
            self.num_of_packets = num_of_packets
            self.num_sam_per_pack = num_sam_per_packet
            self.keywords = ["Sequence No. Errors",
                             "Sequence Inc Errors",
                             "Payload Info Error bit",
                             "Payload Timeout bit"]

    def load(self, filename, radix='hex'):
        """
        Loads logfile specified by <filename>. It's get called from
        __init__() if filename is specified when object is instancied.
        """
        self.filename = filename
        try:
            with open(filename, 'r', encoding="utf8") as rawdata:
                self.rawdata = rawdata.readlines()
        except OSError as my_error:
            print(f"ERROR: {my_error}")
            return False
        self.radix = radix.lower()
        return True

    def refresh(self):
        """
        Reloads data from logfile.
        """
        return self.load(self.filename)

    # CheckSeqError function body
    def check_footer_error(self):
        """
        Check the nsds3 log file for sequence number errors
        This function returns an error value for the following:

        0 - No Error
        1 - Sequence No. Errors     (IENA and XNET)
        1 - Sequence Inc Errors     (IENA and XNET)
        1 - Payload Info Error bit  (XNET)
        1 - Payload Timeout bit     (XNET) <not implemented yet>

        So far, footer checking does both IENA and XNET but doesn't check Timestamp for both
        IENA footers should contain two less lines than XNET footers
        """
        self.refresh()
        result = 0
        seq_num_errors = 0
        sequence_inc_errors = 0
        payload_info_error_bit = 0
        # footer = self.rawdata.readlines()
        footer = self.rawdata
        footer_offset = self.num_of_packets * self.num_sam_per_pack
        footer_length = 21

        # scan footer for sequence number errors line
        for ref in range(footer_length):
            seq_num_errors = (footer[footer_offset + ref]
                              ).find(self.keywords[0])
            sequence_inc_errors = (
                footer[footer_offset + ref]).find(self.keywords[1])
            payload_info_error_bit = (
                footer[footer_offset + ref]).find(self.keywords[2])

            if seq_num_errors != -1:
                seq_num_type = footer[footer_offset + ref]

            if sequence_inc_errors != -1:
                seq_inc_type = footer[footer_offset + ref]

            if payload_info_error_bit != -1:
                payload_info_error_type = footer[footer_offset + ref]

        seq_num_error_num = str(seq_num_type).split(':')
        seq_inc_error_num = str(seq_inc_type).split(':')
        payload_info_error_num = str(payload_info_error_type).split(':')

        # Check that no errors were reported - return 0 if none are detected
        if int(seq_num_error_num[1]) != 0:
            print(f"\n\n** Sequence Number Type Error Detected \
                (Number of Errors = {int(seq_num_error_num[1])}) **\n\n")
            sleep(3)
            result = 1

        if int(seq_inc_error_num[1]) != 0:
            print(f"\n\n** Sequence Increment Type Error Detected \
             (Number of Errors = {int(seq_inc_error_num[1])}) **\n\n")
            sleep(3)
            result = 1

        if int(payload_info_error_num[1]) != 0:
            print(f"\n\n** Payload Info Error Detected \
                (Number of Errors = {int(payload_info_error_num[1])}) **\n\n")
            sleep(3)
            result = 1

        return result

    # getPKTSamples function body
    def get_pkt_samples(self):
        """
        Processes the nwds3 log file and produces arrays of samples
        based on packet number, samples per packet, and number of channels
        """
        self.refresh()
        # Process the file lines into interger samples
        # Array Format after this process is
        # [[Samp0_CH0,Samp0_CH1,....Samp0_CHN],[Samp1_CH0,Samp1_CH1,....Samp1_CHN]....]
        conv = []
        samples = []
        temp = []
        temp2 = []

        # VVB: 08/06/2022 this below to be removed as does nothing.
        # Doesn't do anything at the moment - this is a leftover from the DS3 parser
        # radix = 16
        # if self.radix == 'dec':
        #    radix = 10

        for index in range(self.num_of_packets * self.num_sam_per_pack):
            # var_1 = self.rawdata.readline()
            var_1 = self.rawdata[index]
            var_2 = (var_1.split(':')[1]).strip()
            var_3 = var_2.split('\t')

            # Convert to integer
            for ref in range(self.num_of_channels):
                temp.append(int(var_3[ref]))

            conv.append(temp)
            temp = []

        # Reorganise into [[Samp0_CH0,Samp1_CH0,....SampN_CH0],
        # [Samp0_CH1,Samp1_CH1,....SampN_CH1]....]
        for num_chan in range(self.num_of_channels):
            for num_pack in range(self.num_of_packets * self.num_sam_per_pack):
                temp2.append(conv[num_pack][num_chan])
            samples.append(temp2)
            temp2 = []

        return samples

    #check_footer_error = property(CheckFooterError)
    #extract_pkt_samples = property(getPKTSamples)

# ~~~~~~~~~~~~~~~~~~~~~~#
# ### WIP ####
# ~~~~~~~~~~~~~~~~~~~~~~#


class TempRefExtracter():
    """
    DS3Parser is a class that enables to parse tagged
    logfiles created with DS3 utility (DS3 -X <filename> -l <no of frames>).
    Works similar to the DS3 channel parser above except extracts
    thermocouple or DTM channel data
    """

    rawdata = None
    filename = None

    def __init__(self, filename=None, radix='hex',
                 use_tdc_module_tc_chan='NO',
                 use_tdc_module_cj_chan='NO',
                 use_on_board_dtm_chan='NO',
                 tc_chan_offset=3,
                 cj_chan_offset=4,
                 dtm_chan_offset=5,
                 num_of_tc_channels=1,
                 sfid_offset=2,
                 minor_frames_per_major_frame=1,
                 tdc_samples_per_major_frame=1):

        if filename is not None:
            self.load(filename, radix)
            self.use_tdc_module_tc_chan = use_tdc_module_tc_chan
            self.use_tdcmodule_cj_chan = use_tdc_module_cj_chan
            self.use_on_board_dtm_chan = use_on_board_dtm_chan
            self.tc_chan_offset = tc_chan_offset
            self.cj_chan_offset = cj_chan_offset
            self.dtm_chan_offset = dtm_chan_offset
            self.num_of_tc_channels = num_of_tc_channels
            self.sfid_offset = sfid_offset
            self.minor_frames_er_major_frame = minor_frames_per_major_frame
            self.tdc_samples_per_major_frame = tdc_samples_per_major_frame

    def load(self, filename, radix='hex'):
        """
        Loads logfile specified by <filename>. It's get called from
        __init__() if filename is specified when object is instancied.
        """
        self.filename = filename
        try:
            with open(filename, 'r', encoding="utf8") as fhandle:
                self.rawdata = fhandle.read()
        except OSError as my_error:
            print(f"ERROR:{my_error}")
            return False
        self.radix = radix.lower()
        return True

    def refresh(self):
        """
        Reloads data from logfile.
        """
        return self.load(self.filename)

    def get_data(self):
        """
        Returns data from <DATA> section as tuple of frames as lists.
        Returns False if no correct <DATA> section found.
        This methond is also maped as object property <object>.data.
        """
        if self.rawdata:
            data = re.compile(r"<DATA>\n([\w\s]{0,})\n</DATA>", re.IGNORECASE)
            result = data.search(self.rawdata)
        else:
            return False
        if not result or result.group(1).strip() == '':
            return False
        lines = result.group(1).split('\n')
        while '' in lines:
            lines.remove('')
        datalist = []
        for line in lines:
            datalist.append(tuple(line.split()))
        return tuple(datalist)

    def get_temperatures(self):
        """
        Strips the samples for a specific channel in a frame
        for N*Frames and returns a tuple for each channel with
        samples in order for that channel.

        """
        parlist = [[] for p in self.data[0]]
        sfid_array = []
        tc_chan_array = []
        cj_chan_array = []
        dtm_chan_array = []

        radix = 16
        if self.radix == 'dec':
            radix = 10

        # Create a list of lists of frame columns
        for frame in self.data:
            for index, word in enumerate(frame):
                parlist[index].append(int(word, radix))

        sfid_array = parlist[self.sfid_offset]
        print('\nSFID\n')
        print(sfid_array)

        frame_index = self.minor_frames_er_major_frame / self.tdc_samples_per_major_frame

        print('\nFrame_Index\n')
        print(frame_index)

        # Create a list of thermocouple channel averages for N TC channels
        if self.use_tdc_module_tc_chan == 'YES':
            tc_chan_array = parlist[self.tc_chan_offset]
            print('\nTC Chan Offset\n')
            print(tc_chan_array)

        # Create cold junction channel average
        if self.use_tdc_module_tc_chan == 'YES':
            cj_chan_array = parlist[self.cj_chan_offset]
            print('\nCJ Chan Offset\n')
            print(cj_chan_array)

        # Create DTM channel average
        if self.use_on_board_dtm_chan == 'YES':
            dtm_chan_array = parlist[self.dtm_chan_offset]
            print('\nDTM Chan Offset\n')
            print(dtm_chan_array)

# VVB: 08/06/2022 Not sure if the lines below required.
# These were commented out.
#       # Extract Thermocouple channel information
#        # Return array of N * TC channel average, cold junction,
#        #  and DTM (up to calling program to figure out)
#        # Strip the consecutive channel samples from a multisample
#        frame - if one sample per frame, return just that
#        splice=[[] for p in range(self.num_of_channels)]
#        for ch in range(self.num_of_channels):
#            for frindex in range(self.NumOfFrames):
#                for p in range(self.NumSamPerFrame):
#                    splice[ch].append(parlist[ch+self.FirstChanOffset +
#                                       (p*self.NumOfMFWords)][frindex])

#        return tuple(splice)

    data = property(get_data)
    extract_temp = property(get_temperatures)

# ~~~~~~~~~~~~~~~~~~~~~~~~~#


class UDPParser(DS3Parser):
    """
    UDPParser parses log files genereted by decom utility
    """

    def __init__(self, filename=None):
        DS3Parser.__init__(self, filename)

    def get_packet_count(self):
        """
        Returns number of packets logged. This method
        is also maped as object property <object>.packets.
        """
        if self.rawdata:
            frames = re.compile(r"<PACKETS>(\d+)</PACKETS>", re.IGNORECASE)
            result = frames.search(self.rawdata)
        else:
            return False
        if result:
            return int(result.group(1))
        return False

    def get_error_count(self):
        """
        Returns number of errors occurred. This method
        is also maped as object property <object>.errors.
        """
        if self.rawdata:
            frames = re.compile(r"<ERRORS>(\d+)</ERRORS>", re.IGNORECASE)
            result = frames.search(self.rawdata)
        else:
            return False
        if result:
            return int(result.group(1))
        return False

    def get_timestamp(self, ix_top=2, ix_mid=3, ix_low=4):
        """
        Returns a tuple of time in usec calculated from time stamp
        """
        timelist = []
        for frame in self.data:
            time = int(frame[ix_top], 16) * 65536 * 65536
            time += int(frame[ix_mid], 16) * 65536
            time += int(frame[ix_low], 16)
            timelist.append(time)
        return tuple(timelist)

    packets = property(get_packet_count)
    errors = property(get_error_count)
    timestamp = property(get_timestamp)

# ~~~~~~~~~~~~~~~~~~~~#


class XIDParser:
    """
    XIDParser is a simple class that enables to parse
    XID file and replace sections or keywords with user content.
    """
    error = []
    sections = ['HARDWARE IDENTIFICATION',
                'HARDWARE CONFIGURATION',
                'HARDWARE INFORMATION',
                'WIRING',
                'PROTOCOLS',
                'INSTRUMENT SETUP',
                'DISCRETE PARAMETERS',
                'CONVERSION DIRECTIVES',
                'MONITORING DIRECTIVES']

    def __init__(self, filename):

        try:
            with open(filename, 'r', encoding="utf8") as file_to_read:
                self.file = file_to_read.read()
        except OSError as my_error:
            self.error.append(f"ERROR:{my_error} Cannot open file {filename}")
            return None

    def is_section(self, section):
        """
        Returns True if passed argument is valid XID section,
        otherwise returns False.
        """
        if section in self.sections:
            return True
        return False

    def get_section_list(self):
        """
        Returns a tuple of XID sections in parsed XID file.
        """
        output = []
        for section in self.sections:
            if self.file.find(section) != -1:
                output.append(section)
        return tuple(output)

    def get_keyword_list(self):
        """
        Returns a tuple of keywords in parsed XID file.
        """
        pattern = "<(.*?)>"
        fileparts = re.split(pattern, self.file)
        multi_list = [fileparts[ix] for ix in range(1, len(fileparts), 2)]
        for keyword in multi_list:
            if multi_list.count(keyword) > 1:
                multi_list.remove(keyword)
        return tuple(multi_list)

    def get_section(self, section):
        """
        Returns a content of XID section as tuple.
        """
        if not self.is_section(section):
            self.error.append(f"'{section}' is not valid section.")
            return -1
        output = []
        fileparts = re.split(section, self.file)
        for filepart in fileparts[1:]:
            pattern = '|'.join(self.sections)
            pattern = pattern + '|$'
            pattern = f"^(.*?){pattern}"
            result = re.search(pattern, filepart, re.DOTALL)
            if result:
                output.append(result.group(1).strip())
        return tuple(output)

    def get_file(self):
        """
        Returns parsed XID file as string.
        """
        return self.file

    def replace_keyword(self, keyword, value):
        """
        Replaces all occurrences of '<keyword>' with 'value'.
        Return number of occurrences of <keyword> within parsed XID file.
        """
        keyword = f"<{keyword}>"
        value = f"{value} \t|~~~~~ value generated by script ~~~~~|"
        occurrence = self.file.count(keyword)
        if occurrence != 0:
            self.file = self.file.replace(keyword, value)
        return occurrence

    def replace_section(self, section, value):
        """
        Replaces all occurrences of 'section' with 'value'.
        Return number of occurrences of 'section' within parsed XID file.
        """
        if not self.is_section(section):
            self.error.append(f"'{section}' is not valid section.")
            return -1
        secvalues = self.get_section(section)
        output = self.file.replace(section, '')
        occurrence = 0
        for secvalue in secvalues:
            occurrence = occurrence + 1
            output = output.replace(secvalue, '')
        output = f"{output}\n\n|~~~~~ section generated by script ~~~~~|"
        output = f"{output}\n{section}\n{value}"
        output = self.clear_end_of_line(output)
        self.file = output
        return occurrence

    def clear_end_of_line(self, file):
        """
        Returns passed XID file with mutliple empty lines removed.
        """
        while file.find('\n\n\n') != -1:
            file = file.replace('\n\n\n', '\n\n')
        return file

# The lines below comented out by VVB on 08/06/2022. We have similar
# function in portchecker.py. The function below did not work in
# Python 3, so I assume nobody used that in Python 3 PT.
# The lines to be removed after the code review.
#
# def scan_available_serial_ports():
#     '''scan for available ports. return a list of tuples (num, name)'''
#     available = []
#     for int_count in range(256):
#         try:
#             s_port = serial(int_count)
#             available.append((int_count, s_port.portstr))
#             s_port.close()   # explicit close 'cause of delayed GC in java
#         except serial.Serialexeption:
#             pass
#     return available


# def scan_serial_ports():
#     '''print out the list of ports found to the screen'''
#     print("Found ports:")
#     for port_number, port_name in scan_available_serial_ports():
#         print(f"({port_number:d}) {port_name}")


#  ~~~~~~~~~~~~~~~[END-OF-SOURCE]~~~~~~~~~~~~~~~~~#
