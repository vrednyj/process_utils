'''


 Name:     debugging_tools.py

 Author:   Chris Gilbert

 Date:     June 21, 2022

 Copyright Curtiss Wright 2022

 Description

 Tools and utilities for debugging test libraries.
 
 This is utilities and functions separated out from reporting_tools/PTR2word 
 as was to keep the modules that should be used clean.
 
'''

from random import randint, choice

from acra.reporting_tools import integer_range_result, float_range_result, channel_greater_than_result_int, channel_greater_than_result_float

def random_text():
    '''
    Test function to generate random txt for generated document
    '''
    #this may or may not be appropriate in your company
    random_words = ['strategic','direction','proactive',
    'reengineering','forecast','resources',
    'forward-thinking','profit','growth','doubletalk',
    'venture capital','IPO']

    output = ""
    for _ in range(randint(1,5)):
        output = output + 'Blah'
        for __ in range(randint(10,25)):
            if randint(0,4)==0:
                word = choice(random_words)
            else:
                word = 'blah'
            output = output + ' ' + word
        output = output+'.'
    return output
            
def random_data():
    '''
    Test Function: generate random data for generated document
    '''
    months = ['Jan','Feb', 'Mar', 'Apr']
    data = []
    data.append([''] + months)
    for category in ['Widgets','Consulting','Royalties']:
        row = [category]
        for _ in range(4):
            row.append(randint(10000,30000) * 0.01)
        data.append(row)
    return data

def dummy_count_results(how_many, dummy_min, dummy_max):
    '''
    Test Function: Generates dummy integer results using random numbers
    '''
    msg = ''
    fail_cnt = 0
    for _ in range(how_many):
    #     def integer_range_result(comment, measured, int_min, int_max, units)
        res = integer_range_result('Something meaningful', randint(0,65535), dummy_min, dummy_max, 'counts')
        msg += res[1]
     
        fail_cnt += res[0]
        
    return [fail_cnt, msg]

def dummy_float_results(how_many, dummy_min, dummy_max):
    '''
    Test Function: Generates example float results, using random numbers.
    '''
    msg = ''
    fail_cnt = 0
    for _ in range(how_many):
        res = float_range_result('Something meaningful',float(randint(0,65535))/10000, dummy_min, dummy_max, 'volts')
        msg += res[1]
     
        fail_cnt += res[0]
        
    return [fail_cnt, msg]

def dummy_int_greater(how_many, dummy_min):
    '''
    Test Function: Generates integer greater than results, using random numbers
    '''
    msg = ''
    fail_cnt = 0
    for channel in range(how_many):
                                              # channel, comment, measured, min_value, units
        res = channel_greater_than_result_int(channel, 'Something meaningful',randint(0,65535), dummy_min, 'integer')
        msg += res[1]
     
        fail_cnt += res[0]

def dummy_float_greater(how_many, dummy_min):
    '''
    Test Function: Generates integer greater than results, using random numbers
    '''
    msg = ''
    fail_cnt = 0
    for channel in range(how_many):
        res = channel_greater_than_result_float(channel, 'Something meaningful',float(randint(0,65535)), dummy_min, 'float')
        msg += res[1]
     
        fail_cnt += res[0]

def dummy_int_less(how_many, dummy_max):
    '''
    Test Function: Generates integer greater than results, using random numbers
    '''
    msg = ''
    fail_cnt = 0
    for channel in range(how_many):
        res = channel_greater_than_result_int(channel, 'Something meaningful',randint(0,65535), dummy_max, 'integer')
        msg += res[1]
     
        fail_cnt += res[0]

def dummy_float_less(how_many, dummy_max):
    '''
    Test Function: Generates integer greater than results, using random numbers
    '''
    msg = ''
    fail_cnt = 0
    for channel in range(how_many):
        res = channel_greater_than_result_float(channel, 'Something meaningful',float(randint(0,65535)), dummy_max, 'float')
        msg += res[1]
     
        fail_cnt += res[0]


def dummy_ac_accuracy(no_channels):
    '''
    Generates a dummy array of ac accuracy data
    
    rows a re frequency points
    columns are channels

    '''
    
    frequency_list = [30, 103, 303, 503, 1003, 1253, 2003, 2503, 2603, 2703]
    db3 = 2005
    
    data = []
    
    first_line = ['Frequency']
    for ch in range(no_channels):
        first_line.append(f'Ch {ch}')
    
    data.append(first_line)
    
    for freq in frequency_list:
        freq_info = []
        freq_info.append(f'{freq}')
        for _ in range(no_channels):
            if freq < db3:
                freq_info.append(f'{float(randint(8000,9000)/100)}')
            else:    
                freq_info.append(f'{float(randint(1000,8000)/100)}')
            
        data.append(freq_info)
            
    return data
    
        