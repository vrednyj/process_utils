'''
=============================================================================

 Name:     pt_framework.py

 Author:   Chris Gilbert

 Date:     Feb 10, 2022

 Copyright Curtiss Wright 2022

 Description

 Functions shared by all levels of the test/utility library
 
 Revision History:
 
 1.0    10/02/2022 CJG  Library module created

'''
import re
from os import path, system, rename
from shutil import rmtree
from sys import stdout
from subprocess import Popen, PIPE, STDOUT
from time import sleep, strftime
import logging
from typing import Tuple

from acra.tar_tools import tar_folder
from acra.das_studio_tools import acraexec
from acra.py_cal_file import PyCalFile

#from acra.reporting_tools import report_heading, report_result, pass_fail, report_summary


def get_max_count_val(samples, chan):
    '''
    Imported from Process_Utils

    Function to Determine the maximum count value in a stream of sample contained in the tuple returned from the DS3 Class
    The values generated per channel are appended into a list and returned to the main body
    '''
    maxlist = []

    for ch in range(chan):
        maxp = samples[ch][0]
        for par in range(len(samples[ch])-1):
            if (samples[ch][par+1] > samples[ch][par]):
                if (maxp < samples[ch][par+1]):
                    maxp = samples[ch][par+1]
        maxlist.append(maxp)

    return (maxlist)


def get_min_count_val(samples, chan):
    '''
    Imported from Process_Utils

    Function to Determine the minimum count value in a stream of samples per channel and return them in a list
    '''
    minlist = []

    for ch in range(chan):
        minp = samples[ch][0]
        for par in range(len(samples[ch])-1):
            if (samples[ch][par+1] < samples[ch][par]):
                if (minp > samples[ch][par+1]):
                    minp = samples[ch][par+1]
        minlist.append(minp)

    return (minlist)


def get_avg_count_val(samples, chan):
    '''
    Imported from Process_Utils

    Function to Determine the average count value in a stream of samples per channel and return them in a list
    '''
    avg = 0
    avglist = []

    for ch in range(chan):
        for sample in range(len(samples[ch])):
            avg = avg + samples[ch][sample]

        totalavg = int(avg) / int(len(samples[ch]))
        avglist.append(totalavg)
        avg = 0

    return (avglist)


def not_in_range(value, limit1, limit2):
    '''
    Check a value is within the limits specified
    '''
    if (limit1 <= value <= limit2):
        return False
    return True

def human_round(float_number):
    '''
    Rounds as per humans - < 0.5 rounds down, other wise rounds up.
    '''

    if (float_number - int(float_number)) < 0.5:
        int_number = int(float_number)
    else:
        int_number = int(float_number+1)

    return int_number


def set_format_with_path(fmt, lpt_path):
    '''
    Imported from Process Utils

    More explicit format switching that combats the double command required by Windows XP machines
    This is a path dependent version to overcome the limitations of having to copy the Lptout.exe program into every test sub-folder
    Just have the lptout.exe in one place (usually the root directory) and reference it from there on from that location
    '''

    fmt_int = int(fmt)
    print(f'Switching to Format {fmt_int}')
    lpt_cmd = f'{lpt_path}lptout.exe {fmt_int}'
    system(lpt_cmd)
    sleep(1)
    system(lpt_cmd)
    sleep(1)


def auto_gen_frequency_list(start_freq, stop_freq, step):
    '''
    This Function auto generates the frequency list based on user_name defined range and steps

    made pep8 compliant... 

    while loop replaces if loop
    '''

    frequency_list = []

    while start_freq <= stop_freq:
        frequency_list.append(start_freq)
        start_freq += step

    return (frequency_list)


TIC_YEAR_INFO = {
    0: 'K',
    1: 'L',
    2: 'M',
    3: 'N',
    4: 'P',
    5: 'R',
    6: 'S',
    7: 'T',
    8: 'U',
    9: 'V',
    10: 'W',
    11: 'X',
    12: 'Y',
    13: 'Z',
    14: 'AA',
    15: 'AB',
    16: 'AC',
    17: 'AD',
    18: 'AE',
    19: 'AF',
    20: 'AG',
    21: 'AH',
    22: 'AI',
    23: 'AJ',
    24: 'AK',
    25: 'AL',
    26: 'AM',
    27: 'AN',
    28: 'AO',
    29: 'AP',
    30: 'AQ',
    31: 'AR',
    32: 'AS',
    33: 'AT',
    34: 'AU',
    35: 'AV',
    36: 'AW',
    37: 'AX',
    38: 'AY',
    39: 'AZ',
    40: 'BA'
}


def valid_file_type(some_filename, valid_ext='SPI'):
    '''
    Checks the file extension for correctness and returns
        True: Valid/expected file type/extension
        False: Invalid ISP file type/extension

    filename is just the filename, no path can be included.

    The expected/valid extension must be UPPERCASE

    Defaults to ISP, may be used for other file types by supplying the desired extension as the second argument.
    '''
    file_ok = False
    if path.exists(some_filename):
        extension = some_filename.rsplit('.', maxsplit=1)[-1]
        return bool(extension.upper() == valid_ext)

    return file_ok

def progress(count, total, suffix=''):
    '''
    Make the progress bar for dotsleep and other functions
    '''
    bar_len = total
    filled_len = int(round(float(total))-count)
    percents = round(100.0 * count / float(total), 1)
    hash_bar = '.' * filled_len + '-' * (bar_len-filled_len)
    stdout.write(f'[{hash_bar}] {percents}% ...{suffix}\r')
    # stdout.write(f'[{hash_bar}] {percents}{'%'} ...{suffix}\r')
    stdout.flush()


def dotsleep(dot, debug=False):
    '''
    Prints 1 dot/second to the screen whilst waiting for something to happen.
    Should also be a library function...
    '''
    off_time = dot
    print(f'\n\nWait Time {off_time} sec\n')
    if not debug:
        while dot > 0:
            sleep(1)
            dot = dot - 1
            progress(dot, off_time, 'Time left    ')


def cal_gain_coeff(channel_samples, channel_average, v_samples):
    '''
    Imported from Process_Utils
    Calculates the gain coefficients for N Channels. Input is an array of N channels averages (in counts)
    and the reference reading in counts (usually a DMM). Returns an array of Gain.coeff per channel

    gain_coefficient to be changed to list after initial debug

    '''

    sigma_x = 0
    sigma_y = 0
    sigma_xx = 0
    sigma_xy = 0
    for index, sample in enumerate(channel_samples):
        sigma_x += float(sample)
        sigma_y += float(channel_average[index])
        sigma_xx += (float(sample)*float(sample))
        sigma_xy += (float(sample)*float(channel_average[index]))

    slope = ((v_samples * sigma_xy) - (sigma_x * sigma_y)) / \
        ((v_samples * sigma_xx) - (sigma_x * sigma_x))
    if (slope == 0.0):
        print(
            '\nslope - Divide by Zero encountered - adjusting - Test accuracy compromised')
        slope = 0.000000001
    gain_coefficient = 1 / slope
    return (gain_coefficient)


def cal_offset_coeff(channel_samples, channel_average, gain_coefficient):
    '''
    Imported from Process_Utils

    Calculates the offset coefficients for N Channels. Input is an array of N channels averages (in counts)
    and the previously calculated gain coeffiecient (from the cal_gain_coeff function). Returns an array of Offset.coeff per channel

    '''
    mean_ch_average = 0
    mean_ch_samples = 0
    slope = 1 / gain_coefficient
    for index, sample in enumerate(channel_samples):
        mean_ch_samples += float(sample)
        mean_ch_average += float(channel_average[index])
    mean_ch_samples = mean_ch_samples / len(channel_samples)
    mean_ch_average = mean_ch_average / len(channel_samples)
    intercept = mean_ch_average - (slope * mean_ch_samples)
    offset_coeff = -1 * intercept
    return (offset_coeff)


def ver_cal_file(file, max_val=10):
    """
    This function to verify .ini calibration file
    to check if the values in the lines are not over the roof    
    """
    key_words = ['OffsetCalCoefficient', 'GainCalCoefficient']
    result = False
    faulty_line = 0
    with open(file, 'r', encoding='utf-8') as cal_file:
        my_lines = cal_file.readlines()
    for line in my_lines:
        for word in key_words:
            if word in line:
                _, val = line.split('=')
                if float(val) > max_val:
                    faulty_line += 1
    if faulty_line == 0:
        result = True
    return result


def convert_calibration_ini_to_cal(kup_dir: str, cal_convert_from: str, cal_convert_to: str) -> tuple:
    """
    Converts .ini format calibration file into .cal format file using
    KfileCal.exe utility.
    cal_convert_from = path + 'file_name.ini'
    cal_convert_to = path to store 'file_name.cal' file
    returns tuple ((int,int,int),str)
    """
    result = (1, 1, 1)
    output_file = ''
    converted_file = ''

    cmd = f"{path.join(kup_dir, 'KFileCal.exe')} -f {cal_convert_from} -d {cal_convert_to}"
    print(cmd)
    result = acraexec(cmd)
    converted_file = (path.splitext(
        path.basename(cal_convert_from))[0] + '.cal')
    output_file = path.join(cal_convert_to, converted_file)
    return result, output_file


def convert_calibration_cal_to_bin(cal_file_format: str, bin_file_format: str, text_mode: bool = False,
                                   byte_swap: bool = True) -> object:
    """
    Converts .cal format calibration file into .bin format calibration file.
    cal_file_format = path + 'calibration_file_name.cal'
    bin_file_format = path + 'calibration_file_name.cal'
    returns: object
    """
    run_cnvrt = PyCalFile(cal_file_format, bin_file_format, text_mode, byte_swap)
    return run_cnvrt


def get_float(fmsg):
    '''
    Get a float value from keyboard input (keyboard input)
    '''
    val = input(fmsg)
    while (1):
        try:
            myfloat = float(val)
            break
        except IOError:
            msg = 'ERROR: Invalid value entered: Enter correct value :'
            val = input(msg)
    return myfloat


def time_stamp_results_folder(test_folder_name):
    '''
    Timestamp the folder when backing it up
    '''

    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=logging.DEBUG)

    logging.debug(
        'common_functions/time_stamp_results_folder: test_folder_name {test_folder_name}')

    zip_name = ''

    if path.exists(test_folder_name):

        time_stamp = strftime('%Y_%m_%d_%H_%M_%S')
        timestamped_folder_name = f'{test_folder_name}_{time_stamp}'

        try:
            rename(test_folder_name, timestamped_folder_name)
        except IOError as exc:
            logging.warning(
                f'Unable to rename existing results folder {test_folder_name} to {timestamped_folder_name}\n{exc}')

        # compress the old results folder
        zip_name = f'{timestamped_folder_name}.tar.gz'
        tar_folder(timestamped_folder_name, zip_name)

        # delete the results folder that has just been zipped
        delete_folder_and_contents(timestamped_folder_name)

    return zip_name


def time_stamp_report(test_report_name):
    '''
    Timestamp the report when it is being backed up
    '''

    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=logging.DEBUG)

    logging.debug(
        f'common_functions/time_stamp_report: test_report_name {test_report_name}')

    timestamped_report_name = ''

    try:
        [report_name, extension] = test_report_name.split(".")
    except IOError as exc:
        logging.error(
            f'Test Report name contains illegal characters, must be <path>/<folder>/some_name.extension\n{exc}')

    if path.exists(test_report_name):

        time_stamp = strftime('%Y_%m_%d_%H_%M_%S')
        timestamped_report_name = f'{report_name}_{time_stamp}.{extension}'

        try:
            rename(test_report_name, timestamped_report_name)
        except IOError as exc:
            logging.warning(
                f'Unable to rename existing results folder {test_report_name} to {timestamped_report_name}\n{exc}')

    # else:
    #    input (f'DEBUG: test report not found : {test_report_name}')
    return timestamped_report_name


def delete_folder_and_contents(some_folder):
    '''
    deletes some_folder and its contents

    rmdir /S /Q ABC1234_2022_03_31_15_59_58

    '''
    if path.exists(some_folder):
        #cmd = f'rmdir /Q /S  "{some_folder}"'
        #system(cmd)
        
        rmtree(some_folder)   

def acra_syscmd(cmd, encoding='', my_shell=True)-> Tuple:
    '''
    Runs a command on the system, waits for the command to finish, and then
    returns the text output of the command along with return code.
    '''
    debug = False
    my_output = ''
    return_code = -1
    with Popen(cmd, shell=my_shell, stdin=PIPE, stdout=PIPE, stderr=STDOUT) as my_pipe:
        my_pipe.wait()

        output = my_pipe.stdout.read()
        if len(output) > 1:
            if encoding:
                my_output = output.decode(encoding)
            else:
                my_output = output
        if debug:
            print(f'Debug syscmd: command: {cmd}')
            print(f'Debug syscmd: output: {output}')
            print(f'Debug syscmd: output: {my_pipe.returncode}')

        return_code = my_pipe.returncode

    return return_code, my_output


def validate_ip_address(my_ip="192.168.28.1") -> bool:
    """
    This function to validate if IP is right format.
    returns: Bool
    """
    correct_ip_format = False
    pattern = r"^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\."\
        "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\."\
        "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\."\
        "([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$"
    # re.compile(pattern)
    if re.match(pattern, my_ip):
        correct_ip_format = True
    return correct_ip_format


def validate_mac_address(my_mac='00-0C-4D-12-34-57') -> bool:
    """
    This function to validate if MAC is right format.
    Regex supports dashes or colons as separators
    returns: Bool
    """
    correct_mac_format = False
    pattern = "^(?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2})$"
    if re.match(pattern, my_mac):
        correct_mac_format = True
    return correct_mac_format


def get_nic_interface(nic_name) -> tuple:
    """
    Function to return network interface name.
    Exmple: If KAM500 passed as argument
    the function will return something like
    \Device\ NPF_{52CE8828-EBC7-4D0E-AE73-C8AD67EFD3E2}
    Handy when used with windump.
    windump -i \Device\ NPF_{52CE8828-EBC7-4D0E-AE73-C8AD67EFD3E2}
    """
    present = False
    my_device_name = ''
    cmd = 'getmac /v /nh /fo csv'
    _, output = acra_syscmd(cmd, encoding='utf-8',my_shell=True)
    output = output.split('\n')
    for line in output:
        if nic_name in line:
            present = True
            my_split_line = line.split(',')
            device_name = my_split_line[-1].rstrip('\r')
            my_device_name = device_name.replace("Tcpip", "NPF")
    return present, my_device_name

def contains_number(value):
    '''
    searches for digits in strings.
    '''
    for character in value:
        if character.isdigit():
            return True
    return False

if __name__ == '__main__':

    def show_test_info(anyinfo):
        '''
        Debug function to display the values and limits passed in for the test specific case.
        '''

        return ('DEBUG: show_test_info substitute function')

    from acra.base_test_info import BaseTestInfo

    #my_folder = 'c:/ACRA/Test/COPQ_Libraries/Python3_PEP8/PROCESS_UTILS/acra/RESULTS/ABC1234/resistance_test'

    #retval = check_folder(my_folder)

    #print ('check_folder: return value: {retval}')

    testinfo = BaseTestInfo()

    testinfo.type_no = 0xDEF
    testinfo.ser_no = 'ABC1234'
    testinfo.top_sector = 0x80
    testinfo.product_name = 'AXN/ABC/123/AB1'

    testinfo.humidity_limits = [30.0, 80.5]
    testinfo.temp_limits = [18.0, 28.5]

    testinfo.report_name = f'{testinfo.test_folder}\\{testinfo.test_report_folder}\\{testinfo.ser_no}_TEST_REPORT.TXT'

    testinfo.gen_final_report()
