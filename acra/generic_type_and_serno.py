'''
Name:        generic_type_and_serno.py
Purpose:
Author:      SRM

Created:     16/03/2021
Copyright:   (c) Curtiss Wright 2022
Licence:     <your licence>

History:

22/6/2022   CJG Pylint score improved


needs to use information available in base_test_info

'''
from os import system, path
from time import sleep

from acra.serial_number_tools import SerialNo
from acra.common_functions import TIC_YEAR_INFO, acra_syscmd
from acra.reporting_tools import section_heading, section_result, pass_fail
from acra.test_framework import check_folder



class CHASSIS:
    '''
    
    debug class required for debugging this library file
    
    class for individual chassis
    Contains global constants for the chassis and then a list of
    slots contained in the chassis, with all of their associated parameters.
    '''
    def __init__(self, chassis):
        '''
        slots accessed by slot number
            J0 = controller for AXON so ix = 0
            J2 = controller for KAM  so ix = 2
            etc.
        '''
        self.max_slots  = 17    # use the global constant
        self.chassis    = chassis
        self.logfile    = ''
        self.job        = []
        self.slots      = []
        self.temp_stamp = ''
        self.time_stamp = ''
        self.log_file   = ''
        self.slot        = 0
        
        self.module_info = []

        self.xml_filenames = ['locations.xml', 'config.xml' ]
        self.chassis_retry_count = 3
        self.bcu_slots   = []
        self.filename    = ''
  
        # these need to be passed from base_test_info
        self.tftp        = True
        self.ser_no      = ''
        self.aft_ser_nos = []
        self.aft_test    = False          
        self.bcu_ip_addr = ''
        self.fpga        = ''
        self.fpga_rev    = ''
        self.type_no     = ''

    def getslotinfo(self) :
        '''
        Get the information on a specific slot
        
        Only works with TFTP controllers
        
        '''
        print('\n\n\n\n')
        
        self.filename = 'chs_sts.log'
        retval = 0
        
        # delete the chassis info XML if it already exists
        # assumes these exist in the cwd.
        for filename in self.xml_filenames:
            if path.isfile(filename):
                acra_syscmd(f'ATTRIB -R {filename}')
                acra_syscmd(f'del /Q {filename}')

        # Windows 7 check and path alterations are handled by the framework now when BaseTestInfo is instantiated
        # by BaseTestInfo.check_os()

        tftp_read_success = False
        for filename in self.xml_filenames:
            cmd    = f'tftp -i {self.bcu_ip_addr} get /0/{filename}'   # SRM Check

            read_retry_count = 0
            while (read_retry_count < self.chassis_retry_count) and not tftp_read_success:
                chs_read_result = system(cmd)
                if chs_read_result != 0:
                    system('cls')                   # Don't want the user to see the 'error on server' message
                else: 
                    tftp_read_success = True

                if not tftp_read_success:
                    sleep(read_retry_count)
                    read_retry_count += 1

        if (read_retry_count >= 5) and not tftp_read_success:
            print ('ERROR: Unable to read chassis configuration using TFTP') 
            retval = -1

        return retval
       

    def read_slot_info(self) :
        '''
        find the slot information for each occupied Jslot in the chassis
        and returns a list of occupied slot data in config.xml format. 
        
        All irelevant information in config.xml.location.xml is ignored.
        
        Note that the bitwalk results are removed to make the information consistent
        Whether a slot is occupied or not is determined by whether the bitwalk succeeded
        or not when parsing locations.xml, so no information is lost.
    
        Example location.xml contents

            <?xml version="1.0" encoding="UTF-8"?>
            <!DOCTYPE DAU SYSTEM "http://www.acracontrol.com/dtds/discovery-1.0.dtd">
            <DAU Type="0xffff">
            <Module Location="J2" BitWalk="0xffff" Type="0xaf0" Serial="0x80061b2b" FPGAType="0x100c" FPGAConf="0x1db" FPGARev="0x4">
            <Parameter Name="PCBTemperature" Value="23"/>
            </Module>
            <Module Location="J3" BitWalk="0x0" Type="0x0" Serial="0x8100" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J4" BitWalk="0xffff" Type="0x9b0" Serial="0x80080333" FPGAType="0x140d" FPGAConf="0x141" FPGARev="0xc"/>
            <Module Location="J5" BitWalk="0x0" Type="0x0" Serial="0x0" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J6" BitWalk="0xffff" Type="0x9b0" Serial="0x80080331" FPGAType="0x140d" FPGAConf="0x141" FPGARev="0xc"/>
            <Module Location="J7" BitWalk="0x0" Type="0x0" Serial="0x0" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J8" BitWalk="0x0" Type="0x8031" Serial="0x8035802c" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J9" BitWalk="0x0" Type="0x0" Serial="0x8100" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J10" BitWalk="0x0" Type="0x0" Serial="0x0" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J11" BitWalk="0x0" Type="0x0" Serial="0x0" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J12" BitWalk="0x0" Type="0x0" Serial="0x8100" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J13" BitWalk="0x0" Type="0x803a" Serial="0x80318022" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J14" BitWalk="0x0" Type="0x0" Serial="0x0" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J15" BitWalk="0x0" Type="0x807f" Serial="0x80248027" FPGAType="0x0" FPGAConf="0x0" FPGARev="0x0"/>
            <Module Location="J17" BitWalk="0xffff" Type="0xaf0" Serial="0x80061b2b" FPGAType="0x100c" FPGAConf="0x1db" FPGARev="0x4"/>
            </DAU>        
        
        Example config.xml contents
        
             <?xml version="1.0" encoding="UTF-8"?>
            <!DOCTYPE DAU SYSTEM "http://www.acracontrol.com/dtds/discovery-1.0.dtd">
            <DAU Type="0xffff">
            <Module Location="J2" Type="0xaf0" Serial="0x80061b2b" FPGAType="0x100c" FPGAConf="0x1db" FPGARev="0x4">
            <Parameter Name="PCBTemperature" Value="23"/>
            </Module>
            <Module Location="J4" Type="0x9b0" Serial="0x80080333" FPGAType="0x140d" FPGAConf="0x141" FPGARev="0xc"/>
            <Module Location="J6" Type="0x9b0" Serial="0x80080331" FPGAType="0x140d" FPGAConf="0x141" FPGARev="0xc"/>
            <Module Location="J17" Type="0xaf0" Serial="0x80061b2b" FPGAType="0x100c" FPGAConf="0x1db" FPGARev="0x4"/>
            </DAU>       
            
        
        The primary dfferences are that config.xml only returns entries for slots that are occupied and that locations.xml indicates
        whether the bitwalk was successful, or not
        
        '''

        # back up the selected slot before interrogating the chassis
        sel_slot = self.slot
        
        chassis_config = []
        file_found = False
        for filename in self.xml_filenames:
            if path.isfile(filename):
                if filename.upper() == 'CONFIG.XML':
                    file_found = True
                    with open(filename, 'r', encoding="UTF-8") as xml_file:
                        xml_file_contents = xml_file.readlines()
                        for line in xml_file_contents:
                            if line.find("Module Location") != -1:
                                chassis_config.append(line.strip())
                    #break            
                            

                elif filename.upper() == 'LOCATIONS.XML':
                    file_found = True
                    with open(filename, 'r', encoding="UTF-8") as xml_file: 
                        xml_file_contents = xml_file.readlines()
                        for line in xml_file_contents:
                            if line.find('BitWalk="0xffff"') != -1:                    
                                s_line = line.split()
                                o_line = ''
                                for entry in s_line:
                                    if entry.find("BitWalk") == -1:
                                        o_line += f'{entry} '

                                chassis_config.append(o_line.strip())
                    #break
        
        if file_found:
            self.process_chassis_xml_file(chassis_config)

        else: 
            print ('ERROR: Unable to read chassis configuration file:')
            print ('\t\tCONFIG.XML or LOCATIONS.XML not found')

        # restore the original slot selected.
        self.slot = sel_slot


    def process_chassis_xml_file(self, chassis_config):
        '''
        Don't need to faff about with the TFTP command/path any more

        Only lists occupied slots, presence inlist indicates

        Example file contents:
            
            ['<Module Location="J2" Type="0xaf0" Serial="0x80061b2b" FPGAType="0x100c" FPGAConf="0x1db" FPGARev="0x4">', 
             '<Module Location="J4" Type="0x9b0" Serial="0x80080333" FPGAType="0x140d" FPGAConf="0x141" FPGARev="0xc"/>', 
             '<Module Location="J6" Type="0x9b0" Serial="0x80080331" FPGAType="0x140d" FPGAConf="0x141" FPGARev="0xc"/>', 
             '<Module Location="J17" Type="0xaf0" Serial="0x80061b2b" FPGAType="0x100c" FPGAConf="0x1db" FPGARev="0x4"/>'
            ]


        returns a list of the form
        
             # serial    slot   type         TIC      rev
        
            [['AAG6955', 'J02', '0AF0', 'TIC/AC/012', '04'], 
             ['AAI0819', 'J04', '09B0', 'TIC/AG/013', '0C'], 
             ['AAI0817', 'J06', '09B0', 'TIC/AG/013', '0C'], 
             ['AAG6955', 'J17', '0AF0', 'TIC/AC/012', '04']
            ]

        '''
        
        # instantiate serial_number_tools class for decding serial numbers
        test_serno = SerialNo()
        self.module_info = []
        
        for line in chassis_config:

            line_list = line.split()


            slot        = ''
            type_no     = ''
            fpga_type   = ''
            serno       = ''

            for el in line_list:
                el = el.split('=')

                if el[0].find('Location') != -1:
                    slot = el[1].strip('"')
                        
                    if len(slot) < 3:
                        temp = 'J0' + str(slot[1])
                        slot = temp


                    
                if el[0].find('Serial') != -1:
                    serno           = (el[1].strip('"')).lstrip('0x')
                    if len(serno) < 8:
                        serno = '0' + serno
                    serial_letters = test_serno.dec_ser_no(int(serno[0:4], 16))

                    serial_numbers     = int(serno[4:8], 16)
                    serno           = f'{serial_letters}{serial_numbers:04d}'

                if el[0].find('FPGAType') != -1:
                    fpga_type = int((el[1].strip('"')).lstrip('0x'),16)
                    
                    fpga_letters = (fpga_type & 0xFF00) >> 8
                    fpga_numbers = fpga_type & 0xFF
                    
                    fpga_id = f'TIC/{TIC_YEAR_INFO[fpga_letters]}/{fpga_numbers:03d}'

                elif el[0].find('Type') != -1:
                    type_no = int((el[1].strip('"')).lstrip('0x'),16)
                    type_no = f'{type_no:04X}'                    
                    
                if el[0].find('FPGARev') != -1:
                    # this gets a match but is not cleaning the string correctly.
                    fpga_rev = el[1].replace('"','')
                    fpga_rev = fpga_rev.replace('>','')
                    fpga_rev = fpga_rev.replace('/','')
                    fpga_rev = fpga_rev.lstrip('0x')
                    fpga_rev_int = int(fpga_rev, 16)
                    fpga_rev = f'{fpga_rev_int:02X}'

            self.module_info.append([serno, slot, type_no, fpga_id, fpga_rev])

    def show_module_info(self):
        '''
        return a string that displays the chassis contents in human readable form
        '''
        
        msg = '\tChassis 0 contents:\n\n'
        msg += '\tSlot\tSerial\tType\tFPGA\t\tFPGA Rev\n'
        for entry in self.module_info:
            msg += f'\t{entry[1]}\t{entry[0]}\t{entry[2]}\t{entry[3]}\t{entry[4]}\n'
        
        return (msg)
        

    def check_type_serno(self):
        '''
        perform the check based on the ser no and aft test flag passed in from base_test_info
        
         self.module_info is of this form
        
            [['AAG6955', 'J02', '0AF0', 'TIC/AC/012', '04'], 
             ['AAI0819', 'J04', '09B0', 'TIC/AG/013', '0C'], 
             ['AAI0817', 'J06', '09B0', 'TIC/AG/013', '0C'], 
             ['AAG6955', 'J17', '0AF0', 'TIC/AC/012', '04']
            ]
        '''

        # print(f'\n\n\t<Debug in check_type_serno> \n\tself.module_info is {self.module_info} \n\n ')

        #============================================
        # Initialise the section_fails counter to 0
        #============================================
        section_fails = 0
        
        #============================================
        # demonstrates usage of LessThanResultFloat()
        #============================================
        heading = 'Type and Serial Number Tests'
        rpt = section_heading(heading)     
        
        test_heading = 'Type, Serial Number and FPGA revision tests'

        got_match = False      
        for entry in self.module_info:
            # print(f'\n\n\t<Debug> \n\tentry is {entry} \n\n ')
            if entry[0] == self.ser_no:
                got_match = True                                
                rpt += f'\t{entry[0]} Type No: 0x{entry[2]} located in slot {entry[1]}\n'
                if entry[2] == self.type_no:
                    rpt += f'\tPASS:\tType no 0x{entry[2]} (Expected 0x{self.type_no})\n'
                else:
                    rpt += f'\tERROR:\tIncorrect type number returned\t0x{entry[2]} (Expected 0x{self.type_no})\n'
                    section_fails += 1

                if entry[3] == self.fpga:
                    rpt += f'\tPASS:\tFPGA ID {entry[3]}  (Expected 0x{self.fpga})\n'
                else:
                    rpt += f'\tERROR:\tIncorrect FPGA ID returned\t0x{entry[3]} (Expected 0x{self.fpga})\n'
                    section_fails += 1

                if entry[4] == self.fpga_rev:
                    rpt += f'\tPASS:\tFPGA Rev {entry[3]} (Expected 0x{self.fpga_rev})\n'
                else:
                    rpt += f'\tERROR:\tIncorrect FPGA Rev returned\t0x{entry[4]} (Expected 0x{self.fpga_rev})\n'
                    section_fails += 1


        if not got_match:
            rpt += '\n\n**** ERROR: Module Serial Number not found in chassis **** \n\n'
            section_fails += 1
            
            
        rpt += section_result(heading, pass_fail(section_fails))

        rpt += '\n\n' # formatting            

        if section_fails == 0:
            res = 'Pass'
        else:
            res = 'FAIL'

        interim_rep = section_result(test_heading, res)
        interim_rep += rpt
    
 

        
        return interim_rep
        

def check_type_serno_test(anyinfo):
    '''
    type & serial number test for bench and AFT tests
    
    this is the bit that gets called from the actual test code.
    '''

    test_chs_info = CHASSIS(0)
    test_chs_info.slot = 17
    
    # these are passed in from base_test_info
    test_chs_info.bcu_ip_addr = anyinfo.bcu_ip_addr
    test_chs_info.tftp        = anyinfo.tftp
    test_chs_info.ser_no      = anyinfo.ser_no
    test_chs_info.aft_ser_nos = anyinfo.aft_ser_nos
    test_chs_info.aft_test    = anyinfo.aft_test    
    test_chs_info.fpga        = anyinfo.fpga1
    test_chs_info.fpga_rev    = anyinfo.fpga1_rev
    test_chs_info.type_no     = anyinfo.type_no
    
    print (f'\n\nRunning test {anyinfo.this_test_name}\n\n')
    
    test_heading    = 'Type, Serial Number and FPGA revision tests'
    local_test_name = 'Type_and_Serial_Number_Test'

    # read back the information from the chassis
    # commented out as I've no hardware...
    print (f'\n\n{anyinfo.this_test_name}\tReading back the chassis information\n\n')
    test_chs_info.getslotinfo()

    # read and parse the cofig/location.xml file
    # and return the chassis summary data
    test_chs_info.read_slot_info()
    
    # print it to the screen for sanity
    # print (test_chs_info.show_module_info())
    
    msg = (test_chs_info.show_module_info())
    print(msg)

    if test_chs_info.aft_test: 

        for ser_no in test_chs_info.aft_ser_nos:

            # print(f'\n\n\t<Debug>ser_no is {ser_no} for from {test_chs_info.aft_ser_nos} \n\n ')
            
            # set up the ser_no for reporting
            anyinfo.ser_no = ser_no
            
            # set up the serial number for the test
            test_chs_info.ser_no = ser_no

            #============================================
            # set up the test name path and folders for 
            # the interim report setting up the test name 
            # as its cleared when the report is written.
            #============================================
            anyinfo.set_test_name(local_test_name)
            anyinfo.set_interim_rpt_path(anyinfo.this_test_name)             

            # run the test
            interim_rep = test_chs_info.check_type_serno()
            
            print(interim_rep)
                
            #===========================================
            # this call code must exist in every test
            #===========================================
            anyinfo.write_interim_report(test_heading, interim_rep)

        #======================================
        # clear the base_tst_info serial number
        # when running multiple modules
        #======================================
        anyinfo.ser_no = ''
        
    else:

        #============================================
        # set up the test name, path path and folders 
        # for the interim report
        #============================================
        anyinfo.set_test_name(local_test_name)
        anyinfo.set_interim_rpt_path(anyinfo.this_test_name) 
        
        # run the test
        interim_rep = test_chs_info.check_type_serno()

        print(interim_rep)

        #===========================================
        # this call code must exist in every test
        #===========================================
        anyinfo.write_interim_report(test_heading, interim_rep)

        



if __name__ == '__main__':
    from acra.base_test_info import BaseTestInfo

    test_info = BaseTestInfo()

    # from the test INI file
    test_info.bcu_ip_addr = '192.168.28.1'  # now bcu_ip_addr
    test_info.tftp = True
    #test_info.ser_no             = 'AAI0817'
    test_info.ser_no             = ''
    test_info.aft_ser_nos        = ['AAI0819', 'AAI0817']
    #test_info.aft_ser_nos        = []
    test_info.aft_test           = True    
    test_info.fpga1              = 'TIC/AG/013'
    test_info.fpga1_rev          = '0C'
    test_info.type_no            = '09B0'

    # set up the results list so this can be run standalone
    if test_info.aft_test:
        for test_ser_no in test_info.aft_ser_nos:
            test_info.results_dict[test_ser_no] = []
            check_folder(test_ser_no)
    else:
        test_info.results_dict[test_info.ser_no] = []
        check_folder(test_info.ser_no)


    check_type_serno_test(test_info)
