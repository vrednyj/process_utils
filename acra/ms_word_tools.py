'''

 Name:     ms_word_tools.py

 Author:   Chris Gilbert

 Date:     September 16, 2022

 Copyright Curtiss Wright 2022

 Description

 Python 3 PEP8 Library classes for interfacing/automating MS Word tools

 Library/calls based on Mark Hammonds 'easyword.py' examples

 help(modulename) returns useful information
 
 Useful references:
 
    https://docs.microsoft.com/en-us/office/vba/api/word.wdbuiltinstyle
 
 History:
   
   1.0  CJG 16/09/2022  Split out from reporting_tools
   
 
'''

import win32com.client
#from win32com.client import constants

class MSWordWrapper:
    ''' Wrapper around Word documents to make them easy to build.
        Has variables for the Applications, Document and Selection; 
        most methods add things at the end of the document
    '''
    def __init__(self, template_file=None):
        
        self.word_app = win32com.client.dynamic.Dispatch('Word.Application')
        
        if template_file:
            self.word_doc = self.word_app.Documents.Add(Template=template_file)
        else:
            self.word_doc = self.word_app.Documents.Add()

        self.word_doc.Range(0,0).Select()
        self.word_sel = self.word_app.Selection
        self.word_sel.Collapse(0)
        
        self.styles = []
        
    def show(self):
        '''
        convenience when debugging
        '''
        self.word_app.Visible = 1  

    def close_doc(self):
        '''
        Close the document
        '''
        self.word_doc.Close()
               
    def get_style_list(self):
        '''
        returns a dictionary of the styles in a document
        '''
        
        stylecount = self.word_doc.Styles.Count
        for i in range(1, stylecount + 1):
            style_object = self.word_doc.Styles(i)
            self.styles.append(style_object.NameLocal)

    def save_as(self, filename):
        '''
        Saves the document with a new name or format. 
        
        Figures out the file type from the extension and saves in that file format.
        If it doesn't recognise the format the file is saved in word default format, for the version of word installed
        
        wdFormatDocument97     'DOC'   :0  #Microsoft Office Word 97 - 2003 binary file format.
        wdFormatTemplate97     'DOT'   :1  #Word template format.
        wdFormatRTF            'RTF'   :6  #Rich text format (RTF).
        wdFormatUnicodeText    'TXT'   :7  #Unicode text format.
        wdFormatHTML           'HTML'  :8  #Standard HTML format.
        wdFormatXML            'XML'   :11 #Extensible Markup Language (XML) format.
        wdFormatXMLDocument    'DOCX'  :12 #XML document format.
        wdFormatPDF            'PDF'   :17 #PDF format.
        wdFormatDocumentDefault        :16 #Word default document file format. For Word 2010, this is the DOCX format.

        Full list of formats below:

        Name    Value   Description
        wdFormatDocument                    0   Microsoft Office Word 97 - 2003 binary file format.
        wdFormatDocument97                  0   Microsoft Word 97 document format.
        wdFormatTemplate                    1   Word template format.
        wdFormatTemplate97                  1   Word 97 template format.
        wdFormatText                        2   Microsoft Windows text format.
        wdFormatTextLineBreaks              3   Windows text format with line breaks preserved.
        wdFormatDOSText                     4   Microsoft DOS text format.
        wdFormatDOSTextLineBreaks           5   Microsoft DOS text with line breaks preserved.
        wdFormatRTF                         6   Rich text format (RTF).
        wdFormatEncodedText                 7   Encoded text format.
        wdFormatUnicodeText                 7   Unicode text format.
        wdFormatHTML                        8   Standard HTML format.
        wdFormatWebArchive                  9   Web archive format.
        wdFormatFilteredHTML                10  Filtered HTML format.
        wdFormatXML                         11  Extensible Markup Language (XML) format.
        wdFormatXMLDocument                 12  XML document format.
        wdFormatXMLDocumentMacroEnabled     13  XML document format with macros enabled.
        wdFormatXMLTemplate                 14  XML template format.
        wdFormatXMLTemplateMacroEnabled     15  XML template format with macros enabled.
        wdFormatDocumentDefault             16  Word default document file format. For Word, this is the DOCX format.
        wdFormatPDF                         17  PDF format.
        wdFormatXPS                         18  XPS format.wdFormatFlatXML              19  Open XML file format saved as a single XML file.
        wdFormatFlatXMLMacroEnabled         20  Open XML file format with macros enabled saved as a single XML file.
        wdFormatFlatXMLTemplate             21  Open XML template format saved as a XML single file.
        wdFormatFlatXMLTemplateMacroEnabled 22  Open XML template format with macros enabled saved as a single XML file.
        wdFormatOpenDocumentText            23  OpenDocument Text format.
        wdFormatStrictOpenXMLDocument       24  Strict Open XML document format.


        '''
        
        #msg = f'save_as: filename: {filename}'
        #print (msg)
        #input('hit enter')
        
        word_format_document_default = 16
        
        file_format = {
                        'DOC' :0,
                        'DOT' :1,
                        'RTF' :6,
                        'TXT' :7,
                        'HTML':8,
                        'XML' :11,
                        #'DOCX':12,
                        'DOCX':16,
                        'PDF' :17,
                     }

        s_filename = filename.split('.')
        try:
            extension = s_filename[1].upper()
        except IOError as io_exc:
            print (f'ERROR: Filename {filename} is in an unsupported format\n\texpected filename.ext\n\tLegal extensions are DOC, DOT, RTF, TXT, HTML, XML, DOCX, PDF')
            print (io_exc)
        
        if extension in file_format:
            doc_format = file_format.get(extension)
        else:
            doc_format = word_format_document_default
       
       
        #print (doc_format)
        #input (filename)    # for debug
        
        self.word_doc.SaveAs(filename, FileFormat=doc_format)
       
    def print_out(self):
        '''
        print the document
        '''
        self.word_doc.PrintOut()

    def select_end(self):
        '''
        ensures insertion point is at the end of the document
        0 is the constant wdCollapseEnd; don't want to depend
        on makepy support.
        '''
        self.word_sel.Collapse(0)
      
    def add_text(self, text):
        '''
        Add some text
        '''
        self.word_sel.InsertAfter(text)
        self.select_end()
        
       
    def add_styled_para(self, text, stylename):
        '''
        Add a styled paragraph styled according to stylename
        stylename is one of the formats defined in the template
        '''
        if text[-1] != '\n':
            text = text + '\n'
        
        self.word_sel.InsertAfter(text)
        self.word_sel.Style = stylename
        self.select_end()
        
    def add_list_styled_para(self, mylist, stylename):
        '''
        Add a list to a styled paragraph styled according to stylename
        stylename is one of the formats defined in the template
        '''
        
        for text in mylist:        
            if text[-1] != '\n':
                text = text + '\n'
        
            self.word_sel.InsertAfter(text)
            self.word_sel.Style = stylename
            
        self.select_end()        
            
    def add_table(self, table, style_id=None):
        '''
        Takes a 'list of lists' of data.
        first we format the text.  You might want to preformat
        numbers with the right decimal places etc. first.
        '''
        
        textlines = []
        for row in table:
            textrow = list(map(str, row))   #convert to strings
            no_cols = len(textrow)
            #textline = join(textrow, '\t')
            textline = ''
            ix = 0
            for el in textrow:
                textline += el
                if ix < (no_cols-1):
                    textline += '\t'
                ix += 1
            textlines.append(textline)
        #text = join(textlines, '\n')
        #text = textlines.join('\n')
        text = ''
        ix = 0
        no_lines = len(textlines)
        for line in textlines:
            text += line
            if ix < (no_lines-1):
                text += '\n'
            ix += 1
        # add the text, which remains selected
        self.word_sel.InsertAfter(text)
        
        print (text)
        #input ('hit enter')
        #convert to a table
        word_table = self.word_sel.ConvertToTable(Separator='\t')
        #and format
        # styleid is an integer in the range of 0 to 42, need to suck it and see to determine what format is preferred
        if style_id:
            word_table.AutoFormat(Format=style_id)
        
        self.select_end()
        
    def add_inline_excel_chart(self, filename, caption='', height=216, width=432):
        '''
        adds a chart inline within the text, caption below.
        '''
        
        
        # add an InlineShape to the InlineShapes collection 
        #- could appear anywhere
        shape = self.word_doc.InlineShapes.AddOLEObject(
            ClassType='Excel.Chart',
            FileName=filename
            )
        # set height and width in points
        shape.Height = height
        shape.Width = width
            
        # put it where we want
        shape.Range.Cut()
        
        self.word_sel.InsertAfter('chart will replace this')
        self.word_sel.Range.Paste()  # goes in selection
        self.add_styled_para(caption, 'Normal')
        

