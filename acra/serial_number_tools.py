'''
-------------------------------------------------------------------------------
Pyscripter Python File/Module Template: 02-Oct-2013
-------------------------------------------------------------------------------
Name:        serial_number_tools.PY

Author:      Chris Gilbert

Created:     July 21, 2017

Copyright:   (c) Curtiss Wright Corporation ACRA BU 2017

Description:
    Lower level functions called by the various functions

-------------------------------------------------------------------------------
History:
   21-Jul-2017:   Chris Gilbert      Created, based on BCU Pythons micro_functions.py
   
   13-Mar-2018.   Chris Gilbert      Added function to create top sector populating
                                     the module name field, serial number and timestamp
                                     as well as the type number
                                     
                                     Modified dec_letters and enc_ser_no to handle 
                                     one, two and 3 letter serial numbers, serial numbers 
                                     in the following formats:
                                     
                                     'K0598', 'ZT04277', 'AAC07846'
                                     
    3-Oct-2019:  Chris Gilbert       Migrated to Python 3.  
    
    8-Feb-2022: Chris Gilbert        Made PEP8 compliant
    
    

--------------------------------------------------------------------------------
'''

from os import path, system

from time import time


###########################################################################
# Type and Serial number functions
# code/decode/read/set functions
###########################################################################

class SerialNo():
    '''
    Class for Serial number functions
    Everything that is required to encode and decode serial numbers lives here
    '''
    def __init__(self):
        self.letters_array = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z', ' ']
        self.letters = ''
        self.numbers = 0
        
        # strings for printing
        self.sn_letters = ''
        self.sn_numbers = ''
        
        # utilities for dealing with auxillary housekeeping data as currently used in the BCU/402 family
        # handled as a proper class as this functionality was never available before.
        self.aux_offset   = 0
        self.aux_data     = []
        self.tftp         = False   # assume traditional; programming tools, not tftp
        

    def dec_letters(self, index) :
        '''
        Decode the serial number letter code
        '''
        if index <= 26 :
            retval = self.letters_array[index]
        else :
            retval ='F'
            
        return retval    

    def enc_letters(self, match) :
        '''
        Return the letter code (index) for any letter used in a serial number
        '''
        lcode = self.letters_array.index(match.upper())
        return (lcode)

    def get_serial(self, ser_no):
        '''
        Serial number is of the form AB1234 and is passed as a string
        this requires 2 eeprom locations to store, so we split it into two 16 bit parts
        The letters are allocated a code and the numbers are written in HEX format.
        
        this functin is clearly not used
        '''
        self.sn_letters = f"{self.letters_array.index(ser_no[0]).upper():02X}{self.letters_array.index(ser_no[1]).upper():02X}"
        self.sn_numbers = f"{int(ser_no[2:]):04X}"
        return self.sn_letters,self.sn_numbers
        
    def split_ser_no(self, serial_string):
        '''
        takes any serial number and returns it as a list of string and number
        '''
        
        nos = ''

        for char in serial_string:
            if char in self.letters_array:
                if char != ' ':
                    self.letters += char
            else:
                nos += char
                
        self.numbers = int(nos, 16)
        
        return [self.letters, self.numbers]
            
                            

    def dec_ser_no(self, serial_letters):
        '''
        decode new serial numbers - new and old formats
        Input is the character part of the serial number
        a single 16 bit number.

        returns a string
        '''
        new_type    = 0x8000
        n_hi_mask   = 0x7C00
        n_mid_mask  = 0x03E0
        n_lo_mask   = 0x001F

        n_hi_shift  = 10
        n_mid_shift = 5

        o_hi_mask   = 0xFF00
        o_lo_mask   = 0x00FF

        o_hi_shift  = 8

        ser = ''

        if (serial_letters & new_type) == new_type:
            ser += self.dec_letters((serial_letters & n_hi_mask) >> n_hi_shift)
            ser += self.dec_letters((serial_letters & n_mid_mask) >> n_mid_shift)
            ser += self.dec_letters(serial_letters & n_lo_mask)

        else :
            ser += self.dec_letters((serial_letters & o_hi_mask) >> o_hi_shift)
            ser += self.dec_letters(serial_letters & o_lo_mask)

        return ser


    def enc_ser_no(self, ser_string):
        '''
        Takes a string as input and generates the serial number coding from this string
        returns a list of the form (encoded letters, encoded numbers)

        serial number is in form AB1234 or ABC1234
        
        handles numbers of the form A1234 the same as ABC1234
        '''

        n_hi_shift  = 10
        n_mid_shift = 5

        o_hi_shift  = 8

                                                                # test on the third character to see if its a letter or a number
        if ser_string[2].upper() in (self.letters_array):
            high  = self.enc_letters(ser_string[0])
            mid = self.enc_letters(ser_string[1])
            low  = self.enc_letters(ser_string[2])

            numbers = int(ser_string[3:])

            letters = 0x8000
            letters += high  << n_hi_shift
            letters += mid << n_mid_shift
            letters += low

        elif ser_string[1].upper() in (self.letters_array):
            letters = 0x0000
            high  = self.enc_letters(ser_string[0])
            low  = self.enc_letters(ser_string[1])
            letters += high  << o_hi_shift
            letters += low

            numbers = int(ser_string[2:])
        
        else:               # must be a really old module...
        
            high  = self.enc_letters(' ')
            mid = self.enc_letters(' ')
            low  = self.enc_letters(ser_string[0])

            numbers = int(ser_string[1:])

            letters = 0x8000
            letters += high  << n_hi_shift
            letters += mid << n_mid_shift
            letters += low


        return [letters, numbers]


    def get_mod_name_word(self, word_ofs, module_name):
        '''
        Returns a single word of the module name. The first 2 chars are
        word 0, the next 2 are word 1 and so on. The name is nul-terminated
        and words after the nul are 0xFFFF.
        
        expects that any checking on the module name has happened at a higher level.
        
        Heavily based on the function of the same name used in BCU_Python
        
        Because ksd/BCU_Python/whatever is reading 16-bit values from the file of 
        8-bit bytes transferred by whatever protocol (tftp or proprietry). 
        Because ACRA FPGAs use little-endian words the LSB is read first, then the MSB, 
        as the FPGA would read it. But in the text file they are printed in the opposite order, 
        
        i.e. in 0x1FFFFF 1234 
        
        12 is the MSB, 34 the LSB but they are the opposite way around (34, 12) in the file of 8-bit bytes.

        '''
        name_bytes = ['','']
        name_bytes[0] = 0xFF
        name_bytes[1] = 0xFF
        
        offset = word_ofs*2
        name_len = len(module_name)
        module_name = module_name.upper()
        for ix in range(2):
            if offset == name_len:
                name_bytes[ix] = 0
            elif offset < name_len:
                name_bytes[ix] = ord(module_name[offset])
            offset = offset+1
    
        return (name_bytes[1] << 8) | name_bytes[0]
        
        

    def create_top_sector(self, hex_type, ser_string, eprom_size, name_string = '', tftp = False) :
        '''
        creates a string with all entries set to 1 except for the last 8 entries.
        these will be filled with the type and serial no
        Uses the eeprom size to determine the top sector number.
    
        <FJP>The module name is stored at address FE00 - FE0B. These addresses
        are from the KAD/BCU/142 FPGA. If we don't have a module name when we
        get here, the sector must be read and the module name extracted. If we
        have it, then it either matches what is in the module, or it has been
        entered by the user; either way it gets written as is.</FJP>
    
        Top sector width is 16 bits.
        
        if tftp == False return the top sector in KAM format otherwise return it as a list of eeprom contents.
                
        
        '''
        
        max_lines = 0x10000
    
        fill_data = 0xFFFF
    
        sector_addr = (eprom_size - 1)
    
        max_lines = 0x10000
    
        output = ''
    
        start_name = 0xFFE0
        
        self.tftp = tftp        # save it for use later if necessary, so it does not need to be passed continuously
        
        if isinstance(name_string, str):
            name_length = len(name_string)
        else: 
            name_length = 0
    
       
        data = []
        
        for ix in range (max_lines-8) :
            
            # note that the name is byte swapped, unlike all other data stored in eeprom
            # this is because the other data is considered to be hex numbers but the name 
            # string is considered to be a string, in not so many words.
            if (start_name <= ix <(start_name+name_length)):
                output_word = f'{self.get_mod_name_word(ix-start_name, name_string):04X}'              

            else:
                output_word = f'{fill_data:04X}'

            data.append(output_word)
    
        serno   = self.enc_ser_no(ser_string)
        sn_letters     = f'{serno[0]:04X}'
        sn_numbers     = f'{serno[1]:04X}'
        
        time_stamp = f'{int(time()):012X}'
        
        data.append(f'{time_stamp[0:4]}')
        data.append(f'{time_stamp[4:8]}')
        data.append(f'{time_stamp[8:12]}')
        data.append(f'{sn_letters}')
        data.append(f'{sn_numbers}')
        data.append(f'{fill_data:04X}')
        data.append(f'{fill_data:04X}')
        data.append(f'{hex_type:04X}')
        

        if not self.tftp:
            output = ''
            for ix in range (max_lines):
                output += f'{sector_addr:02X}{ix:04X} {data[ix]}\n' 
            retval = output    
        else:
        
            output = []
            for ix in range (max_lines):
                upper_byte = ((int(data[ix],16) & 0xFF00) >> 8)
                lower_byte = (int(data[ix],16) & 0x00FF)
                
                output.append(upper_byte)
                output.append(lower_byte)

            retval = [sector_addr, output]

        return retval
    
    def write_binary_file(self, sector, data, ext = 'bin'):
        '''
        Uses the sector number as the root of the file name
        i.e. %0X0000.%s %(sector, ext)
        
        Ext is the file extension and defaults to 'bin', though 'sys' may be required for some products

        Writes a list of byte sized integers to the binary file

        data must be a list of integers, each integer in the range of 0 to 255
        '''

        filename = f'{sector:X}0000.{ext}'

        #delete it if it already exists

        try:
            del_file(filename)
        except IOError as exc:
            print (f'Unable to delete {filename}')
            print ('Make sure it is not locked by another program/utility')
            print (exc)
            input ('Hit enter to continue')

        # write the data in big endian format
        # all byte swapping etc is handled before we get to here.
        with open(filename, 'wb') as ofile:
            for entry in data:
                ofile.write(entry.to_bytes(1, 'big'))

    def read_aux_file(self, filename):
        '''
        Reads in auxillary data file. The datamust be in the following format:
        7FC100 1104
        7FC101 0501
        7FC102 1109
        7FC103 0200
        7FC104 110D
        7FC105 001F

        first column is the address, second is the data to be written in hex.
        data must be contiguous - no gaps. If gaps are required then each contiguous segment 
        must be read separately.
        
        return is of the form [start_address(hex), list_of_contents(strings)]
        
        filename must include path. 
        
        Exits if file not found.
        
        new functionality so can use class properly, rather than returning data.
        '''
        
        if path.exists(filename):
            with open(filename, 'r', encoding='utf-8') as ifile:
                contents = ifile.readlines()
                
            first_line = True
            
            self.aux_data = []
            for line in contents:
                address, datum = line.strip().split()
                if first_line:
                    self.aux_offset = (int(address, 16) & 0xFFFF)
                    first_line    = False
                    
                self.aux_data.append(datum)
        
            retval = True
        else:
            input (f'ERROR: Unable to find file: {filename}')
            retval = False
        return retval
 
    def insert_aux_data(self, sector_contents):
        '''
        Takes the auxilliary data already generated and inserts it into the output contents/list
        as this is generic, returns the updated sector contents in the required format, text or bin.
        
        '''
                
        # binary or text output...
        if self.tftp:
            # generate a binary list so.
            
            # first convert the data to be inserted into a list of bytes
            
            aux_data_bytes = []
            
            for datum in self.aux_data:
                upper_byte = ((int(datum,16) & 0xFF00) >> 8)
                lower_byte = (int(datum,16) & 0x00FF)
                            
                aux_data_bytes.append(upper_byte)
                aux_data_bytes.append(lower_byte)
            
            ##now turn the word offset into a byte offset
            # and figure out how many bytes of data we need to insert
            byte_offset = self.aux_offset << 1
            no_aux_bytes = len(aux_data_bytes)
            
            output = []
            ix   = 0
            d_ix = 0
            
            for data_byte in sector_contents:
                if ((ix < byte_offset) | (ix >= byte_offset+no_aux_bytes)): 
                    output.append(data_byte) 
                else:
                    output.append(aux_data_bytes[d_ix])   
                    d_ix += 1
                ix += 1
                  
        else:
            # OK classical text output
            
            # convert the data into a list
            # and pull out the sector start address
            ix = 0
            contents = []
            start_address = 0
            
            split_sector_contents = sector_contents.split('\n')
            
            for entry in split_sector_contents:
                if entry != '':
                    address, datum = entry.split()

                    if ix == 0:
                        start_address = int(address, 16)
                        ix += 1         # don't need ix to count higher than 0,1
                    contents.append(datum)
                
            # now regenerate the file contents, with the auxilliary data...
            ix = 0
            d_ix = 0
            output = ''
            
            # how much data to insert
            no_entries = len(self.aux_data)
            
            address = start_address
            
            for datum in contents:
                if self.aux_offset <= ix < self.aux_offset+no_entries: 
                    output += f'{address:06X} {self.aux_data[d_ix]}\n'
                    d_ix   += 1
                else:
                    output += f'{address:06X} {datum}\n'

                ix      += 1
                address += 1
            
        # return the sector contents for all branches
        return output            

class SERIAL_NO(SerialNo):
    '''
    Wrapper for legacy tests. Only the class name is different...
    '''

    def __init__(self):
        '''
        Inherit all of the methods from the parent class
        '''
        super().__init__()    

        
        
def del_file(some_file_name):
    '''
    need this locally as otherwise we have a circular reference...
    this needs to be sorted before we're fully done with this update

    If the filename exists, delete it, quietly and forcefully
    '''
    if path.exists(some_file_name):
        cmd = "del /Q /F " + some_file_name
        system(cmd)        



        
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Main/test area
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




if __name__ == '__main__':

    from sys import argv
    
    old_new  = argv[1]
    
    if old_new.upper() == 'OLD':
        input ('INFO: Running Wrapper class')
        
        test_ser_no = SERIAL_NO()
    else:
        input ('INFO: Running base class')
        test_ser_no = SerialNo()
    

    
    serials = ['K0598', 'ZT04277', 'AAC07846']
    #for serial in serials:
    #
    #    print(serial)
    #    serial_list = ser_no.enc_ser_no(serial)
    #    print(serial_list)
    #
    #    letters = ser_no.dec_ser_no(serial_list[0])
    #    print('%s%05d' %(letters, serial_list[1]))
    #    
    
    A_HEX_STRING    = 0xDEF
    A_SER_STRING  = 'ABC1234'
    AN_EPROM_SIZE  = 0x80
    A_NAME_STRING = 'AXN/ABC/123/AB1'
    
    ADDITIONAL_DATA = 'C:/ACRA/Test/AXNBCU402C_Aux_Data/def_cfg.txt'
    
    MY_TFTP = False
    test_data = test_ser_no.create_top_sector(A_HEX_STRING, A_SER_STRING, AN_EPROM_SIZE, A_NAME_STRING, MY_TFTP)
    
    test_ser_no.read_aux_file(ADDITIONAL_DATA)
    print (f'Phy start address: {test_ser_no.aux_offset:0X}')
    print (test_ser_no.aux_data)
    input('Aux_data: hit enter')
    
    # classic mode
    #data = ser_no.insert_aux_data(data)
    
    #filename = ('topsector.txt')
    #ofile = open(filename, 'w')
    #ofile.write(data)
    #ofile.close()
    
    MY_TFTP = True
    test_sector, test_data = test_ser_no.create_top_sector(A_HEX_STRING, A_SER_STRING, AN_EPROM_SIZE, A_NAME_STRING, MY_TFTP)
    
    print (test_data)
    input ('Sector data: hit enter')
    print (f'sector: {test_sector:02X}')
    
    new_data = test_ser_no.insert_aux_data(test_data)  
    print (new_data)
    input ('new_data: hit enter')
    test_ser_no.write_binary_file(test_sector, new_data)
      
    
    
