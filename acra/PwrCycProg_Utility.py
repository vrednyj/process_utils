'''
-------------------------------------------------------------------------------
 Author:      David Donaldson

 Created:     08/10/2020
 Copyright:   Curtiss-Wright Controls

 Description:
    Power Cycling and Reprogramming Script

    Prior to this development, everyone in DV had their own personal Power Cycling and Reprogramming script
    The purpose of this script is to consolidate the diverging development streams
    The INI file will control how the test will run
    The only change the user will need to make is to the INI file

    If no programming is required, then the INI file variable PowerCycleOnly is set to True
    If only programming and a single power cycle is required then ProgramOnly is set to True
    If neither of these are set to True then the module will be programmed, then power cycled PU_Loop times before reprogramming


    Sample usage:

    try:
        IniFileName = sys.argv[1]
    except IndexError:
        sys.exit('No ini file argument supplied')

    # Instantiate the structure
    PU_STRUCT_INST = PU_STRUCT()
    res = PU_STRUCT_INST.CheckConfig()
    PU_STRUCT_INST.ReadConfig(IniFileName, res)
    PU_STRUCT_INST.ShowCfg()
    PU_STRUCT_INST.CheckLogFile()
    PU_STRUCT_INST.Initialise_Instruments()
    PU_STRUCT_INST.SetPSU_Output()

    try:
        while(1):
            PU_STRUCT_INST.run_pu_prog()

    except KeyboardInterrupt:
        self.msg += ('\n'*3 + '\tCNTRL-C Pressed . Terminating.' + '\n'*3)
        self.WriteLog()
        sys.exit(self.msg)



    INI file format follows Configparser conventions and can be extended beyond the
    the the settings shown in the reference config.ini.

-------------------------------------------------------------------------------
 History:

   Updated 03 Nov, 2020:    CJG     Sample usage added to file header
-------------------------------------------------------------------------------
'''
from os import path
from time import time
from datetime import date
from configparser import *
from random import *
from acra.Bench_PSUs import *
from acra.utils import *
from acra.portchecker import *
from acra.PT_Framework import *
from acra.Thermometer import *
from acra.OVEN import *
import threading

######################
#GLOBAL DEFINITION
######################

class PU_STRUCT :
    def __init__(self):
        # general test configuration settings
        self.prog_cnt                   = ''
        self.DUMMY_XIDML                = ''
        #self.DummyFlag                  = ''
        self.TEST_XIDML                 = ''
        self.DS_Dir                     = ''
        self.ABI_Transactor_Dir         = ''
        self.SectorFilename             = ''
        self.SectorFilename_Orig        = ''
        self.DS_Dir_Msg                 = ''
        self.PU_Loop                    = 1
        self.NWDS3Cmd                   = ''
        self.NWDS3LogFile               = ''
        self.TotalErrors                = 0
        self.ETProgram                  = False
        self.ProgCmd                    = ''
        self.ReportFile                 = ''
        self.LogFileName                = ''
        self.Therm_port                 = 0
        self.ThermometerPresent         = False
        self.PSU_Present                = False
        self.OvenTemp                   = [-40, -20, 0, 20, 60, 85]
        self.LogWait                    = 60
        self.PowerOffTime               = 'random'   # default to random
        self.PSU_Port                   = 0
        self.PSUType                    = 0
        self.ThermType                  = ''
        self.pwrdelay                   = 0
        self.PSU_List_Ch_Used           = [1]  # Default is 1 channel
        self.PSU_Voltage_List_Ch_Used   = [28] # Default is 28 Volts
        self.PSU_Current_List_Ch_Used   = [2]  # Default is 2 Amps
        self.prog_cycle                 = 0
        self.iter                       = 0
        self.MaxRandomVal               = 60   # Maximum possible random value delay in seconds, default is 60 Secs
        self.ProgTryCount               = ''
        self.ShortDelay                 = 1
        self.Func_cache                 = False
        self.OvenTemp                   = []
        self.OvenTemp_duration_minutes  = []
        self.OvenControlled             = False
        self.OvenType                   = ''
        self.OvenPort                   = 0
        self.DummyFlag                  = False
        self.PowerCycleOnly             = False
        self.ProgramOnly                = False
        self.EthernetAdaptorNumber      = 2
        self.InstancesOfNWDS3Error      = 0
        self.debug                      = False

    def ShowCfg(self):
        '''
        Prints out all configuration parameters
        '''
        self.msg  = 'DUMMY_XIDML            : %s\n'   %self.DUMMY_XIDML

        self.msg += 'DummyFlag              : %s\n'   %self.DummyFlag
        self.msg += 'PowerCycleOnly         : %s\n'   %self.PowerCycleOnly
        self.msg += 'ProgramOnly            : %s\n'   %self.ProgramOnly

        self.msg += 'TEST_XIDML             : %s\n'   %self.TEST_XIDML
        self.msg += 'PU_Loop                : %s\n'   %self.PU_Loop
        self.msg += 'NWDS3Cmd               : %s\n'   %self.NWDS3Cmd
        self.msg += 'NWDS3LogFile           : %s\n'   %self.NWDS3LogFile
        self.msg += 'NWDS3LogDir            : %s\n'   %self.NWDS3LogDir
        self.msg += 'Wait before logging    : %s\n'   %self.LogWait
        self.msg += 'ExpectedErrors         : %s\n'   %self.ExpectedErrors
        self.msg += 'TotalErrors            : %s\n'   %self.TotalErrors
        self.msg += 'ETProgram              : %s\n'   %self.ETProgram
        self.msg += 'ProgCmd                : %s\n'   %self.ProgCmd
        #msg += 'ProgLOGDir          : %s\n'   %self.ProgLOGDir
        self.msg += 'ReportFile             : %s\n'   %self.ReportFile
        self.msg += 'LogWait                : %s\n'   %self.LogWait
        self.msg += 'ThermType              : %s\n'   %self.ThermType
        self.msg += 'PSUType                : %s\n'   %self.PSUType
        self.msg += 'OvenType               : %s\n'   %self.OvenType
        self.msg += 'PSU_port               : %s\n'   %self.PSU_Port
        self.msg += 'OvenPort               : %s\n'   %self.OvenPort
        self.msg += 'Therm_port             : %s\n'   %self.Therm_port
        self.msg += 'Power_off_time         : %s\n'   %self.PowerOffTime
        self.msg += 'Logfile Name           : %s\n'   %self.LogFileName
        self.msg += 'PSU Channels used      : %s\n'   %self.PSU_List_Ch_Used
        self.msg += 'PSU Voltage used       : %s\n'   %self.PSU_Voltage_List_Ch_Used
        self.msg += 'PSU Current used       : %s\n'   %self.PSU_Current_List_Ch_Used
        self.msg += 'Max Random Value delay : %s\n'   %self.MaxRandomVal

        self.msg += 'OvenTemp               : %s\n'   %self.OvenTemp
        self.msg += 'OventTempDurationMins  : %s\n'   %self.OvenTemp_duration_minutes

        self.msg += 'OvenTemp               : '
        for temp in self.OvenTemp :
            self.msg += ' %s' %temp
        self.msg += '\n'
        self.msg += 'OvenTemp Duration Mins : '
        for duration in self.OvenTemp_duration_minutes :
            self.msg += ' %s' %duration
        self.msg += '\n'

        if self.ThermometerPresent == True:
            self.msg += 'ThermometerPresent : True\n'
        else:
            self.msg += 'ThermometerPresent : False\n'

        if self.PSU_Present == True:
            self.msg += 'PSU_Present        : True\n'
        else:
            self.msg += 'PSU_Present        : False\n'

        print(self.msg)
        return self.msg


    def CheckConfig(self):
        '''
        Check if the ini file exists before doing anything else - bail out if it doesn't
        '''
        try:
            IniFileName = sys.argv[1]
            return True
        except:
            print('No configuration file passed.....')
            return False


    def ReadConfig (self,IniFileName, IniFileUsed):
    # def ReadConfig (self,IniFileName):
        '''
        Reads the supplied ini file and sets the relevant variables
        '''
        if IniFileUsed == True:
            IniFile = '%s' %IniFileName
            if (os.path.exists(IniFile)==False):
                sys.exit('The configuration file %s does not exist\n\nTerminating...' %(IniFile))
            else :
                ConfigFileHandle=ConfigParser()
                ConfigFileHandle.read(sys.argv[1])

                # XidML files and general test stuff
                self.PU_Loop                    = int(ConfigFileHandle.get(         'TestSettings','PU_Loop'))
                self.NWDS3Cmd                   = ConfigFileHandle.get(             'TestSettings','NWDS3Cmd')
                self.NWDS3LogFile               = ConfigFileHandle.get(             'TestSettings','NWDS3LogFile')
                self.NWDS3LogDir                = ConfigFileHandle.get(             'TestSettings','NWDS3LogDir')
                self.ExpectedErrors             = int(ConfigFileHandle.get(         'TestSettings','ExpectedErrors'))
                self.Cache_Prog                 = ConfigFileHandle.get(             'TestSettings','Cache_Prog').upper()
                if self.Cache_Prog == 'TRUE':
                    self.Cache_Prog = bool(1)
                else:
                    self.Cache_Prog = bool(0)
                self.ProgCmd                    = str(ConfigFileHandle.get(         'TestSettings','ProgCmd'))
                self.Dummy_Config               = str(ConfigFileHandle.get(         'TestSettings','DUMMY_CONFIG'))
                self.Test_Config                = str(ConfigFileHandle.get(         'TestSettings','TEST_CONFIG'))
                self.DAS_MSG_LOG                = ConfigFileHandle.get(             'TestSettings','DasStudio Msg Log')
                self.ETProgram                  = str.lower(ConfigFileHandle.get(   'TestSettings','ETProgram'))
                self.KUPCmd                     = ConfigFileHandle.get(             'TestSettings','KUPCmd')
                self.KUPTryCount                = int(ConfigFileHandle.get(         'TestSettings','KUPTryCount'))
                self.ReportFile                 = ConfigFileHandle.get(             'TestSettings','ReportFile')
                self.KUPLOGDir                  = ConfigFileHandle.get(             'TestSettings','KUPLOGDir')
                self.LogWait                    = int(ConfigFileHandle.get(         'TestSettings','LogWait'))
                self.PowerOffTime               = str(ConfigFileHandle.get(         'TestSettings','PowerOffTime'))
                self.PowerCycleOnly             = ConfigFileHandle.get(             'TestSettings','PowerCycleOnly').upper()
                if self.PowerCycleOnly == 'TRUE':
                    self.PowerCycleOnly = bool(1)
                else:
                    self.PowerCycleOnly = bool(0)
                self.ProgramOnly                = ConfigFileHandle.get(             'TestSettings','ProgramOnly').upper()
                if self.ProgramOnly == 'TRUE':
                    self.ProgramOnly = bool(1)
                else:
                    self.ProgramOnly = bool(0)
                self.LogFileName                = str(ConfigFileHandle.get(         'TestSettings','LogFileName'))
                self.DAS_STANDALONE_DIR         = str(ConfigFileHandle.get(         'TestSettings','DasStudio Standalone Dir'))
                self.ProgTryCount               = int(ConfigFileHandle.get(         'TestSettings','ProgTryCount'))
                self.ShortDelay                 = int(ConfigFileHandle.get(         'TestSettings','ShortDelay'))
                self.DummyFlag                  = ConfigFileHandle.get(             'TestSettings','DummyFlag').upper()
                if self.DummyFlag == 'TRUE':
                    self.DummyFlag = bool(1)
                else:
                    self.DummyFlag = bool(0)
                print((self.DummyFlag))
                self.ABI_Transactor_Dir         = str(ConfigFileHandle.get(         'TestSettings','ABITransactorDir'))
                self.ABI_Download_Cmd           = str(ConfigFileHandle.get(         'TestSettings','ABIDownloadCmd'))
                self.SectorFilename             = (ConfigFileHandle.get(            'TestSettings','SectorFilename'))
                self.SectorFilename_Orig        = (ConfigFileHandle.get(            'TestSettings','SectorFilename_Orig'))
                self.binary_compare             = str(ConfigFileHandle.get(         'TestSettings','binary_compare'))
                self.Cache_Dir                  = str(ConfigFileHandle.get(         'TestSettings','Cache_Dir'))
                self.Cache_Filename             = str(ConfigFileHandle.get(         'TestSettings','Cache_Filename'))
                self.Cache_Orig_Dir             = str(ConfigFileHandle.get(         'TestSettings','Cache_Orig_Dir'))
                self.Cache_Orig_Func_Dir        = str(ConfigFileHandle.get(         'TestSettings','Cache_Orig_Func_Dir'))


                self.ThermType                  = str(ConfigFileHandle.get(         'InstrSettings','ThermType'))
                self.ThermometerPresent         = ConfigFileHandle.get(     'InstrSettings','ThermometerPresent').upper()
                metertempvar = self.ThermometerPresent
                if self.ThermometerPresent == 'TRUE':
                    self.ThermometerPresent = bool(1)
                else:
                    self.ThermometerPresent = bool(0)
                self.PSUType                    = str(ConfigFileHandle.get(         'InstrSettings','PSUType'))
                self.PSU_Present                = ConfigFileHandle.get(        'InstrSettings','PSU_Present').upper()
                if self.PSU_Present == 'TRUE':
                    self.PSU_Present = bool(1)
                else:
                    self.PSU_Present = bool(0)
                self.PSU_Port                   = int(ConfigFileHandle.get(         'InstrSettings','PSU_Port'))
                self.Therm_port             = int(ConfigFileHandle.get(         'InstrSettings','Therm_port'))
                self.PSU_List_Ch_Used           = (ConfigFileHandle.get(            'InstrSettings','PSU_List_Ch_Used')).strip('][').split(',')
                self.PSU_Voltage_List_Ch_Used   = (ConfigFileHandle.get(            'InstrSettings','PSU_Voltage_List_Ch_Used')).strip('][').split(',')
                self.PSU_Current_List_Ch_Used   = (ConfigFileHandle.get(            'InstrSettings','PSU_Current_List_Ch_Used')).strip('][').split(',')
                self.MaxRandomVal               = int(ConfigFileHandle.get(         'InstrSettings','Max_Random_Delay_Secs'))
                self.OvenControlled             = ConfigFileHandle.get(             'InstrSettings','OvenControlled').upper()
                if self.OvenControlled == 'TRUE':
                    self.OvenControlled = bool(1)
                else:
                    self.OvenControlled = bool(0)
                self.OvenTemp_duration_minutes  = (ConfigFileHandle.get(            'InstrSettings','OvenTemp_duration_minutes')).strip('][').split(',')
                self.OvenTemp                   = (ConfigFileHandle.get(            'InstrSettings','OvenTemp')).strip('][').split(',')
                self.OvenType                   = str(ConfigFileHandle.get(         'InstrSettings','OvenType'))
                self.OvenPort                   = int(ConfigFileHandle.get(         'InstrSettings','OvenPort'))

        else:
            sys.exit('No configuration file passed.....')

    def CheckLogFile(self):
        '''
        Check if a logfile already exists, if it does create a backup copy and delete create a new file overwriting the original
        '''
        self.LogFileName = os.getcwd()+r'\%s'%self.ReportFile
        if (os.path.exists(self.LogFileName)==True):
            now = datetime.now()
            self.timeStamp = now.strftime("%Y%m%d__%H_%M_%S")
            os.rename(self.LogFileName,(self.LogFileName[0:-4]+"_BKUP_"+str(self.timeStamp)+".log"))
            logfile = open(self.LogFileName, 'w')
            logfile.close()

    def mWrLog(self):
        #self.LogFileName = os.getcwd()+r'\%s'%self.ReportFile
        update_logfile = open(self.LogFileName,'a')
        update_logfile.write(self.msg)
        update_logfile.close()

    def WriteLog(self) :
        '''
        writes message to the screen and records it in the log file
        '''
        now = datetime.now()
        self.timeStamp = now.strftime("%Y-%m-%d %H:%M:%S")
        self.msg = '%s : %s\n' %(self.timeStamp, self.msg)
        print(self.msg)
        try:
            self.mWrLog()
        except:
            msg = ('************************************************************************\nEXCEPTION: mWrLog write exception:%s: %s; %s' %(self.msg, sys.exc_info()[0], sys.exc_info()[1]))
            print(self.msg)


    def Prog_XidML(self):
        self.result         = 0
        self.prg_err_cnt    = 1
        self.ProgSuccessful = False
        os.chdir(self.DAS_STANDALONE_DIR)
        if self.DummyFlag == True:          # If True, Program the Dummy Configuration
            self.file = self.Dummy_Config
        else:
            self.file = self.Test_Config
        self.KUPCmd = self.ProgCmd + self.file
        self.msg = self.KUPCmd
        self.WriteLog()
        self.result = os.system(self.KUPCmd)
        print(self.result)
        if self.result == 0:
            self.ProgSuccessful = True
            self.msg = ('Programming Success')
            self.WriteLog()
        else:
            while(self.ProgSuccessful == False):
                if (self.result == -1): # == -1  means failed programming
                    print ('result=%s'%self.result)
                    self.KUPCmd = self.ProgCmd + self.file
                    self.msg = self.KUPCmd
                    self.WriteLog()
                    self.result = os.system(self.KUPCmd)
                    print ('result=%s****************************'%self.result)
                    self.msg = ('Programming Failure')
                    self.WriteLog()
                else:
                    self.ProgSuccessful = True
                self.prg_err_cnt += 1
                if (self.prg_err_cnt == self.ProgTryCount):  # Break out of the While Loop
                    print ('prg_err_cnt=%s self.ProgTryCount%s***************************'%(self.prg_err_cnt,self.ProgTryCount))
                    self.msg = '<ERROR> Programming FAILED 3 PROGRAMMING CYCLES  '
                    self.WriteLog ()
                    self.ProgSuccessful = False
                    print ('ProgSuccessful=%s **********************'%self.ProgSuccessful)
                    break
            print ('prg_err_cnt=%s ----------------------'%self.prg_err_cnt)
        return 0

    def SectorCompare(self):
       '''
       Compare sectors by downloading the sector using abi transactor
       Does not return value, only outputs empty files if with difference or no difference
       '''
       Sector_dCmd = self.ABI_Transactor_Dir + self.ABI_Download_Cmd + str(' ') + self.SectorFilename
       Orig_File = os.getcwd() + (str('\%s')%self.SectorFilename_Orig)

       print (Sector_dCmd)
       SecFolder = os.getcwd()+r'\SectorFiles'
       print (SecFolder)
       if (os.path.exists(SecFolder)==False):
           print ('creating folder SectorFiles')
           os.mkdir(r'%s'%SecFolder)
       else:
           print ('Folder exist:%s'%SecFolder)
#       os.system(Sector_dCmd)
       now = datetime.now()
       self.timeStamp = now.strftime("%Y%m%d__%H_%M_%S")
       copy_secfile_cmd = r'copy %s%s %s\%s' %(self.ABI_Transactor_Dir, self.SectorFilename, SecFolder,self.SectorFilename[0:-4]+"_BKUP_"+str(self.timeStamp)+".log") #[0:-4]+"_Dummy_"+str(self.timeStamp)+".log
       print (copy_secfile_cmd)
       os.system(copy_secfile_cmd)
       print ('Compare sectors')
        ##########Comparing#######
        ##returns the diffrence of the downloaded from the original####
       print(self.ABI_Transactor_Dir + self.SectorFilename)
       print (Orig_File)
       with open(self.ABI_Transactor_Dir + self.SectorFilename, 'r') as file1:
          with open(Orig_File, 'r') as file2:
              same = set(file1).difference(file2)
       same.discard('\n')
       output_file = r'%s'%( SecFolder + "\_difference_" +str(self.timeStamp)+".log")
       print (output_file)
       with open(output_file, 'w') as file_out:
              for line in same:
                  file_out.write(line)
       file_out.close()

    # def Prog_Cache(self):
    #     if self.DummyFlag == True:
    #         pass  # GDG completing
    #     else:
    #         pass  # GDG completing

    def CacheCompare(self):
        '''
        Command for comparing Caches is
        ACRA.BinaryComparer.exe directory_A directory_B report_text_file
        There is a command (-L) to place the cache files in an alternative location.
        '''
        if self.Func_cache == True:
           Cache_Compare_Dir = self.Cache_Orig_Func_Dir
        else:
           Cache_Compare_Dir = self.Cache_Orig_Dir
        CacheCompare_Cmd = self.binary_compare + str(' ') + self.Cache_Dir + str(' ')+ str(Cache_Compare_Dir) + str(' ') + str(self.Cache_Filename)
        self.msg = CacheCompare_Cmd
        print(self.msg)
        self.WriteLog()
        Cache_File = os.getcwd() + (str('\%s')%self.Cache_Filename)
        self.msg = Cache_File
        print(self.msg)
        self.WriteLog()
        CacheFolder = os.getcwd()+r'\CacheFiles'
        if (os.path.exists(CacheFolder)==False):
            print ('creating folder CacheFiles')
            os.mkdir(r'%s'%CacheFolder)
        else:
            print ('Cache Folder exists:%s'%CacheFolder)
        os.system(CacheCompare_Cmd)
        ### Check result if cache is the same###
        try:
            f = open(self.Cache_Filename,'r')
            lines = f.readlines()
            f.close()
            search = "Passed"
            found = False
            result = 'Fail'
            for line in lines:
                if search in line:
                   print (line)
                   result = 'Pass'

                   found = True
            if found == True:
                self.msg = ('Cache Compare Passed')
                print ('result=%s'%result)
                self.WriteLog()

            else:
                self.msg = ('Cache Compare Failed')
                print ('result=%s'%result)
                self.WriteLog()
        except:
            result='Fail'
            self.msg = ('<ERROR> Could not open file: %s' %self.Cache_Filename)
            self.WriteLog()

        ##########################################
        now = datetime.now()
        self.timeStamp = now.strftime("%Y%m%d__%H_%M_%S")

        copy_cacheresult_cmd = r'copy %s %s\%s%s' %(Cache_File, CacheFolder, result, "_" + self.Cache_Filename[0:-4]+"_BKUP_"+str(self.timeStamp)+".txt") #[0:-4]+"_Dummy_"+str(self.timeStamp)+".log
        print (copy_cacheresult_cmd)
        copy_result=os.system(copy_cacheresult_cmd)
        if copy_result != 0:
            self.msg=('<ERROR> Could not copy file: %s' %self.Cache_Filename)
            print(self.msg)
            self.WriteLog()
        #return result

    def ProgFuncTest(self):
        '''
        Sets the test type to "FUNC"  and calls ProgTest()
        '''
        if self.Cache_Prog == False:
            self.msg = '<INFO> Programming %s' %self.TEST_XIDML
            self.WriteLog()
            self.Prog_XidML()
            #self.WriteLog()
            print(self.msg)
        else:
            print (self.Prog_XidML())
            # Todo, programming cache method dependent on Controller type
            self.Prog_XidML()

    def VerTestProg(self):
        '''
        Verify Programming result  and calls VerTestProg()
        '''

    def ProgDummyTest(self)  :
        '''
        Sets the test type to "DUMMY"  and calls ProgTest()
        '''
        if self.Cache_Prog == False:
            self.msg = '<INFO> Programming %s' %self.DUMMY_XIDML
            self.Prog_XidML()
            #self.WriteLog()
            print(self.msg)
        else:
            print (self.Prog_XidML())
            # Todo, programming cache method dependent on Controller type
            self.Prog_XidML()

    def TimeStamp(self):
        '''
        Gets current time and creates "msg"
        '''
        now1 = datetime.datetime.now()
        timeStamp = now1.strftime("%Y-%m-%d %H:%M:%S")
        self.msg = '<INFO> Timestamp: %s ' %(timeStamp)
        return self.msg

    def pwrcycle(self):
        '''
        Power cycle the required channel
        The power down delay is random, based on the random variable self.pwrdelay
        This Method handles multi-channel PSUs
        '''
        self.msg = "<INFO> %s Power OFF"%self.PSUType
        print(self.msg)
        self.WriteLog()
        if self.PSU_Present == True :
            for index, channel in enumerate(self.PSU_List_Ch_Used):
                self.power.setONOFF('OFF',channel)


        sleep(self.pwrdelay)

        self.msg = "<INFO> %s Power ON"%self.PSUType
        print(self.msg)
        self.WriteLog()
        if self.PSU_Present == True :
            for index, channel in enumerate(self.PSU_List_Ch_Used):
                self.power.setONOFF('ON',channel)

        sleep(self.ShortDelay)

    def LogData(self):
        '''
        Runs the NWDS3 Cmd and returns "True" is run OK
        '''
        NWDS3cmd = self.NWDS3Cmd
        #print('Debug DDN NWDS3cmd', NWDS3cmd)
        #result = os.system('del %s' %(self.NWDS3LogFile))
        result = os.system(NWDS3cmd)
        print ("NWDS3_Response: " + str(result))
        self.msg = ('<INFO> NWDS3_Response: %s' %(str(result)))
        self.WriteLog()
        if(int(result)!=0):
            NWDS3_Response = False
        else:
            NWDS3_Response = True

    def CheckLog(self):
        '''
        Check the NWDS3 log file for the total errors
        and compare it to the expected allowable number of errors
        '''
        linesInFile = 999
        error = 999
        warnings = 999
        logDirFile = ('%s\%s' %(self.NWDS3LogDir, self.NWDS3LogFile))
        try:
            infile = open(logDirFile, 'r')
            self.msg = ('*************************** NWDS3_Inspect Report ***************************')
            self.WriteLog()
            for line in infile:
                if 'Total Errors:' in line:
                   msgline = str(line).split(' ')
                if line != '':
                    linesInFile += 1
            infile.close()
        except:
            self.msg=('<ERROR> There were an eror during reading the file: %s' %self.NWDS3LogFile)
            print(self.msg)
            self.WriteLog()
        try:
            error = int(msgline[2])
        except:
            error = 999
        self.msg = ('*************************************************************************')
        self.WriteLog()
        self.msg = ('<INFO> NWDS3 log report: Total lines: %d; Errors: %d; Warnings: %d' %(linesInFile, error, warnings))
        self.WriteLog()
        print(self.msg)

        if error > self.ExpectedErrors:
            self.msg = '<INFO>: ********* %s Errors detected, %s expected *********' %(error, self.ExpectedErrors)
            self.WriteLog ()
            self.CopyLogs()
            return 'ERRORS'
        else :
            return 'OK'

    def CopyLogs(self):
        '''
        copy the log files for later analysis
        '''
        file_list = [self.NWDS3LogFile]
        for file in file_list:
            if (self.iter == -1):
                logname = 'Prog_%d_programm_%s' %(self.prog_cycle, file)
            else :
                logname = 'Prog_%d_cycle_%d_%s' %(self.prog_cycle, self.iter, file)
                if file == self.NWDS3LogFile:
                    logname = '%s.log' %(logname[:-4])
            cmd = 'copy %s\%s %s\%s' %(self.NWDS3LogDir, file, self.KUPLOGDir, logname)
            self.msg = '<DEBUG> Copy log file cmd: %s' %(cmd)
            self.WriteLog()
            result = os.system(cmd)
            self.msg = '<DEBUG> Copy result: %s' %(str(result))
            self.WriteLog()
            if result !=0:
                self.msg = '<ERROR> Could not copy result file: %s' %(str(file))
                self.WriteLog()

    def Initialise_Instruments(self):
        '''
        Instruments to be initalised
        '''
        self.Init_PSU()
        self.InitTherm()

        if self.OvenControlled == True: # If = False, the Oven is independently running its own predefined profile or is being controlled by another script
            self.msg = ('<DEBUG> Oven is being controlled by the power cycling script')
            self.WriteLog()
            self.InitOven()
        else:
            self.msg = ('<DEBUG> Oven is NOT being controlled by the power cycling script')
            self.WriteLog()
    #######################################################################################
    # EXAMPLE OF HOW TO ADD AN INSTRUMENT
    #   self.InitAnyInstrument()  # Add call here to initialise required instrument
    #
    # def InitAnyInstrument():      # Add code here to do the instrument initialisation
    #   print('ADD CODE HERE')
    ######################################################################################
    def InitOven(self):
        '''
        First the Oven will be initialised by a call to OVEN.py
        Next the Oven Profile will be configured to run as a separate thread
        The ramp for each stage in the profile will be run sequentially in the same thread
        When each stage completes, the for loop moves onto the next stage
        '''
        try:
            self.ov = OVEN(self.OvenPort,self.OvenType)
            self.msg = ('OvenInitialised, starting Temperature Profile as a parallel process')
            print(self.msg)
            self.WriteLog()
            ##self.oven_thread = threading.Thread(target = self.OvenLoopParallelProcess, args = (self.OvenTemp,self.OventTempDurationMins))
            self.oven_thread = threading.Thread(target = self.OvenLoopParallelProcess, args = (self.ov,) )
            self.oven_thread.start()
        except SerialException:
            self.msg = ('SerialException: Could not initialise oven, check port setting')
            print(self.msg)
            sys.exit(self.msg)
        except:
            print('Error, Threading Issue')

    def OvenLoopParallelProcess(self,mydebugindex):
        '''
        This process will run in parallel to the power cycling script
        It will loop through all stages in the OvenTemp profile defined in the INI file
        This index of each value will be used to locate the corresponding value in the temperature duration list
        '''
        for index,element in enumerate(self.OvenTemp):
            print(int(element),index)
            try:
                self.msg = ('Oven stage %d, Temperature %d, duration in Minutes %d'%(index,int(element),int(self.OvenTemp_duration_minutes[index])))
                self.WriteLog()
                print(self.msg)
                # This setRamp function thread will wait until each OvenTempDuration stage time is complete
                # Hence the reason it needs to be a parallel process
                # The original thread will continue independently
                self.ov.setRamp(int(element),(int(self.OvenTemp_duration_minutes[index])*60)) # ramp function expects seconds
                #self.ov.setRamp(-40,1200) debug
                input('pause')
            except FileNotFoundError:
                self.msg = ("Error, Problem calling setRamp function")
                self.WriteLog()
                print(self.msg)
                os.sys.exit(self.msg)

    def Init_PSU(self):
        '''
        Initialise the PSU
        '''
        try:
            if self.PSU_Present == True :
                self.power = BENCH_PSU(self.PSU_Port,self.PSUType)
                self.power.setRst()
            else :
                self.msg = '<DEBUG> NO PSU CONNECTED: Simulated PSU initialised and reset'
                self.WriteLog()
        except SerialException:
            os.sys.exit('Exiting,SerialException Cannot connect to PSU')
        except IOError:
            os.sys.exit('Exiting,IOError Cannot connect to PSU')

    def InitTherm(self):
        '''
        Initialise the digital Thermometer
        '''
        if self.ThermometerPresent == True :
            self.Tport = THERM()
            self.msg = '<DEBUG> THERMOMETER Begin Initialisation: Com port %d' %(self.Therm_port)
            self.WriteLog()
            self.msg = self.Tport.InitMeter(True,self.Therm_port,self.ThermType)
            #self.msg = '<DEBUG> THERMOMETER Initialised: Com port %d' %(self.Therm_port) # duplicate of code in InitMeter
            self.WriteLog()
            self.GetTemperature()
        else :
            self.msg = '<DEBUG> NO THERMOMETER CONNECTED: Simulated initialisation'
            self.WriteLog()


        # try:
        #     if self.ThermometerPresent == True :
        #         self.Tport = THERM()
        #         self.msg = '<DEBUG> THERMOMETER Begin Initialisation: Com port %s' %(self.Therm_port)
        #         self.WriteLog()
        #         self.Tport.InitMeter(True,self.Therm_port,self.ThermType)
        #         self.msg = '<DEBUG> THERMOMETER Initialised: Com port %s' %(self.Therm_port)
        #         self.WriteLog()
        #         self.GetTemperature()
        #     else :
        #         self.msg = '<DEBUG> NO THERMOMETER CONNECTED: Simulated initialisation'
        #         self.WriteLog()
        # except SerialException:
        #     os.sys.exit('Exiting, SerialException Cannot connect to Thermometer')
        # except IOError:
        #     os.sys.exit('Exiting, IOError Cannot connect to Thermometer')

    def SetPSU_Output(self):
        '''
        Set up the PSU output levels (V/I)
        This Method handles multi-channel PSUs
        The Curent and Voltage lists in the INI file must correspond with the channels listed in PSU_List_Ch_Used
        For a single channel PSU each INI parameter list will be a list containing 1 element eg. [28] for 28Volts
        '''
        if self.PSU_Present == True :
            for index, channel in enumerate(self.PSU_List_Ch_Used):
                self.power.setVolt(self.PSU_Voltage_List_Ch_Used[index], int(channel))
                self.power.setAmp(self.PSU_Current_List_Ch_Used[index], int(channel))
                self.power.setONOFF('ON', int(channel))
        else :
            self.msg = '<DEBUG> NO PSU CONNECTED: Simulating Setting PSU outputs ON'
            self.WriteLog()
        sleep(self.ShortDelay)   # allow the switch time to start up.


    def GetTemperature(self):
        '''
        Read the current temperature from the thermometer
        '''
        temp = [0,0,0,0,0]
        if self.ThermometerPresent == True:
            try:  #  Allows for test to continue even if Thermometer powers down unexpectedly
                self.gettemp = self.Tport.GetTemp()
                print(self.gettemp)
                for index,element in enumerate(self.gettemp): # handles 2 or 4 channel readings
                    temp[index] = element
                #print(temp)
                self.msg = ('Successfully Read Thermometer: %s'%temp)
            except:
                self.msg = ('Thermometer is not responding, continuing anyway')
            self.WriteLog()
        else :
            temp[0] = 999; temp[1] = 25.0; temp[2] = 25.0
            self.msg = "<DEBUG>: Simulating thermometer"
            self.msg += 'Temperature Channel 1 %2.2f, Channel 2 %2.2f' %(temp[1], temp[2])
            self.WriteLog()
            #print(self.msg)

        if temp[0] == 999:
            if len(temp)>2:
                self.msg = 'Temperature Channel 1 %2.2f, Channel 2 %2.2f, Channel 3 %2.2f, Channel 4 %2.2f' %(temp[0], temp[1], temp[2], temp[3])
            else:
                self.msg = 'Temperature Channel 1 %2.2f, Channel 2 %2.2f' %(temp[1], temp[2])
            self.WriteLog()
            #print(self.msg)


        elif (temp[1] > 85.0) or (temp[1] < -45.0):
            self.ETRange = True
            if self.ETProgram == 'yes':
                self.msg = '<INFO> ****************** Programming in Extended Range: temp = %s ******************' %temp
                self.WriteLog()
                #print(self.msg)
            else:
                self.msg = '<INFO> ******************Thermometer outside expected Range: temp = %s ******************' %temp
                self.WriteLog()
                print(self.msg)
        else :
            self.msg = '<INFO> Programming in Normal Range: temp = %.1f Degrees Celsius' %temp[1]
            self.WriteLog()
            #print(self.msg)


    def generate_random_delay(self):
        '''
        Create a random delay to simulate real world
        '''
        self.random = float((randint(1,(self.MaxRandomVal*10)))/10)     # range is 0.1 secs to self.MaxRandomVal
        if self.PowerOffTime.lower() == 'random':
            self.pwrdelay = self.random
        else:
            try :
                self.pwrdelay = float(self.PowerOffTime)   # User supplied delay
            except ValueError:
                self.msg = "Invalid Power Delay Setting in ini file , enter a real no or just random"
                self.WriteLog (self)
                sys.exit()


    def power_cycle_PU_Loop_times(self):
        '''
        Power cycle Cycle using loop count from INI file parameter PU_Loop
        '''
        for self.cyclecount in range(self.PU_Loop):      # take loop count from ini file
            self.i = self.cyclecount + 1
            self.msg  = '*****************************************'
            self.WriteLog()
            self.msg = '<INFO> Programming cycle %d Power Cycle %d' %(self.prog_cnt, self.i)
            self.WriteLog()
            self.msg = '*****************************************'
            self.WriteLog()

            self.GetTemperature()

            if self.PSU_Present == True:
                self.generate_random_delay()

                self.msg = 'Power Delay ' + str(self.pwrdelay)
                self.WriteLog()
                self.msg = '<INFO> Programming cycle %d Power Cycle %d: Power down for %f seconds' %(self.prog_cnt, self.i, self.random)
                self.WriteLog()
                self.pwrcycle()
            else :
                self.msg = "<DEBUG> Power cycling Output"
                self.WriteLog()

            self.msg = '<INFO> Waiting for %d seconds before starting logging' %(self.LogWait)
            self.WriteLog()
            sleep(self.LogWait)

            self.msg = '<INFO> Logging data for programming cycle %d Power Cycle %d' %(self.prog_cnt, self.i)
            self.WriteLog()

            result = self.LogData()

            if result == False :
                self.msg = '<ERROR> Error logging data for programming cycle %d Power Cycle %d' %(self.prog_cnt,self.i)
                self.WriteLog()

            result = self.CheckLog()

            if result == 'OK' :
                self.msg = '<RESULT> Programming cycle %d Power up Cycle %d - Data logged correctly' %(self.prog_cnt, self.i)
                self.WriteLog()

            elif result == 'ERRORS' :
                self.msg = '<RESULT> Programming cycle %d Power up Cycle %d - ****************** Errors Logged ******************' %(self.prog_cnt, self.i)
                self.WriteLog()
                self.InstancesOfNWDS3Error+=1

            else:
                self.msg = '<RESULT> Unknown result code: %s' %(str(result))
                self.WriteLog()
            self.testNum  +=1

    def log_data_for_analysis(self):
        sleep(self.LogWait)
        self.msg = '<INFO> Logging data for programming cycle %d' %self.prog_cnt
        self.WriteLog()
        result = self.LogData()
        if result == False :
            self.msg = '<ERROR> NWDS3 failed to run for programming cycle %d' %self.prog_cnt
            self.WriteLog()
        result = self.CheckLog()

        if result == 'OK' :
            self.msg = '<RESULT> Programming cycle %d - Data logged correctly' %self.prog_cnt
            self.WriteLog()
        elif result == 'ERRORS' :
            self.msg = '<RESULT> Programming cycle %d - ****************** Errors Logged ******************' %self.prog_cnt
            self.WriteLog ()
        else :
            self.msg = '<RESULT> Unknown result code: %d' %result
            self.WriteLog ()

    def StartOvenProfile(self):
        print("OvenStarted")
        input("Stop Here")

    def run_pu_prog(self):
        '''
        Main Cycle
        '''
        # while 1 (until exited by user hitting Ctrl+C)
        self.prog_cnt = 1
        self.ETRange = False
        self.iter = -1
        self.testNum = 0
        while 1 :
            if self.DummyFlag == True:
                self.msg  = '*****************************************'
                self.WriteLog()
                self.msg = '<INFO> Programming cycle %d Dummy' %(self.prog_cnt)
                self.WriteLog()
                self.msg = '******************************************'
                self.WriteLog()
            else:
                self.msg  = '*****************************************'
                self.WriteLog()
                self.msg = '<INFO> Programming cycle %d Functional Test' %(self.prog_cnt)
                self.WriteLog()
                self.msg = '******************************************'
                self.WriteLog()
            if self.PSU_Present == True :
                #------------------- Set PSU Output before Reprogramming --------------------
                print ('Power Cycle Running----')
                self.SetPSU_Output()
            # ------------------ Program dummy config ------------------------------------
            if self.PowerCycleOnly == False: #  True = Power Cycle only, no programming
                if self.DummyFlag == True:     #  True = Program the Dummy Configuration
                    self.msg = '<DEBUG> Program dummy XIDML file first'
                    self.WriteLog()
                    print(self.msg)
                    self.ProgDummyTest()    # program the dummy XIDML file
                    self.Func_cache = False
                    self.CacheCompare()
                # ------------------ Program test config -------------- Cache compare is not necessary, NWDS3 will verify programming
                else:
                    print ('functional -----------------------')
                    self.msg = '<DEBUG> Program test XIDML file'
                    self.WriteLog()
                    print(self.msg)
                    self.ProgFuncTest()    # program the real XIDML file
                    self.Func_cache = True
                    #self.CacheCompare()  # NWDS3 will verify programming

            else:
                print ('power cycle only----------------')
                self.msg = '<DEBUG> Power cycle only mode, Module has not been programmed'
                self.WriteLog()

            if self.ProgramOnly == False: # if self.ProgramOnly == True then we will not be power cycling
                #self.log_data_for_analysis() #GDG
                #-------------- Power Cycle chassis n times--------------
                if self.DummyFlag == False:  # Don't log data for Dummy Cycle, we use cache compare to verify dummy cycle
                    self.power_cycle_PU_Loop_times() #GDG

            if self.PSU_Present == False:
                msg = '<DEBUG> NO PSU CONNECTED: Simulating Reset'
                self.WriteLog()
            if self.DummyFlag == False:
                self.DummyFlag = True
                self.prog_cnt += 1  # We only increment the programming count after the functional test
            else:
                self.DummyFlag = False
            self.iter += 1
            self.msg = ('Cumulative instances of logging Error = %d'%self.InstancesOfNWDS3Error)
            print(self.msg)
            self.WriteLog()



###########################################################################
###                         Start the test                             ####
###########################################################################
if __name__ == '__main__':

    try:
        IniFileName = sys.argv[1]
    except IndexError:
        sys.exit('No ini file argument supplied')

    # Instantiate the structure
    PU_STRUCT_INST = PU_STRUCT()
    res = PU_STRUCT_INST.CheckConfig()
    PU_STRUCT_INST.ReadConfig(IniFileName, res)
    PU_STRUCT_INST.ShowCfg()
    PU_STRUCT_INST.CheckLogFile()
    PU_STRUCT_INST.Initialise_Instruments()
    PU_STRUCT_INST.SetPSU_Output()

    try:
        while(1):
            PU_STRUCT_INST.run_pu_prog()

    except KeyboardInterrupt:
        self.msg += ('\n'*3 + '\tCNTRL-C Pressed . Terminating.' + '\n'*3)
        self.WriteLog()
        sys.exit(self.msg)