#-------------------------------------------------------------------------------
# Name:        list_files.PY     
#
# Author:      cjg
#
# Created:     19/03/2014
# Copyright:   (c) Curtiss Wright Controls
#
# Description:
#
#   Script which searches through a given root folder and generates a list of all files 
#   in that folder and its sub folders with the listed file extensions
#
#-------------------------------------------------------------------------------
# History:
#
#   Created 19 Mar, 2014:   Created.
#-------------------------------------------------------------------------------

import string
import os

from os import walk
from os.path import splitext, join
def select_files(root, files, extensions):
    """
    simple logic here to filter out interesting files
    File extension passed in as an arguement.
    """

    selected_files = []
    append = selected_files.append

    for file in files:
        #do concatenation here to get full path
        full_path = join(root, file)
        ext = splitext(file)[1]

        if ext in extensions:
            append(full_path)

    return selected_files


def build_recursive_dir_tree(path, extensions):
    """
    path    -    where to begin folder scan
    extensions - list of extensions that we are interested in.
    returns a list of files of interest with their full paths
    """
    selected_files = []

    for root, dirs, files in walk(path):
        selected_files += select_files(root, files, extensions)

    return selected_files



def do_git_add(file_list):
    '''
    File_list   -   list of files consisting of file names and full paths

    runs the git add command on all file in file_list
    
    Doesn't matter that old files are added to the repo, git is smart enough 
    to ignore them if there have been no changes.
    '''
    for file in file_list:
        cmd = 'git add %s\n' %file
        os.popen(cmd)
   
##############################################################
# test area
# this is what needs to be run to get the final answer.....
###############################################################
path = '.\\acra'
my_extensions = ['.py', '.txt', '.doc', '.docx', '.bat']

required_list = build_recursive_dir_tree(path, my_extensions)
do_git_add(required_list)

comment = raw_input('Enter Commit Comment here : ')

commit_cmd = 'git commit -m \"%s\"' %comment
os.popen(commit_cmd)

tag_version = raw_input('Enter Tag Version (format vX.YY: ')
tag_comment = raw_input('Enter Tag Comment: ')

tag_cmd = 'git tag -a %s -m \"%s\"' %(tag_version, tag_comment)
os.popen(tag_cmd)





