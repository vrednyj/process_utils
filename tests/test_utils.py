import unittest
from sys import path
from os import getcwd
path.insert(0, getcwd())
from acra.utils import DS3Parser
from acra.utils import NWInspectParser


class Test (unittest.TestCase):

    def test_ds3p(self):
        '''
        Tests DS3Parser
        '''
        my_test_file = './tests/test_utils/ds3log.dat'
        ds3p = DS3Parser(filename=my_test_file, radix='dec',
                    num_of_channels=12,
                    num_of_frames=20,
                    num_sam_per_frame=10,
                    num_of_mf_words=20,
                    first_chan_offset=3)
        self.assertEqual(True, ds3p.is_in_lock())
        self.assertEqual(True, ds3p.inlock)
        self.assertEqual(20, ds3p.get_frame_count(), 'Expected to be: 20')
        self.assertEqual(20, ds3p.frames)
        self.assertEqual(200, ds3p.get_param_count(), 'Expected to be: 200')
        self.assertEqual(200, ds3p.params)
        self.assertEqual(21840641426, ds3p.get_time(1,2,3)[0])
        self.assertEqual('65131', ds3p.data[0][0])
        self.assertEqual('10304', ds3p.get_word(1,1))


    def test_nwds(self):
        '''
        Tests NWInspectParser
        '''
        my_test_file = './tests/test_utils/nwlog.dat'
        nwds3p = NWInspectParser(
            my_test_file,
            radix='dec',
            num_of_channels=12,
            num_of_packets=200,
            num_sam_per_packet=10)
        self.assertEqual(0, nwds3p.check_footer_error())


if __name__ == '__main__':
    # begin the unittest.main()
    unittest.main()
