from setuptools import setup, find_packages
from os import environ

try :
    with open("README.md", "r") as fh:
        long_description = fh.read()
except Exception:
    long_description  = "Long description is not generated"
try:
    if environ.get('CI_COMMIT_TAG'):
        version = environ['CI_COMMIT_TAG']
    else:
        version = environ['CI_JOB_ID']
except KeyError:
    version = '0.0.0'

try:
    if environ.get('CI_COMMIT_AUTHOR'):
        author = environ['CI_COMMIT_AUTHOR']
    else:
        author = 'Unknown Author'
except KeyError:
    author = 'Unknown Author'

setup(
   name='acra',
   author=author,
   description="Generic ACRA test utilities",   
   packages=find_packages(),
   version = version,
   long_description=long_description,
   long_description_content_type="text/markdown",
   include_package_data=False,
   classifiers=["Programming Language :: Python :: 3",
                "Natural Language :: English",
                "Operating System :: Microsoft :: Windows",
                "Framework :: Sphinx"                
                ],
   python_requires='>=3.7',
   setup_requires=['setuptools-git-versioning'],
   setuptools_git_versioning={
       "dirty_template": "{tag}",
   }
)